const PREC = {
  range: 15,
  call: 14,
  field: 13,
  unary: 11,
  multiplicative: 10,
  additive: 9,
  shift: 8,
  bitand: 7,
  bitxor: 6,
  bitor: 5,
  comparative: 4,
  and: 3,
  or: 2,
  parenthesized_expression: 1,
  assign: 0,
}

const SEMICOLON = ';'

module.exports = grammar({
  name: 'monkeyc',

  extras: $ => [
    /\s|\\\r?\n/,
    $.comment,
  ],

  rules: {
    source_file: $ => repeat($._statement),

    _statement: $ => choice(
      $._expression_statement,
      $._declaration_statement
    ),

    _expression_statement: $ => seq(
      $._expression,
      SEMICOLON
    ),

    _expression: $ => choice(
      $.identifier,
      $.string_expression,
      $.number_expression,
      $.array_expression,
      $.dictionary_expression,
      $.option_dictionary_expression,
      $.class_creation_expression,
      $.attribute_expression,
      $.binary_expression,
      $.parenthesized_expression,
      $.null_expression,
      $.index_expression,
      $.unary_expression,
      $.update_expression,
    ),

    _declaration_statement: $ => choice(
      $.class_declaration,
      $.function_declaration,
      $.import_statement,
      $.return_statement,
      $.if_statement,
      $.while_statement,
      $.for_statement,
      $.try_statement,
      $.variable_declaration,
      $.variable_assingment,
    ),

    import_statement: $ => seq(
      choice('using', 'import'),
      choice($.dotted_name, $.aliased_import),
      SEMICOLON
    ),

    return_statement: $ => seq(
      choice(
        prec.left(seq('return', $._expression)),
        prec(-1, 'return'),
      ),
      SEMICOLON
    ),

    aliased_import: $ => seq(
      field('name', $.dotted_name),
      'as',
      field('alias', $.identifier)
    ),

    if_statement: $ => seq(
      'if',
      '(',
      field('condition', $._expression),
      ')',
      field('body', $.block),
      repeat(field('alternative', $.elif_clause)),
      optional(field('alternative', $.else_clause))
    ),

    elif_clause: $ => seq(
      'else if',
      '(',
      field('condition', $._expression),
      ')',
      field('body', $.block)
    ),

    else_clause: $ => seq(
      'else',
      field('body', $.block)
    ),

    for_statement: $ => seq(
      'for',
      '(',
      field('initializer', $.variable_declaration),
      field('condition', optional($._expression)), ';',
      field('update', optional($._expression)),
      ')',
      $.block,
    ),

    while_statement: $ => seq(
      'while',
      '(',
      field('condition', $._expression),
      ')',
      field('body', $.block)
    ),

    try_statement: $ => seq(
      'try',
      field('body', $.block),
      choice(
        seq(
          repeat1($.catch_clause),
          optional($.finally_clause)
        ),
        $.finally_clause
      )
    ),

    catch_clause: $ => seq(
      'catch',
      field('error',
        seq(
          '(',
          field('error_name', $._expression),
          optional(seq(
            'instanceof',
            field('class', $.dotted_name),
          )),
          ')',
        ),
      ),
      $.block
    ),

    finally_clause: $ => seq(
      'finally',
      field('body', $.block)
    ),


    class_declaration: $ => seq(
      'class',
      $.identifier,
      optional(seq(
        'extends',
        $.dotted_name,
      )),
      $.block,
    ),

    function_declaration: $ => seq(
      field('modifier', optional($._modifier)),
      'function',
      $.identifier,
      choice($.argument_list, $.empty_argument_list),
      optional(
        field('return_type',
          seq('as', $.identifier),
        )
      ),
      $.block
    ),

    variable_declaration: $ => seq(
      optional($._modifier),
      'var',
      $.identifier,
      optional(seq(
        '=',
        field('value', $._expression),
      )),
      SEMICOLON
    ),

    variable_assingment: $ => seq(
      $.identifier,
      '=',
      $._expression,
      SEMICOLON
    ),

    argument: $ => seq(
      $._expression,
      optional(seq(
        'as',
        field('type', $._type)
      ))
    ),

    empty_argument_list: $ => seq(
      '()'
    ),

    argument_list: $ => seq(
      '(',
      $.argument,
      optional(repeat(seq(',', $.argument))),
      ')'
    ),

    _type: $ => choice(
      'Boolean',
      'String',
      'Number',
      $.identifier
    ),

    block: $ => seq(
      '{',
      repeat($._statement),
      '}'
    ),

    null_expression: $ => "null",

    string_expression: $ => seq(
      '"',
      repeat(choice(
        token.immediate(prec(1, /[^\\"\n]+/)),
        $.escape_sequence
      )),
      '"',
    ),

    number_expression: $ => /\d+/,

     binary_expression: $ => {
      const table = [
        [PREC.and, '&&'],
        [PREC.or, '||'],
        [PREC.bitand, '&'],
        [PREC.bitor, '|'],
        [PREC.bitxor, '^'],
        [PREC.comparative, choice('==', '!=', '<', '<=', '>', '>=')],
        [PREC.shift, choice('<<', '>>')],
        [PREC.additive, choice('+', '-')],
        [PREC.multiplicative, choice('*', '/', '%')],
      ];

      return choice(...table.map(([precedence, operator]) => prec.left(precedence, seq(
        field('left', $._expression),
        field('operator', operator),
        field('right', $._expression),
      ))));
    },

    unary_expression: $ => prec.left(PREC.unary, seq(
      field('operator', choice('!', '~', '-', '+')),
      field('argument', $._expression)
    )),

    update_expression: $ => {
      const argument = field('argument', $._expression);
      const operator = field('operator', choice('--', '++'));
      return prec.right(PREC.unary, choice(
        seq(operator, argument),
        seq(argument, operator),
      ));
    },

    option_dictionary_expression: $ => seq(
      '{',
      repeat1(seq(':', $.identifier, '=>', $._expression, optional(','))),
      '}'
    ),

    dictionary_expression: $ => seq(
      '{',
      repeat1(seq($.string_expression, '=>', $._expression, optional(','))),
      '}'
    ),

    array_expression: $ => seq(
      '[',
      $._expression,
      optional(repeat(seq(',', $._expression))),
      ']',
    ),

    attribute_expression: $ => seq(
      field('object', $._expression),
      '.',
      field('attribute',
        choice(
          $.identifier,
          $.call,
        )
      ),
    ),

    index_expression: $ => prec(PREC.call, seq($._expression, '[', $._expression, ']')),

    class_creation_expression: $ => seq(
      'new',
      $.identifier,
      choice('()', $.argument_list)
    ),

    call: $ => prec(PREC.call, seq(
      field('function', $.identifier),
      field('arguments', choice(
        $.argument_list,
        $.empty_argument_list,
      ))
    )),

    parenthesized_expression: $ => prec(PREC.parenthesized_expression, seq(
      '(',
      $._expression,
      ')'
    )),

    escape_sequence: $ => token.immediate(
      seq('\\',
        choice(
          /[^xu]/,
          /u[0-9a-fA-F]{4}/,
          /u{[0-9a-fA-F]+}/,
          /x[0-9a-fA-F]{2}/
        )
      )),

    _modifier: $ => choice(
      'public',
      'private',
      'protected',
    ),

    // To much regex for me
    identifier: $ => /[_\p{XID_Start}][_\p{XID_Continue}]*/,

    // dotted_name: $ => choice(
    //   prec.left(seq($.identifier, optional(repeat(seq('.', $.identifier))))),
    //   prec(-1, $.identifier)
    // ),

    dotted_name: $ => seq($.identifier, optional(repeat(seq('.', $.identifier)))),

    comment: $ => token(choice(
      seq('//', /(\\(.|\r?\n)|[^\\\n])*/),
      seq(
        '/*',
        /[^*]*\*+([^/*][^*]*\*+)*/,
        '/'
      )
    )),

  }
});
