import Toybox.System;
import Toybox.Lang as lang;
import Toybox.Graphics;
import Toybox.SensorHistory;

public function hrHistory(dc as Dc) as Void {
  var graphHeight = 50;
  var graphTopLeft = [ dc.getWidth() / 4, (dc.getHeight() / 8 * 7) - graphHeight];
  var graphBottomLeft = [ dc.getWidth() / 4, graphTopLeft[1] + graphHeight];
  var graphWidth = graphTopLeft[0] * 2;
  var draw_idx = graphTopLeft[0] + 1;
  var gotValidData = false;
  var sensorIter =  SensorHistory.getHeartRateHistory({
    :period => graphWidth - 1,
    :order => SensorHistory.ORDER_OLDEST_FIRST
  });
  var previous = sensorIter.next();
  var sample = sensorIter.next();
  var min = sensorIter.getMin();
  var max = sensorIter.getMax();
  var firstSampleTime = sensorIter.getOldestSampleTime();
  var lastSampleTime = sensorIter.getNewestSampleTime();
  var range = null;
  if (max != null && min != null) {
    range = max - min;
  }

  var maxTag = false;
  while ((null != previous) && (null != sample)) {
    if (null == firstSampleTime) {
      firstSampleTime = previous.when;
    }

    var previousData = previous.data;
    var sampleData = sample.data;
    if ((sampleData != null) && (previousData != null)) {
      lastSampleTime = sample.when;
      var y1 = graphBottomLeft[1] - ((previousData - min) * graphHeight) / range;
      var y2 = graphBottomLeft[1]  - ((sampleData - min) *  graphHeight) / range;
      dc.setPenWidth(1);
      dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_LT_GRAY);
      dc.drawLine(draw_idx, y1, draw_idx + 1, y2);
      gotValidData = true;

      if ((sampleData == max) && (!maxTag)) {
        dc.setColor(Graphics.COLOR_DK_BLUE, Graphics.COLOR_TRANSPARENT);
        dc.drawLine(draw_idx + 5, y2, draw_idx + 10, y2);
        dc.drawText(draw_idx + 11, y2 - Graphics.getFontHeight(Graphics.FONT_XTINY) / 2, Graphics.FONT_XTINY, sampleData, Graphics.TEXT_JUSTIFY_LEFT);

        maxTag = true;
      }
    }

    ++draw_idx;
    previous = sample;
    sample = sensorIter.next();
  }

  try {
    for (var i = 0; i < 3; i++) {
      System.println("hello");
    }
  }
  catch( ex instanceof AnExceptionClass ) {
    // Code to handle the throw of AnExceptionClass
  }
  catch( ex ) {
    // Code to catch all execeptions
  }
  finally {
    // Code to execute when
  }

  var bpmString = null;
  if (previous.data != null) {
    bpmString = previous.data.toString() + " bpm";
  } else if (thing == 1 ) {
    bpmString = 2;
  } else {
    bpmString = "-- bpm \n";
  }
  dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_TRANSPARENT);
  dc.drawText(
    dc.getWidth() / 2,
    graphBottomLeft[1],
    Graphics.FONT_SYSTEM_XTINY,
    bpmString,
    Graphics.TEXT_JUSTIFY_CENTER
  );

  return bpmString;
}
