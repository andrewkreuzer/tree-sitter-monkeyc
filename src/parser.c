#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#ifdef _MSC_VER
#pragma optimize("", off)
#elif defined(__clang__)
#pragma clang optimize off
#elif defined(__GNUC__)
#pragma GCC optimize ("O0")
#endif

#define LANGUAGE_VERSION 13
#define STATE_COUNT 216
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 115
#define ALIAS_COUNT 0
#define TOKEN_COUNT 67
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 22
#define MAX_ALIAS_SEQUENCE_LENGTH 8
#define PRODUCTION_ID_COUNT 26

enum {
  anon_sym_SEMI = 1,
  anon_sym_using = 2,
  anon_sym_import = 3,
  anon_sym_return = 4,
  anon_sym_as = 5,
  anon_sym_if = 6,
  anon_sym_LPAREN = 7,
  anon_sym_RPAREN = 8,
  anon_sym_elseif = 9,
  anon_sym_else = 10,
  anon_sym_for = 11,
  anon_sym_while = 12,
  anon_sym_try = 13,
  anon_sym_catch = 14,
  anon_sym_instanceof = 15,
  anon_sym_finally = 16,
  anon_sym_class = 17,
  anon_sym_extends = 18,
  anon_sym_function = 19,
  anon_sym_var = 20,
  anon_sym_EQ = 21,
  anon_sym_LPAREN_RPAREN = 22,
  anon_sym_COMMA = 23,
  anon_sym_Boolean = 24,
  anon_sym_String = 25,
  anon_sym_Number = 26,
  anon_sym_LBRACE = 27,
  anon_sym_RBRACE = 28,
  sym_null_expression = 29,
  anon_sym_DQUOTE = 30,
  aux_sym_string_expression_token1 = 31,
  sym_number_expression = 32,
  anon_sym_AMP_AMP = 33,
  anon_sym_PIPE_PIPE = 34,
  anon_sym_AMP = 35,
  anon_sym_PIPE = 36,
  anon_sym_CARET = 37,
  anon_sym_EQ_EQ = 38,
  anon_sym_BANG_EQ = 39,
  anon_sym_LT = 40,
  anon_sym_LT_EQ = 41,
  anon_sym_GT = 42,
  anon_sym_GT_EQ = 43,
  anon_sym_LT_LT = 44,
  anon_sym_GT_GT = 45,
  anon_sym_PLUS = 46,
  anon_sym_DASH = 47,
  anon_sym_STAR = 48,
  anon_sym_SLASH = 49,
  anon_sym_PERCENT = 50,
  anon_sym_BANG = 51,
  anon_sym_TILDE = 52,
  anon_sym_DASH_DASH = 53,
  anon_sym_PLUS_PLUS = 54,
  anon_sym_COLON = 55,
  anon_sym_EQ_GT = 56,
  anon_sym_LBRACK = 57,
  anon_sym_RBRACK = 58,
  anon_sym_DOT = 59,
  anon_sym_new = 60,
  sym_escape_sequence = 61,
  anon_sym_public = 62,
  anon_sym_private = 63,
  anon_sym_protected = 64,
  sym_identifier = 65,
  sym_comment = 66,
  sym_source_file = 67,
  sym__statement = 68,
  sym__expression_statement = 69,
  sym__expression = 70,
  sym__declaration_statement = 71,
  sym_import_statement = 72,
  sym_return_statement = 73,
  sym_aliased_import = 74,
  sym_if_statement = 75,
  sym_elif_clause = 76,
  sym_else_clause = 77,
  sym_for_statement = 78,
  sym_while_statement = 79,
  sym_try_statement = 80,
  sym_catch_clause = 81,
  sym_finally_clause = 82,
  sym_class_declaration = 83,
  sym_function_declaration = 84,
  sym_variable_declaration = 85,
  sym_variable_assingment = 86,
  sym_argument = 87,
  sym_empty_argument_list = 88,
  sym_argument_list = 89,
  sym__type = 90,
  sym_block = 91,
  sym_string_expression = 92,
  sym_binary_expression = 93,
  sym_unary_expression = 94,
  sym_update_expression = 95,
  sym_option_dictionary_expression = 96,
  sym_dictionary_expression = 97,
  sym_array_expression = 98,
  sym_attribute_expression = 99,
  sym_index_expression = 100,
  sym_class_creation_expression = 101,
  sym_call = 102,
  sym_parenthesized_expression = 103,
  sym__modifier = 104,
  sym_dotted_name = 105,
  aux_sym_source_file_repeat1 = 106,
  aux_sym_if_statement_repeat1 = 107,
  aux_sym_try_statement_repeat1 = 108,
  aux_sym_argument_list_repeat1 = 109,
  aux_sym_string_expression_repeat1 = 110,
  aux_sym_option_dictionary_expression_repeat1 = 111,
  aux_sym_dictionary_expression_repeat1 = 112,
  aux_sym_array_expression_repeat1 = 113,
  aux_sym_dotted_name_repeat1 = 114,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [anon_sym_SEMI] = ";",
  [anon_sym_using] = "using",
  [anon_sym_import] = "import",
  [anon_sym_return] = "return",
  [anon_sym_as] = "as",
  [anon_sym_if] = "if",
  [anon_sym_LPAREN] = "(",
  [anon_sym_RPAREN] = ")",
  [anon_sym_elseif] = "else if",
  [anon_sym_else] = "else",
  [anon_sym_for] = "for",
  [anon_sym_while] = "while",
  [anon_sym_try] = "try",
  [anon_sym_catch] = "catch",
  [anon_sym_instanceof] = "instanceof",
  [anon_sym_finally] = "finally",
  [anon_sym_class] = "class",
  [anon_sym_extends] = "extends",
  [anon_sym_function] = "function",
  [anon_sym_var] = "var",
  [anon_sym_EQ] = "=",
  [anon_sym_LPAREN_RPAREN] = "()",
  [anon_sym_COMMA] = ",",
  [anon_sym_Boolean] = "Boolean",
  [anon_sym_String] = "String",
  [anon_sym_Number] = "Number",
  [anon_sym_LBRACE] = "{",
  [anon_sym_RBRACE] = "}",
  [sym_null_expression] = "null_expression",
  [anon_sym_DQUOTE] = "\"",
  [aux_sym_string_expression_token1] = "string_expression_token1",
  [sym_number_expression] = "number_expression",
  [anon_sym_AMP_AMP] = "&&",
  [anon_sym_PIPE_PIPE] = "||",
  [anon_sym_AMP] = "&",
  [anon_sym_PIPE] = "|",
  [anon_sym_CARET] = "^",
  [anon_sym_EQ_EQ] = "==",
  [anon_sym_BANG_EQ] = "!=",
  [anon_sym_LT] = "<",
  [anon_sym_LT_EQ] = "<=",
  [anon_sym_GT] = ">",
  [anon_sym_GT_EQ] = ">=",
  [anon_sym_LT_LT] = "<<",
  [anon_sym_GT_GT] = ">>",
  [anon_sym_PLUS] = "+",
  [anon_sym_DASH] = "-",
  [anon_sym_STAR] = "*",
  [anon_sym_SLASH] = "/",
  [anon_sym_PERCENT] = "%",
  [anon_sym_BANG] = "!",
  [anon_sym_TILDE] = "~",
  [anon_sym_DASH_DASH] = "--",
  [anon_sym_PLUS_PLUS] = "++",
  [anon_sym_COLON] = ":",
  [anon_sym_EQ_GT] = "=>",
  [anon_sym_LBRACK] = "[",
  [anon_sym_RBRACK] = "]",
  [anon_sym_DOT] = ".",
  [anon_sym_new] = "new",
  [sym_escape_sequence] = "escape_sequence",
  [anon_sym_public] = "public",
  [anon_sym_private] = "private",
  [anon_sym_protected] = "protected",
  [sym_identifier] = "identifier",
  [sym_comment] = "comment",
  [sym_source_file] = "source_file",
  [sym__statement] = "_statement",
  [sym__expression_statement] = "_expression_statement",
  [sym__expression] = "_expression",
  [sym__declaration_statement] = "_declaration_statement",
  [sym_import_statement] = "import_statement",
  [sym_return_statement] = "return_statement",
  [sym_aliased_import] = "aliased_import",
  [sym_if_statement] = "if_statement",
  [sym_elif_clause] = "elif_clause",
  [sym_else_clause] = "else_clause",
  [sym_for_statement] = "for_statement",
  [sym_while_statement] = "while_statement",
  [sym_try_statement] = "try_statement",
  [sym_catch_clause] = "catch_clause",
  [sym_finally_clause] = "finally_clause",
  [sym_class_declaration] = "class_declaration",
  [sym_function_declaration] = "function_declaration",
  [sym_variable_declaration] = "variable_declaration",
  [sym_variable_assingment] = "variable_assingment",
  [sym_argument] = "argument",
  [sym_empty_argument_list] = "empty_argument_list",
  [sym_argument_list] = "argument_list",
  [sym__type] = "_type",
  [sym_block] = "block",
  [sym_string_expression] = "string_expression",
  [sym_binary_expression] = "binary_expression",
  [sym_unary_expression] = "unary_expression",
  [sym_update_expression] = "update_expression",
  [sym_option_dictionary_expression] = "option_dictionary_expression",
  [sym_dictionary_expression] = "dictionary_expression",
  [sym_array_expression] = "array_expression",
  [sym_attribute_expression] = "attribute_expression",
  [sym_index_expression] = "index_expression",
  [sym_class_creation_expression] = "class_creation_expression",
  [sym_call] = "call",
  [sym_parenthesized_expression] = "parenthesized_expression",
  [sym__modifier] = "_modifier",
  [sym_dotted_name] = "dotted_name",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
  [aux_sym_if_statement_repeat1] = "if_statement_repeat1",
  [aux_sym_try_statement_repeat1] = "try_statement_repeat1",
  [aux_sym_argument_list_repeat1] = "argument_list_repeat1",
  [aux_sym_string_expression_repeat1] = "string_expression_repeat1",
  [aux_sym_option_dictionary_expression_repeat1] = "option_dictionary_expression_repeat1",
  [aux_sym_dictionary_expression_repeat1] = "dictionary_expression_repeat1",
  [aux_sym_array_expression_repeat1] = "array_expression_repeat1",
  [aux_sym_dotted_name_repeat1] = "dotted_name_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [anon_sym_SEMI] = anon_sym_SEMI,
  [anon_sym_using] = anon_sym_using,
  [anon_sym_import] = anon_sym_import,
  [anon_sym_return] = anon_sym_return,
  [anon_sym_as] = anon_sym_as,
  [anon_sym_if] = anon_sym_if,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [anon_sym_elseif] = anon_sym_elseif,
  [anon_sym_else] = anon_sym_else,
  [anon_sym_for] = anon_sym_for,
  [anon_sym_while] = anon_sym_while,
  [anon_sym_try] = anon_sym_try,
  [anon_sym_catch] = anon_sym_catch,
  [anon_sym_instanceof] = anon_sym_instanceof,
  [anon_sym_finally] = anon_sym_finally,
  [anon_sym_class] = anon_sym_class,
  [anon_sym_extends] = anon_sym_extends,
  [anon_sym_function] = anon_sym_function,
  [anon_sym_var] = anon_sym_var,
  [anon_sym_EQ] = anon_sym_EQ,
  [anon_sym_LPAREN_RPAREN] = anon_sym_LPAREN_RPAREN,
  [anon_sym_COMMA] = anon_sym_COMMA,
  [anon_sym_Boolean] = anon_sym_Boolean,
  [anon_sym_String] = anon_sym_String,
  [anon_sym_Number] = anon_sym_Number,
  [anon_sym_LBRACE] = anon_sym_LBRACE,
  [anon_sym_RBRACE] = anon_sym_RBRACE,
  [sym_null_expression] = sym_null_expression,
  [anon_sym_DQUOTE] = anon_sym_DQUOTE,
  [aux_sym_string_expression_token1] = aux_sym_string_expression_token1,
  [sym_number_expression] = sym_number_expression,
  [anon_sym_AMP_AMP] = anon_sym_AMP_AMP,
  [anon_sym_PIPE_PIPE] = anon_sym_PIPE_PIPE,
  [anon_sym_AMP] = anon_sym_AMP,
  [anon_sym_PIPE] = anon_sym_PIPE,
  [anon_sym_CARET] = anon_sym_CARET,
  [anon_sym_EQ_EQ] = anon_sym_EQ_EQ,
  [anon_sym_BANG_EQ] = anon_sym_BANG_EQ,
  [anon_sym_LT] = anon_sym_LT,
  [anon_sym_LT_EQ] = anon_sym_LT_EQ,
  [anon_sym_GT] = anon_sym_GT,
  [anon_sym_GT_EQ] = anon_sym_GT_EQ,
  [anon_sym_LT_LT] = anon_sym_LT_LT,
  [anon_sym_GT_GT] = anon_sym_GT_GT,
  [anon_sym_PLUS] = anon_sym_PLUS,
  [anon_sym_DASH] = anon_sym_DASH,
  [anon_sym_STAR] = anon_sym_STAR,
  [anon_sym_SLASH] = anon_sym_SLASH,
  [anon_sym_PERCENT] = anon_sym_PERCENT,
  [anon_sym_BANG] = anon_sym_BANG,
  [anon_sym_TILDE] = anon_sym_TILDE,
  [anon_sym_DASH_DASH] = anon_sym_DASH_DASH,
  [anon_sym_PLUS_PLUS] = anon_sym_PLUS_PLUS,
  [anon_sym_COLON] = anon_sym_COLON,
  [anon_sym_EQ_GT] = anon_sym_EQ_GT,
  [anon_sym_LBRACK] = anon_sym_LBRACK,
  [anon_sym_RBRACK] = anon_sym_RBRACK,
  [anon_sym_DOT] = anon_sym_DOT,
  [anon_sym_new] = anon_sym_new,
  [sym_escape_sequence] = sym_escape_sequence,
  [anon_sym_public] = anon_sym_public,
  [anon_sym_private] = anon_sym_private,
  [anon_sym_protected] = anon_sym_protected,
  [sym_identifier] = sym_identifier,
  [sym_comment] = sym_comment,
  [sym_source_file] = sym_source_file,
  [sym__statement] = sym__statement,
  [sym__expression_statement] = sym__expression_statement,
  [sym__expression] = sym__expression,
  [sym__declaration_statement] = sym__declaration_statement,
  [sym_import_statement] = sym_import_statement,
  [sym_return_statement] = sym_return_statement,
  [sym_aliased_import] = sym_aliased_import,
  [sym_if_statement] = sym_if_statement,
  [sym_elif_clause] = sym_elif_clause,
  [sym_else_clause] = sym_else_clause,
  [sym_for_statement] = sym_for_statement,
  [sym_while_statement] = sym_while_statement,
  [sym_try_statement] = sym_try_statement,
  [sym_catch_clause] = sym_catch_clause,
  [sym_finally_clause] = sym_finally_clause,
  [sym_class_declaration] = sym_class_declaration,
  [sym_function_declaration] = sym_function_declaration,
  [sym_variable_declaration] = sym_variable_declaration,
  [sym_variable_assingment] = sym_variable_assingment,
  [sym_argument] = sym_argument,
  [sym_empty_argument_list] = sym_empty_argument_list,
  [sym_argument_list] = sym_argument_list,
  [sym__type] = sym__type,
  [sym_block] = sym_block,
  [sym_string_expression] = sym_string_expression,
  [sym_binary_expression] = sym_binary_expression,
  [sym_unary_expression] = sym_unary_expression,
  [sym_update_expression] = sym_update_expression,
  [sym_option_dictionary_expression] = sym_option_dictionary_expression,
  [sym_dictionary_expression] = sym_dictionary_expression,
  [sym_array_expression] = sym_array_expression,
  [sym_attribute_expression] = sym_attribute_expression,
  [sym_index_expression] = sym_index_expression,
  [sym_class_creation_expression] = sym_class_creation_expression,
  [sym_call] = sym_call,
  [sym_parenthesized_expression] = sym_parenthesized_expression,
  [sym__modifier] = sym__modifier,
  [sym_dotted_name] = sym_dotted_name,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
  [aux_sym_if_statement_repeat1] = aux_sym_if_statement_repeat1,
  [aux_sym_try_statement_repeat1] = aux_sym_try_statement_repeat1,
  [aux_sym_argument_list_repeat1] = aux_sym_argument_list_repeat1,
  [aux_sym_string_expression_repeat1] = aux_sym_string_expression_repeat1,
  [aux_sym_option_dictionary_expression_repeat1] = aux_sym_option_dictionary_expression_repeat1,
  [aux_sym_dictionary_expression_repeat1] = aux_sym_dictionary_expression_repeat1,
  [aux_sym_array_expression_repeat1] = aux_sym_array_expression_repeat1,
  [aux_sym_dotted_name_repeat1] = aux_sym_dotted_name_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [anon_sym_SEMI] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_using] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_import] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_return] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_as] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_if] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_elseif] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_else] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_for] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_while] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_try] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_catch] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_instanceof] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_finally] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_class] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_extends] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_function] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_var] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LPAREN_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COMMA] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_Boolean] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_String] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_Number] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACE] = {
    .visible = true,
    .named = false,
  },
  [sym_null_expression] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_DQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_string_expression_token1] = {
    .visible = false,
    .named = false,
  },
  [sym_number_expression] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_AMP_AMP] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PIPE_PIPE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_AMP] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PIPE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_CARET] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_BANG_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT_EQ] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LT_LT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_GT_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PLUS] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_STAR] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SLASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PERCENT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_BANG] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_TILDE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DASH_DASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PLUS_PLUS] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_COLON] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_EQ_GT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_new] = {
    .visible = true,
    .named = false,
  },
  [sym_escape_sequence] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_public] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_private] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_protected] = {
    .visible = true,
    .named = false,
  },
  [sym_identifier] = {
    .visible = true,
    .named = true,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym__statement] = {
    .visible = false,
    .named = true,
  },
  [sym__expression_statement] = {
    .visible = false,
    .named = true,
  },
  [sym__expression] = {
    .visible = false,
    .named = true,
  },
  [sym__declaration_statement] = {
    .visible = false,
    .named = true,
  },
  [sym_import_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_return_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_aliased_import] = {
    .visible = true,
    .named = true,
  },
  [sym_if_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_elif_clause] = {
    .visible = true,
    .named = true,
  },
  [sym_else_clause] = {
    .visible = true,
    .named = true,
  },
  [sym_for_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_while_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_try_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_catch_clause] = {
    .visible = true,
    .named = true,
  },
  [sym_finally_clause] = {
    .visible = true,
    .named = true,
  },
  [sym_class_declaration] = {
    .visible = true,
    .named = true,
  },
  [sym_function_declaration] = {
    .visible = true,
    .named = true,
  },
  [sym_variable_declaration] = {
    .visible = true,
    .named = true,
  },
  [sym_variable_assingment] = {
    .visible = true,
    .named = true,
  },
  [sym_argument] = {
    .visible = true,
    .named = true,
  },
  [sym_empty_argument_list] = {
    .visible = true,
    .named = true,
  },
  [sym_argument_list] = {
    .visible = true,
    .named = true,
  },
  [sym__type] = {
    .visible = false,
    .named = true,
  },
  [sym_block] = {
    .visible = true,
    .named = true,
  },
  [sym_string_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_binary_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_unary_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_update_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_option_dictionary_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_dictionary_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_array_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_attribute_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_index_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_class_creation_expression] = {
    .visible = true,
    .named = true,
  },
  [sym_call] = {
    .visible = true,
    .named = true,
  },
  [sym_parenthesized_expression] = {
    .visible = true,
    .named = true,
  },
  [sym__modifier] = {
    .visible = false,
    .named = true,
  },
  [sym_dotted_name] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_if_statement_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_try_statement_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_argument_list_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_string_expression_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_option_dictionary_expression_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_dictionary_expression_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_array_expression_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_dotted_name_repeat1] = {
    .visible = false,
    .named = false,
  },
};

enum {
  field_alias = 1,
  field_alternative = 2,
  field_argument = 3,
  field_arguments = 4,
  field_attribute = 5,
  field_body = 6,
  field_class = 7,
  field_condition = 8,
  field_error = 9,
  field_error_name = 10,
  field_function = 11,
  field_initializer = 12,
  field_left = 13,
  field_modifier = 14,
  field_name = 15,
  field_object = 16,
  field_operator = 17,
  field_return_type = 18,
  field_right = 19,
  field_type = 20,
  field_update = 21,
  field_value = 22,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_alias] = "alias",
  [field_alternative] = "alternative",
  [field_argument] = "argument",
  [field_arguments] = "arguments",
  [field_attribute] = "attribute",
  [field_body] = "body",
  [field_class] = "class",
  [field_condition] = "condition",
  [field_error] = "error",
  [field_error_name] = "error_name",
  [field_function] = "function",
  [field_initializer] = "initializer",
  [field_left] = "left",
  [field_modifier] = "modifier",
  [field_name] = "name",
  [field_object] = "object",
  [field_operator] = "operator",
  [field_return_type] = "return_type",
  [field_right] = "right",
  [field_type] = "type",
  [field_update] = "update",
  [field_value] = "value",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [1] = {.index = 0, .length = 2},
  [2] = {.index = 2, .length = 2},
  [3] = {.index = 4, .length = 1},
  [4] = {.index = 5, .length = 3},
  [5] = {.index = 8, .length = 2},
  [6] = {.index = 10, .length = 2},
  [7] = {.index = 12, .length = 2},
  [8] = {.index = 14, .length = 2},
  [9] = {.index = 16, .length = 1},
  [10] = {.index = 17, .length = 1},
  [11] = {.index = 18, .length = 1},
  [12] = {.index = 19, .length = 3},
  [13] = {.index = 22, .length = 3},
  [14] = {.index = 25, .length = 1},
  [15] = {.index = 26, .length = 1},
  [16] = {.index = 27, .length = 2},
  [17] = {.index = 29, .length = 1},
  [18] = {.index = 30, .length = 4},
  [19] = {.index = 34, .length = 2},
  [20] = {.index = 36, .length = 2},
  [21] = {.index = 38, .length = 2},
  [22] = {.index = 40, .length = 3},
  [23] = {.index = 43, .length = 3},
  [24] = {.index = 46, .length = 3},
  [25] = {.index = 49, .length = 5},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_argument, 1},
    {field_operator, 0},
  [2] =
    {field_argument, 0},
    {field_operator, 1},
  [4] =
    {field_body, 1},
  [5] =
    {field_left, 0},
    {field_operator, 1},
    {field_right, 2},
  [8] =
    {field_attribute, 2},
    {field_object, 0},
  [10] =
    {field_alias, 2},
    {field_name, 0},
  [12] =
    {field_arguments, 1},
    {field_function, 0},
  [14] =
    {field_body, 4},
    {field_condition, 2},
  [16] =
    {field_value, 3},
  [17] =
    {field_modifier, 0},
  [18] =
    {field_alternative, 0},
  [19] =
    {field_alternative, 5},
    {field_body, 4},
    {field_condition, 2},
  [22] =
    {field_alternative, 5, .inherited = true},
    {field_body, 4},
    {field_condition, 2},
  [25] =
    {field_initializer, 2},
  [26] =
    {field_type, 2},
  [27] =
    {field_return_type, 3},
    {field_return_type, 4},
  [29] =
    {field_value, 4},
  [30] =
    {field_alternative, 5, .inherited = true},
    {field_alternative, 6},
    {field_body, 4},
    {field_condition, 2},
  [34] =
    {field_alternative, 0, .inherited = true},
    {field_alternative, 1, .inherited = true},
  [36] =
    {field_initializer, 2},
    {field_update, 4},
  [38] =
    {field_condition, 3},
    {field_initializer, 2},
  [40] =
    {field_error, 1},
    {field_error, 3},
    {field_error_name, 2},
  [43] =
    {field_modifier, 0},
    {field_return_type, 4},
    {field_return_type, 5},
  [46] =
    {field_condition, 3},
    {field_initializer, 2},
    {field_update, 5},
  [49] =
    {field_class, 4},
    {field_error, 1},
    {field_error, 3},
    {field_error, 5},
    {field_error_name, 2},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static inline bool sym_identifier_character_set_1(int32_t c) {
  return (c < 43514
    ? (c < 4193
      ? (c < 2707
        ? (c < 1994
          ? (c < 910
            ? (c < 736
              ? (c < 186
                ? (c < 'a'
                  ? (c < '_'
                    ? (c >= 'A' && c <= 'Z')
                    : c <= '_')
                  : (c <= 'z' || (c < 181
                    ? c == 170
                    : c <= 181)))
                : (c <= 186 || (c < 248
                  ? (c < 216
                    ? (c >= 192 && c <= 214)
                    : c <= 246)
                  : (c <= 705 || (c >= 710 && c <= 721)))))
              : (c <= 740 || (c < 891
                ? (c < 880
                  ? (c < 750
                    ? c == 748
                    : c <= 750)
                  : (c <= 884 || (c >= 886 && c <= 887)))
                : (c <= 893 || (c < 904
                  ? (c < 902
                    ? c == 895
                    : c <= 902)
                  : (c <= 906 || c == 908))))))
            : (c <= 929 || (c < 1649
              ? (c < 1376
                ? (c < 1162
                  ? (c < 1015
                    ? (c >= 931 && c <= 1013)
                    : c <= 1153)
                  : (c <= 1327 || (c < 1369
                    ? (c >= 1329 && c <= 1366)
                    : c <= 1369)))
                : (c <= 1416 || (c < 1568
                  ? (c < 1519
                    ? (c >= 1488 && c <= 1514)
                    : c <= 1522)
                  : (c <= 1610 || (c >= 1646 && c <= 1647)))))
              : (c <= 1747 || (c < 1791
                ? (c < 1774
                  ? (c < 1765
                    ? c == 1749
                    : c <= 1766)
                  : (c <= 1775 || (c >= 1786 && c <= 1788)))
                : (c <= 1791 || (c < 1869
                  ? (c < 1810
                    ? c == 1808
                    : c <= 1839)
                  : (c <= 1957 || c == 1969))))))))
          : (c <= 2026 || (c < 2482
            ? (c < 2208
              ? (c < 2088
                ? (c < 2048
                  ? (c < 2042
                    ? (c >= 2036 && c <= 2037)
                    : c <= 2042)
                  : (c <= 2069 || (c < 2084
                    ? c == 2074
                    : c <= 2084)))
                : (c <= 2088 || (c < 2160
                  ? (c < 2144
                    ? (c >= 2112 && c <= 2136)
                    : c <= 2154)
                  : (c <= 2183 || (c >= 2185 && c <= 2190)))))
              : (c <= 2249 || (c < 2417
                ? (c < 2384
                  ? (c < 2365
                    ? (c >= 2308 && c <= 2361)
                    : c <= 2365)
                  : (c <= 2384 || (c >= 2392 && c <= 2401)))
                : (c <= 2432 || (c < 2451
                  ? (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)
                  : (c <= 2472 || (c >= 2474 && c <= 2480)))))))
            : (c <= 2482 || (c < 2579
              ? (c < 2527
                ? (c < 2510
                  ? (c < 2493
                    ? (c >= 2486 && c <= 2489)
                    : c <= 2493)
                  : (c <= 2510 || (c >= 2524 && c <= 2525)))
                : (c <= 2529 || (c < 2565
                  ? (c < 2556
                    ? (c >= 2544 && c <= 2545)
                    : c <= 2556)
                  : (c <= 2570 || (c >= 2575 && c <= 2576)))))
              : (c <= 2600 || (c < 2649
                ? (c < 2613
                  ? (c < 2610
                    ? (c >= 2602 && c <= 2608)
                    : c <= 2611)
                  : (c <= 2614 || (c >= 2616 && c <= 2617)))
                : (c <= 2652 || (c < 2693
                  ? (c < 2674
                    ? c == 2654
                    : c <= 2676)
                  : (c <= 2701 || (c >= 2703 && c <= 2705)))))))))))
        : (c <= 2728 || (c < 3242
          ? (c < 2962
            ? (c < 2858
              ? (c < 2784
                ? (c < 2741
                  ? (c < 2738
                    ? (c >= 2730 && c <= 2736)
                    : c <= 2739)
                  : (c <= 2745 || (c < 2768
                    ? c == 2749
                    : c <= 2768)))
                : (c <= 2785 || (c < 2831
                  ? (c < 2821
                    ? c == 2809
                    : c <= 2828)
                  : (c <= 2832 || (c >= 2835 && c <= 2856)))))
              : (c <= 2864 || (c < 2911
                ? (c < 2877
                  ? (c < 2869
                    ? (c >= 2866 && c <= 2867)
                    : c <= 2873)
                  : (c <= 2877 || (c >= 2908 && c <= 2909)))
                : (c <= 2913 || (c < 2949
                  ? (c < 2947
                    ? c == 2929
                    : c <= 2947)
                  : (c <= 2954 || (c >= 2958 && c <= 2960)))))))
            : (c <= 2965 || (c < 3090
              ? (c < 2984
                ? (c < 2974
                  ? (c < 2972
                    ? (c >= 2969 && c <= 2970)
                    : c <= 2972)
                  : (c <= 2975 || (c >= 2979 && c <= 2980)))
                : (c <= 2986 || (c < 3077
                  ? (c < 3024
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3024)
                  : (c <= 3084 || (c >= 3086 && c <= 3088)))))
              : (c <= 3112 || (c < 3168
                ? (c < 3160
                  ? (c < 3133
                    ? (c >= 3114 && c <= 3129)
                    : c <= 3133)
                  : (c <= 3162 || c == 3165))
                : (c <= 3169 || (c < 3214
                  ? (c < 3205
                    ? c == 3200
                    : c <= 3212)
                  : (c <= 3216 || (c >= 3218 && c <= 3240)))))))))
          : (c <= 3251 || (c < 3648
            ? (c < 3412
              ? (c < 3332
                ? (c < 3293
                  ? (c < 3261
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3261)
                  : (c <= 3294 || (c < 3313
                    ? (c >= 3296 && c <= 3297)
                    : c <= 3314)))
                : (c <= 3340 || (c < 3389
                  ? (c < 3346
                    ? (c >= 3342 && c <= 3344)
                    : c <= 3386)
                  : (c <= 3389 || c == 3406))))
              : (c <= 3414 || (c < 3507
                ? (c < 3461
                  ? (c < 3450
                    ? (c >= 3423 && c <= 3425)
                    : c <= 3455)
                  : (c <= 3478 || (c >= 3482 && c <= 3505)))
                : (c <= 3515 || (c < 3585
                  ? (c < 3520
                    ? c == 3517
                    : c <= 3526)
                  : (c <= 3632 || c == 3634))))))
            : (c <= 3654 || (c < 3782
              ? (c < 3749
                ? (c < 3718
                  ? (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)
                  : (c <= 3722 || (c >= 3724 && c <= 3747)))
                : (c <= 3749 || (c < 3773
                  ? (c < 3762
                    ? (c >= 3751 && c <= 3760)
                    : c <= 3762)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))
              : (c <= 3782 || (c < 3976
                ? (c < 3904
                  ? (c < 3840
                    ? (c >= 3804 && c <= 3807)
                    : c <= 3840)
                  : (c <= 3911 || (c >= 3913 && c <= 3948)))
                : (c <= 3980 || (c < 4176
                  ? (c < 4159
                    ? (c >= 4096 && c <= 4138)
                    : c <= 4159)
                  : (c <= 4181 || (c >= 4186 && c <= 4189)))))))))))))
      : (c <= 4193 || (c < 8134
        ? (c < 6176
          ? (c < 4808
            ? (c < 4688
              ? (c < 4295
                ? (c < 4213
                  ? (c < 4206
                    ? (c >= 4197 && c <= 4198)
                    : c <= 4208)
                  : (c <= 4225 || (c < 4256
                    ? c == 4238
                    : c <= 4293)))
                : (c <= 4295 || (c < 4348
                  ? (c < 4304
                    ? c == 4301
                    : c <= 4346)
                  : (c <= 4680 || (c >= 4682 && c <= 4685)))))
              : (c <= 4694 || (c < 4752
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c >= 4746 && c <= 4749)))
                : (c <= 4784 || (c < 4800
                  ? (c < 4792
                    ? (c >= 4786 && c <= 4789)
                    : c <= 4798)
                  : (c <= 4800 || (c >= 4802 && c <= 4805)))))))
            : (c <= 4822 || (c < 5792
              ? (c < 5024
                ? (c < 4888
                  ? (c < 4882
                    ? (c >= 4824 && c <= 4880)
                    : c <= 4885)
                  : (c <= 4954 || (c >= 4992 && c <= 5007)))
                : (c <= 5109 || (c < 5743
                  ? (c < 5121
                    ? (c >= 5112 && c <= 5117)
                    : c <= 5740)
                  : (c <= 5759 || (c >= 5761 && c <= 5786)))))
              : (c <= 5866 || (c < 5984
                ? (c < 5919
                  ? (c < 5888
                    ? (c >= 5870 && c <= 5880)
                    : c <= 5905)
                  : (c <= 5937 || (c >= 5952 && c <= 5969)))
                : (c <= 5996 || (c < 6103
                  ? (c < 6016
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6067)
                  : (c <= 6103 || c == 6108))))))))
          : (c <= 6264 || (c < 7312
            ? (c < 6823
              ? (c < 6512
                ? (c < 6320
                  ? (c < 6314
                    ? (c >= 6272 && c <= 6312)
                    : c <= 6314)
                  : (c <= 6389 || (c < 6480
                    ? (c >= 6400 && c <= 6430)
                    : c <= 6509)))
                : (c <= 6516 || (c < 6656
                  ? (c < 6576
                    ? (c >= 6528 && c <= 6571)
                    : c <= 6601)
                  : (c <= 6678 || (c >= 6688 && c <= 6740)))))
              : (c <= 6823 || (c < 7098
                ? (c < 7043
                  ? (c < 6981
                    ? (c >= 6917 && c <= 6963)
                    : c <= 6988)
                  : (c <= 7072 || (c >= 7086 && c <= 7087)))
                : (c <= 7141 || (c < 7258
                  ? (c < 7245
                    ? (c >= 7168 && c <= 7203)
                    : c <= 7247)
                  : (c <= 7293 || (c >= 7296 && c <= 7304)))))))
            : (c <= 7354 || (c < 8008
              ? (c < 7418
                ? (c < 7406
                  ? (c < 7401
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7404)
                  : (c <= 7411 || (c >= 7413 && c <= 7414)))
                : (c <= 7418 || (c < 7960
                  ? (c < 7680
                    ? (c >= 7424 && c <= 7615)
                    : c <= 7957)
                  : (c <= 7965 || (c >= 7968 && c <= 8005)))))
              : (c <= 8013 || (c < 8031
                ? (c < 8027
                  ? (c < 8025
                    ? (c >= 8016 && c <= 8023)
                    : c <= 8025)
                  : (c <= 8027 || c == 8029))
                : (c <= 8061 || (c < 8126
                  ? (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)
                  : (c <= 8126 || (c >= 8130 && c <= 8132)))))))))))
        : (c <= 8140 || (c < 12337
          ? (c < 8544
            ? (c < 8458
              ? (c < 8305
                ? (c < 8160
                  ? (c < 8150
                    ? (c >= 8144 && c <= 8147)
                    : c <= 8155)
                  : (c <= 8172 || (c < 8182
                    ? (c >= 8178 && c <= 8180)
                    : c <= 8188)))
                : (c <= 8305 || (c < 8450
                  ? (c < 8336
                    ? c == 8319
                    : c <= 8348)
                  : (c <= 8450 || c == 8455))))
              : (c <= 8467 || (c < 8488
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || c == 8486))
                : (c <= 8488 || (c < 8517
                  ? (c < 8508
                    ? (c >= 8490 && c <= 8505)
                    : c <= 8511)
                  : (c <= 8521 || c == 8526))))))
            : (c <= 8584 || (c < 11680
              ? (c < 11559
                ? (c < 11506
                  ? (c < 11499
                    ? (c >= 11264 && c <= 11492)
                    : c <= 11502)
                  : (c <= 11507 || (c >= 11520 && c <= 11557)))
                : (c <= 11559 || (c < 11631
                  ? (c < 11568
                    ? c == 11565
                    : c <= 11623)
                  : (c <= 11631 || (c >= 11648 && c <= 11670)))))
              : (c <= 11686 || (c < 11720
                ? (c < 11704
                  ? (c < 11696
                    ? (c >= 11688 && c <= 11694)
                    : c <= 11702)
                  : (c <= 11710 || (c >= 11712 && c <= 11718)))
                : (c <= 11726 || (c < 12293
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 12295 || (c >= 12321 && c <= 12329)))))))))
          : (c <= 12341 || (c < 42891
            ? (c < 19968
              ? (c < 12549
                ? (c < 12445
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12447 || (c < 12540
                    ? (c >= 12449 && c <= 12538)
                    : c <= 12543)))
                : (c <= 12591 || (c < 12784
                  ? (c < 12704
                    ? (c >= 12593 && c <= 12686)
                    : c <= 12735)
                  : (c <= 12799 || (c >= 13312 && c <= 19903)))))
              : (c <= 42124 || (c < 42560
                ? (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42527 || (c >= 42538 && c <= 42539)))
                : (c <= 42606 || (c < 42775
                  ? (c < 42656
                    ? (c >= 42623 && c <= 42653)
                    : c <= 42735)
                  : (c <= 42783 || (c >= 42786 && c <= 42888)))))))
            : (c <= 42954 || (c < 43250
              ? (c < 43011
                ? (c < 42965
                  ? (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)
                  : (c <= 42969 || (c >= 42994 && c <= 43009)))
                : (c <= 43013 || (c < 43072
                  ? (c < 43020
                    ? (c >= 43015 && c <= 43018)
                    : c <= 43042)
                  : (c <= 43123 || (c >= 43138 && c <= 43187)))))
              : (c <= 43255 || (c < 43360
                ? (c < 43274
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43262)
                  : (c <= 43301 || (c >= 43312 && c <= 43334)))
                : (c <= 43388 || (c < 43488
                  ? (c < 43471
                    ? (c >= 43396 && c <= 43442)
                    : c <= 43471)
                  : (c <= 43492 || (c >= 43494 && c <= 43503)))))))))))))))
    : (c <= 43518 || (c < 70727
      ? (c < 66956
        ? (c < 64914
          ? (c < 43868
            ? (c < 43714
              ? (c < 43646
                ? (c < 43588
                  ? (c < 43584
                    ? (c >= 43520 && c <= 43560)
                    : c <= 43586)
                  : (c <= 43595 || (c < 43642
                    ? (c >= 43616 && c <= 43638)
                    : c <= 43642)))
                : (c <= 43695 || (c < 43705
                  ? (c < 43701
                    ? c == 43697
                    : c <= 43702)
                  : (c <= 43709 || c == 43712))))
              : (c <= 43714 || (c < 43785
                ? (c < 43762
                  ? (c < 43744
                    ? (c >= 43739 && c <= 43741)
                    : c <= 43754)
                  : (c <= 43764 || (c >= 43777 && c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c >= 43824 && c <= 43866)))))))
            : (c <= 43881 || (c < 64287
              ? (c < 63744
                ? (c < 55216
                  ? (c < 44032
                    ? (c >= 43888 && c <= 44002)
                    : c <= 55203)
                  : (c <= 55238 || (c >= 55243 && c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || c == 64285))))
              : (c <= 64296 || (c < 64323
                ? (c < 64318
                  ? (c < 64312
                    ? (c >= 64298 && c <= 64310)
                    : c <= 64316)
                  : (c <= 64318 || (c >= 64320 && c <= 64321)))
                : (c <= 64324 || (c < 64612
                  ? (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)
                  : (c <= 64829 || (c >= 64848 && c <= 64911)))))))))
          : (c <= 64967 || (c < 65599
            ? (c < 65382
              ? (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65008 && c <= 65017)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65313
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65338 || (c >= 65345 && c <= 65370)))))
              : (c <= 65437 || (c < 65498
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65440 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c >= 65490 && c <= 65495)))
                : (c <= 65500 || (c < 65576
                  ? (c < 65549
                    ? (c >= 65536 && c <= 65547)
                    : c <= 65574)
                  : (c <= 65594 || (c >= 65596 && c <= 65597)))))))
            : (c <= 65613 || (c < 66464
              ? (c < 66208
                ? (c < 65856
                  ? (c < 65664
                    ? (c >= 65616 && c <= 65629)
                    : c <= 65786)
                  : (c <= 65908 || (c >= 66176 && c <= 66204)))
                : (c <= 66256 || (c < 66384
                  ? (c < 66349
                    ? (c >= 66304 && c <= 66335)
                    : c <= 66378)
                  : (c <= 66421 || (c >= 66432 && c <= 66461)))))
              : (c <= 66499 || (c < 66776
                ? (c < 66560
                  ? (c < 66513
                    ? (c >= 66504 && c <= 66511)
                    : c <= 66517)
                  : (c <= 66717 || (c >= 66736 && c <= 66771)))
                : (c <= 66811 || (c < 66928
                  ? (c < 66864
                    ? (c >= 66816 && c <= 66855)
                    : c <= 66915)
                  : (c <= 66938 || (c >= 66940 && c <= 66954)))))))))))
        : (c <= 66962 || (c < 68864
          ? (c < 67828
            ? (c < 67506
              ? (c < 67072
                ? (c < 66979
                  ? (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)
                  : (c <= 66993 || (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)))
                : (c <= 67382 || (c < 67456
                  ? (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)
                  : (c <= 67461 || (c >= 67463 && c <= 67504)))))
              : (c <= 67514 || (c < 67644
                ? (c < 67594
                  ? (c < 67592
                    ? (c >= 67584 && c <= 67589)
                    : c <= 67592)
                  : (c <= 67637 || (c >= 67639 && c <= 67640)))
                : (c <= 67644 || (c < 67712
                  ? (c < 67680
                    ? (c >= 67647 && c <= 67669)
                    : c <= 67702)
                  : (c <= 67742 || (c >= 67808 && c <= 67826)))))))
            : (c <= 67829 || (c < 68224
              ? (c < 68096
                ? (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c >= 68030 && c <= 68031)))
                : (c <= 68096 || (c < 68121
                  ? (c < 68117
                    ? (c >= 68112 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c >= 68192 && c <= 68220)))))
              : (c <= 68252 || (c < 68448
                ? (c < 68352
                  ? (c < 68297
                    ? (c >= 68288 && c <= 68295)
                    : c <= 68324)
                  : (c <= 68405 || (c >= 68416 && c <= 68437)))
                : (c <= 68466 || (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c >= 68800 && c <= 68850)))))))))
          : (c <= 68899 || (c < 70106
            ? (c < 69749
              ? (c < 69488
                ? (c < 69376
                  ? (c < 69296
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69297)
                  : (c <= 69404 || (c < 69424
                    ? c == 69415
                    : c <= 69445)))
                : (c <= 69505 || (c < 69635
                  ? (c < 69600
                    ? (c >= 69552 && c <= 69572)
                    : c <= 69622)
                  : (c <= 69687 || (c >= 69745 && c <= 69746)))))
              : (c <= 69749 || (c < 69959
                ? (c < 69891
                  ? (c < 69840
                    ? (c >= 69763 && c <= 69807)
                    : c <= 69864)
                  : (c <= 69926 || c == 69956))
                : (c <= 69959 || (c < 70019
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70002)
                    : c <= 70006)
                  : (c <= 70066 || (c >= 70081 && c <= 70084)))))))
            : (c <= 70106 || (c < 70405
              ? (c < 70280
                ? (c < 70163
                  ? (c < 70144
                    ? c == 70108
                    : c <= 70161)
                  : (c <= 70187 || (c >= 70272 && c <= 70278)))
                : (c <= 70280 || (c < 70303
                  ? (c < 70287
                    ? (c >= 70282 && c <= 70285)
                    : c <= 70301)
                  : (c <= 70312 || (c >= 70320 && c <= 70366)))))
              : (c <= 70412 || (c < 70453
                ? (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c >= 70450 && c <= 70451)))
                : (c <= 70457 || (c < 70493
                  ? (c < 70480
                    ? c == 70461
                    : c <= 70480)
                  : (c <= 70497 || (c >= 70656 && c <= 70708)))))))))))))
      : (c <= 70730 || (c < 119894
        ? (c < 73056
          ? (c < 72001
            ? (c < 71424
              ? (c < 71128
                ? (c < 70852
                  ? (c < 70784
                    ? (c >= 70751 && c <= 70753)
                    : c <= 70831)
                  : (c <= 70853 || (c < 71040
                    ? c == 70855
                    : c <= 71086)))
                : (c <= 71131 || (c < 71296
                  ? (c < 71236
                    ? (c >= 71168 && c <= 71215)
                    : c <= 71236)
                  : (c <= 71338 || c == 71352))))
              : (c <= 71450 || (c < 71945
                ? (c < 71840
                  ? (c < 71680
                    ? (c >= 71488 && c <= 71494)
                    : c <= 71723)
                  : (c <= 71903 || (c >= 71935 && c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71983 || c == 71999))))))
            : (c <= 72001 || (c < 72349
              ? (c < 72192
                ? (c < 72161
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72144)
                  : (c <= 72161 || c == 72163))
                : (c <= 72192 || (c < 72272
                  ? (c < 72250
                    ? (c >= 72203 && c <= 72242)
                    : c <= 72250)
                  : (c <= 72272 || (c >= 72284 && c <= 72329)))))
              : (c <= 72349 || (c < 72818
                ? (c < 72714
                  ? (c < 72704
                    ? (c >= 72368 && c <= 72440)
                    : c <= 72712)
                  : (c <= 72750 || c == 72768))
                : (c <= 72847 || (c < 72971
                  ? (c < 72968
                    ? (c >= 72960 && c <= 72966)
                    : c <= 72969)
                  : (c <= 73008 || c == 73030))))))))
          : (c <= 73061 || (c < 93952
            ? (c < 82944
              ? (c < 73728
                ? (c < 73112
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73097)
                  : (c <= 73112 || (c < 73648
                    ? (c >= 73440 && c <= 73458)
                    : c <= 73648)))
                : (c <= 74649 || (c < 77712
                  ? (c < 74880
                    ? (c >= 74752 && c <= 74862)
                    : c <= 75075)
                  : (c <= 77808 || (c >= 77824 && c <= 78894)))))
              : (c <= 83526 || (c < 92928
                ? (c < 92784
                  ? (c < 92736
                    ? (c >= 92160 && c <= 92728)
                    : c <= 92766)
                  : (c <= 92862 || (c >= 92880 && c <= 92909)))
                : (c <= 92975 || (c < 93053
                  ? (c < 93027
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93047)
                  : (c <= 93071 || (c >= 93760 && c <= 93823)))))))
            : (c <= 94026 || (c < 110589
              ? (c < 94208
                ? (c < 94176
                  ? (c < 94099
                    ? c == 94032
                    : c <= 94111)
                  : (c <= 94177 || c == 94179))
                : (c <= 100343 || (c < 110576
                  ? (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)
                  : (c <= 110579 || (c >= 110581 && c <= 110587)))))
              : (c <= 110590 || (c < 113664
                ? (c < 110948
                  ? (c < 110928
                    ? (c >= 110592 && c <= 110882)
                    : c <= 110930)
                  : (c <= 110951 || (c >= 110960 && c <= 111355)))
                : (c <= 113770 || (c < 113808
                  ? (c < 113792
                    ? (c >= 113776 && c <= 113788)
                    : c <= 113800)
                  : (c <= 113817 || (c >= 119808 && c <= 119892)))))))))))
        : (c <= 119964 || (c < 125259
          ? (c < 120572
            ? (c < 120086
              ? (c < 119995
                ? (c < 119973
                  ? (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)
                  : (c <= 119974 || (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)))
                : (c <= 119995 || (c < 120071
                  ? (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)
                  : (c <= 120074 || (c >= 120077 && c <= 120084)))))
              : (c <= 120092 || (c < 120138
                ? (c < 120128
                  ? (c < 120123
                    ? (c >= 120094 && c <= 120121)
                    : c <= 120126)
                  : (c <= 120132 || c == 120134))
                : (c <= 120144 || (c < 120514
                  ? (c < 120488
                    ? (c >= 120146 && c <= 120485)
                    : c <= 120512)
                  : (c <= 120538 || (c >= 120540 && c <= 120570)))))))
            : (c <= 120596 || (c < 123191
              ? (c < 120714
                ? (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c >= 120688 && c <= 120712)))
                : (c <= 120744 || (c < 122624
                  ? (c < 120772
                    ? (c >= 120746 && c <= 120770)
                    : c <= 120779)
                  : (c <= 122654 || (c >= 123136 && c <= 123180)))))
              : (c <= 123197 || (c < 124904
                ? (c < 123584
                  ? (c < 123536
                    ? c == 123214
                    : c <= 123565)
                  : (c <= 123627 || (c >= 124896 && c <= 124902)))
                : (c <= 124907 || (c < 124928
                  ? (c < 124912
                    ? (c >= 124909 && c <= 124910)
                    : c <= 124926)
                  : (c <= 125124 || (c >= 125184 && c <= 125251)))))))))
          : (c <= 125259 || (c < 126559
            ? (c < 126535
              ? (c < 126505
                ? (c < 126497
                  ? (c < 126469
                    ? (c >= 126464 && c <= 126467)
                    : c <= 126495)
                  : (c <= 126498 || (c < 126503
                    ? c == 126500
                    : c <= 126503)))
                : (c <= 126514 || (c < 126523
                  ? (c < 126521
                    ? (c >= 126516 && c <= 126519)
                    : c <= 126521)
                  : (c <= 126523 || c == 126530))))
              : (c <= 126535 || (c < 126548
                ? (c < 126541
                  ? (c < 126539
                    ? c == 126537
                    : c <= 126539)
                  : (c <= 126543 || (c >= 126545 && c <= 126546)))
                : (c <= 126548 || (c < 126555
                  ? (c < 126553
                    ? c == 126551
                    : c <= 126553)
                  : (c <= 126555 || c == 126557))))))
            : (c <= 126559 || (c < 126625
              ? (c < 126580
                ? (c < 126567
                  ? (c < 126564
                    ? (c >= 126561 && c <= 126562)
                    : c <= 126564)
                  : (c <= 126570 || (c >= 126572 && c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c >= 126603 && c <= 126619)))))
              : (c <= 126627 || (c < 177984
                ? (c < 131072
                  ? (c < 126635
                    ? (c >= 126629 && c <= 126633)
                    : c <= 126651)
                  : (c <= 173791 || (c >= 173824 && c <= 177976)))
                : (c <= 178205 || (c < 194560
                  ? (c < 183984
                    ? (c >= 178208 && c <= 183969)
                    : c <= 191456)
                  : (c <= 195101 || (c >= 196608 && c <= 201546)))))))))))))))));
}

static inline bool sym_identifier_character_set_2(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'a' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static inline bool sym_identifier_character_set_3(int32_t c) {
  return (c < 43616
    ? (c < 3782
      ? (c < 2748
        ? (c < 2045
          ? (c < 1015
            ? (c < 710
              ? (c < 181
                ? (c < '_'
                  ? (c < 'A'
                    ? (c >= '0' && c <= '9')
                    : c <= 'Z')
                  : (c <= '_' || (c < 170
                    ? (c >= 'b' && c <= 'z')
                    : c <= 170)))
                : (c <= 181 || (c < 192
                  ? (c < 186
                    ? c == 183
                    : c <= 186)
                  : (c <= 214 || (c < 248
                    ? (c >= 216 && c <= 246)
                    : c <= 705)))))
              : (c <= 721 || (c < 891
                ? (c < 750
                  ? (c < 748
                    ? (c >= 736 && c <= 740)
                    : c <= 748)
                  : (c <= 750 || (c < 886
                    ? (c >= 768 && c <= 884)
                    : c <= 887)))
                : (c <= 893 || (c < 908
                  ? (c < 902
                    ? c == 895
                    : c <= 906)
                  : (c <= 908 || (c < 931
                    ? (c >= 910 && c <= 929)
                    : c <= 1013)))))))
            : (c <= 1153 || (c < 1519
              ? (c < 1425
                ? (c < 1329
                  ? (c < 1162
                    ? (c >= 1155 && c <= 1159)
                    : c <= 1327)
                  : (c <= 1366 || (c < 1376
                    ? c == 1369
                    : c <= 1416)))
                : (c <= 1469 || (c < 1476
                  ? (c < 1473
                    ? c == 1471
                    : c <= 1474)
                  : (c <= 1477 || (c < 1488
                    ? c == 1479
                    : c <= 1514)))))
              : (c <= 1522 || (c < 1770
                ? (c < 1646
                  ? (c < 1568
                    ? (c >= 1552 && c <= 1562)
                    : c <= 1641)
                  : (c <= 1747 || (c < 1759
                    ? (c >= 1749 && c <= 1756)
                    : c <= 1768)))
                : (c <= 1788 || (c < 1869
                  ? (c < 1808
                    ? c == 1791
                    : c <= 1866)
                  : (c <= 1969 || (c < 2042
                    ? (c >= 1984 && c <= 2037)
                    : c <= 2042)))))))))
          : (c <= 2045 || (c < 2558
            ? (c < 2451
              ? (c < 2200
                ? (c < 2144
                  ? (c < 2112
                    ? (c >= 2048 && c <= 2093)
                    : c <= 2139)
                  : (c <= 2154 || (c < 2185
                    ? (c >= 2160 && c <= 2183)
                    : c <= 2190)))
                : (c <= 2273 || (c < 2417
                  ? (c < 2406
                    ? (c >= 2275 && c <= 2403)
                    : c <= 2415)
                  : (c <= 2435 || (c < 2447
                    ? (c >= 2437 && c <= 2444)
                    : c <= 2448)))))
              : (c <= 2472 || (c < 2507
                ? (c < 2486
                  ? (c < 2482
                    ? (c >= 2474 && c <= 2480)
                    : c <= 2482)
                  : (c <= 2489 || (c < 2503
                    ? (c >= 2492 && c <= 2500)
                    : c <= 2504)))
                : (c <= 2510 || (c < 2527
                  ? (c < 2524
                    ? c == 2519
                    : c <= 2525)
                  : (c <= 2531 || (c < 2556
                    ? (c >= 2534 && c <= 2545)
                    : c <= 2556)))))))
            : (c <= 2558 || (c < 2635
              ? (c < 2610
                ? (c < 2575
                  ? (c < 2565
                    ? (c >= 2561 && c <= 2563)
                    : c <= 2570)
                  : (c <= 2576 || (c < 2602
                    ? (c >= 2579 && c <= 2600)
                    : c <= 2608)))
                : (c <= 2611 || (c < 2620
                  ? (c < 2616
                    ? (c >= 2613 && c <= 2614)
                    : c <= 2617)
                  : (c <= 2620 || (c < 2631
                    ? (c >= 2622 && c <= 2626)
                    : c <= 2632)))))
              : (c <= 2637 || (c < 2693
                ? (c < 2654
                  ? (c < 2649
                    ? c == 2641
                    : c <= 2652)
                  : (c <= 2654 || (c < 2689
                    ? (c >= 2662 && c <= 2677)
                    : c <= 2691)))
                : (c <= 2701 || (c < 2730
                  ? (c < 2707
                    ? (c >= 2703 && c <= 2705)
                    : c <= 2728)
                  : (c <= 2736 || (c < 2741
                    ? (c >= 2738 && c <= 2739)
                    : c <= 2745)))))))))))
        : (c <= 2757 || (c < 3168
          ? (c < 2958
            ? (c < 2866
              ? (c < 2809
                ? (c < 2768
                  ? (c < 2763
                    ? (c >= 2759 && c <= 2761)
                    : c <= 2765)
                  : (c <= 2768 || (c < 2790
                    ? (c >= 2784 && c <= 2787)
                    : c <= 2799)))
                : (c <= 2815 || (c < 2831
                  ? (c < 2821
                    ? (c >= 2817 && c <= 2819)
                    : c <= 2828)
                  : (c <= 2832 || (c < 2858
                    ? (c >= 2835 && c <= 2856)
                    : c <= 2864)))))
              : (c <= 2867 || (c < 2908
                ? (c < 2887
                  ? (c < 2876
                    ? (c >= 2869 && c <= 2873)
                    : c <= 2884)
                  : (c <= 2888 || (c < 2901
                    ? (c >= 2891 && c <= 2893)
                    : c <= 2903)))
                : (c <= 2909 || (c < 2929
                  ? (c < 2918
                    ? (c >= 2911 && c <= 2915)
                    : c <= 2927)
                  : (c <= 2929 || (c < 2949
                    ? (c >= 2946 && c <= 2947)
                    : c <= 2954)))))))
            : (c <= 2960 || (c < 3031
              ? (c < 2984
                ? (c < 2972
                  ? (c < 2969
                    ? (c >= 2962 && c <= 2965)
                    : c <= 2970)
                  : (c <= 2972 || (c < 2979
                    ? (c >= 2974 && c <= 2975)
                    : c <= 2980)))
                : (c <= 2986 || (c < 3014
                  ? (c < 3006
                    ? (c >= 2990 && c <= 3001)
                    : c <= 3010)
                  : (c <= 3016 || (c < 3024
                    ? (c >= 3018 && c <= 3021)
                    : c <= 3024)))))
              : (c <= 3031 || (c < 3132
                ? (c < 3086
                  ? (c < 3072
                    ? (c >= 3046 && c <= 3055)
                    : c <= 3084)
                  : (c <= 3088 || (c < 3114
                    ? (c >= 3090 && c <= 3112)
                    : c <= 3129)))
                : (c <= 3140 || (c < 3157
                  ? (c < 3146
                    ? (c >= 3142 && c <= 3144)
                    : c <= 3149)
                  : (c <= 3158 || (c < 3165
                    ? (c >= 3160 && c <= 3162)
                    : c <= 3165)))))))))
          : (c <= 3171 || (c < 3450
            ? (c < 3293
              ? (c < 3242
                ? (c < 3205
                  ? (c < 3200
                    ? (c >= 3174 && c <= 3183)
                    : c <= 3203)
                  : (c <= 3212 || (c < 3218
                    ? (c >= 3214 && c <= 3216)
                    : c <= 3240)))
                : (c <= 3251 || (c < 3270
                  ? (c < 3260
                    ? (c >= 3253 && c <= 3257)
                    : c <= 3268)
                  : (c <= 3272 || (c < 3285
                    ? (c >= 3274 && c <= 3277)
                    : c <= 3286)))))
              : (c <= 3294 || (c < 3346
                ? (c < 3313
                  ? (c < 3302
                    ? (c >= 3296 && c <= 3299)
                    : c <= 3311)
                  : (c <= 3314 || (c < 3342
                    ? (c >= 3328 && c <= 3340)
                    : c <= 3344)))
                : (c <= 3396 || (c < 3412
                  ? (c < 3402
                    ? (c >= 3398 && c <= 3400)
                    : c <= 3406)
                  : (c <= 3415 || (c < 3430
                    ? (c >= 3423 && c <= 3427)
                    : c <= 3439)))))))
            : (c <= 3455 || (c < 3570
              ? (c < 3520
                ? (c < 3482
                  ? (c < 3461
                    ? (c >= 3457 && c <= 3459)
                    : c <= 3478)
                  : (c <= 3505 || (c < 3517
                    ? (c >= 3507 && c <= 3515)
                    : c <= 3517)))
                : (c <= 3526 || (c < 3542
                  ? (c < 3535
                    ? c == 3530
                    : c <= 3540)
                  : (c <= 3542 || (c < 3558
                    ? (c >= 3544 && c <= 3551)
                    : c <= 3567)))))
              : (c <= 3571 || (c < 3718
                ? (c < 3664
                  ? (c < 3648
                    ? (c >= 3585 && c <= 3642)
                    : c <= 3662)
                  : (c <= 3673 || (c < 3716
                    ? (c >= 3713 && c <= 3714)
                    : c <= 3716)))
                : (c <= 3722 || (c < 3751
                  ? (c < 3749
                    ? (c >= 3724 && c <= 3747)
                    : c <= 3749)
                  : (c <= 3773 || (c >= 3776 && c <= 3780)))))))))))))
      : (c <= 3782 || (c < 8025
        ? (c < 5888
          ? (c < 4688
            ? (c < 3953
              ? (c < 3872
                ? (c < 3804
                  ? (c < 3792
                    ? (c >= 3784 && c <= 3789)
                    : c <= 3801)
                  : (c <= 3807 || (c < 3864
                    ? c == 3840
                    : c <= 3865)))
                : (c <= 3881 || (c < 3897
                  ? (c < 3895
                    ? c == 3893
                    : c <= 3895)
                  : (c <= 3897 || (c < 3913
                    ? (c >= 3902 && c <= 3911)
                    : c <= 3948)))))
              : (c <= 3972 || (c < 4256
                ? (c < 4038
                  ? (c < 3993
                    ? (c >= 3974 && c <= 3991)
                    : c <= 4028)
                  : (c <= 4038 || (c < 4176
                    ? (c >= 4096 && c <= 4169)
                    : c <= 4253)))
                : (c <= 4293 || (c < 4304
                  ? (c < 4301
                    ? c == 4295
                    : c <= 4301)
                  : (c <= 4346 || (c < 4682
                    ? (c >= 4348 && c <= 4680)
                    : c <= 4685)))))))
            : (c <= 4694 || (c < 4882
              ? (c < 4786
                ? (c < 4704
                  ? (c < 4698
                    ? c == 4696
                    : c <= 4701)
                  : (c <= 4744 || (c < 4752
                    ? (c >= 4746 && c <= 4749)
                    : c <= 4784)))
                : (c <= 4789 || (c < 4802
                  ? (c < 4800
                    ? (c >= 4792 && c <= 4798)
                    : c <= 4800)
                  : (c <= 4805 || (c < 4824
                    ? (c >= 4808 && c <= 4822)
                    : c <= 4880)))))
              : (c <= 4885 || (c < 5112
                ? (c < 4969
                  ? (c < 4957
                    ? (c >= 4888 && c <= 4954)
                    : c <= 4959)
                  : (c <= 4977 || (c < 5024
                    ? (c >= 4992 && c <= 5007)
                    : c <= 5109)))
                : (c <= 5117 || (c < 5761
                  ? (c < 5743
                    ? (c >= 5121 && c <= 5740)
                    : c <= 5759)
                  : (c <= 5786 || (c < 5870
                    ? (c >= 5792 && c <= 5866)
                    : c <= 5880)))))))))
          : (c <= 5909 || (c < 6688
            ? (c < 6176
              ? (c < 6016
                ? (c < 5984
                  ? (c < 5952
                    ? (c >= 5919 && c <= 5940)
                    : c <= 5971)
                  : (c <= 5996 || (c < 6002
                    ? (c >= 5998 && c <= 6000)
                    : c <= 6003)))
                : (c <= 6099 || (c < 6112
                  ? (c < 6108
                    ? c == 6103
                    : c <= 6109)
                  : (c <= 6121 || (c < 6159
                    ? (c >= 6155 && c <= 6157)
                    : c <= 6169)))))
              : (c <= 6264 || (c < 6470
                ? (c < 6400
                  ? (c < 6320
                    ? (c >= 6272 && c <= 6314)
                    : c <= 6389)
                  : (c <= 6430 || (c < 6448
                    ? (c >= 6432 && c <= 6443)
                    : c <= 6459)))
                : (c <= 6509 || (c < 6576
                  ? (c < 6528
                    ? (c >= 6512 && c <= 6516)
                    : c <= 6571)
                  : (c <= 6601 || (c < 6656
                    ? (c >= 6608 && c <= 6618)
                    : c <= 6683)))))))
            : (c <= 6750 || (c < 7232
              ? (c < 6847
                ? (c < 6800
                  ? (c < 6783
                    ? (c >= 6752 && c <= 6780)
                    : c <= 6793)
                  : (c <= 6809 || (c < 6832
                    ? c == 6823
                    : c <= 6845)))
                : (c <= 6862 || (c < 7019
                  ? (c < 6992
                    ? (c >= 6912 && c <= 6988)
                    : c <= 7001)
                  : (c <= 7027 || (c < 7168
                    ? (c >= 7040 && c <= 7155)
                    : c <= 7223)))))
              : (c <= 7241 || (c < 7380
                ? (c < 7312
                  ? (c < 7296
                    ? (c >= 7245 && c <= 7293)
                    : c <= 7304)
                  : (c <= 7354 || (c < 7376
                    ? (c >= 7357 && c <= 7359)
                    : c <= 7378)))
                : (c <= 7418 || (c < 7968
                  ? (c < 7960
                    ? (c >= 7424 && c <= 7957)
                    : c <= 7965)
                  : (c <= 8005 || (c < 8016
                    ? (c >= 8008 && c <= 8013)
                    : c <= 8023)))))))))))
        : (c <= 8025 || (c < 11720
          ? (c < 8458
            ? (c < 8178
              ? (c < 8126
                ? (c < 8031
                  ? (c < 8029
                    ? c == 8027
                    : c <= 8029)
                  : (c <= 8061 || (c < 8118
                    ? (c >= 8064 && c <= 8116)
                    : c <= 8124)))
                : (c <= 8126 || (c < 8144
                  ? (c < 8134
                    ? (c >= 8130 && c <= 8132)
                    : c <= 8140)
                  : (c <= 8147 || (c < 8160
                    ? (c >= 8150 && c <= 8155)
                    : c <= 8172)))))
              : (c <= 8180 || (c < 8336
                ? (c < 8276
                  ? (c < 8255
                    ? (c >= 8182 && c <= 8188)
                    : c <= 8256)
                  : (c <= 8276 || (c < 8319
                    ? c == 8305
                    : c <= 8319)))
                : (c <= 8348 || (c < 8421
                  ? (c < 8417
                    ? (c >= 8400 && c <= 8412)
                    : c <= 8417)
                  : (c <= 8432 || (c < 8455
                    ? c == 8450
                    : c <= 8455)))))))
            : (c <= 8467 || (c < 11499
              ? (c < 8490
                ? (c < 8484
                  ? (c < 8472
                    ? c == 8469
                    : c <= 8477)
                  : (c <= 8484 || (c < 8488
                    ? c == 8486
                    : c <= 8488)))
                : (c <= 8505 || (c < 8526
                  ? (c < 8517
                    ? (c >= 8508 && c <= 8511)
                    : c <= 8521)
                  : (c <= 8526 || (c < 11264
                    ? (c >= 8544 && c <= 8584)
                    : c <= 11492)))))
              : (c <= 11507 || (c < 11647
                ? (c < 11565
                  ? (c < 11559
                    ? (c >= 11520 && c <= 11557)
                    : c <= 11559)
                  : (c <= 11565 || (c < 11631
                    ? (c >= 11568 && c <= 11623)
                    : c <= 11631)))
                : (c <= 11670 || (c < 11696
                  ? (c < 11688
                    ? (c >= 11680 && c <= 11686)
                    : c <= 11694)
                  : (c <= 11702 || (c < 11712
                    ? (c >= 11704 && c <= 11710)
                    : c <= 11718)))))))))
          : (c <= 11726 || (c < 42623
            ? (c < 12540
              ? (c < 12337
                ? (c < 11744
                  ? (c < 11736
                    ? (c >= 11728 && c <= 11734)
                    : c <= 11742)
                  : (c <= 11775 || (c < 12321
                    ? (c >= 12293 && c <= 12295)
                    : c <= 12335)))
                : (c <= 12341 || (c < 12441
                  ? (c < 12353
                    ? (c >= 12344 && c <= 12348)
                    : c <= 12438)
                  : (c <= 12442 || (c < 12449
                    ? (c >= 12445 && c <= 12447)
                    : c <= 12538)))))
              : (c <= 12543 || (c < 19968
                ? (c < 12704
                  ? (c < 12593
                    ? (c >= 12549 && c <= 12591)
                    : c <= 12686)
                  : (c <= 12735 || (c < 13312
                    ? (c >= 12784 && c <= 12799)
                    : c <= 19903)))
                : (c <= 42124 || (c < 42512
                  ? (c < 42240
                    ? (c >= 42192 && c <= 42237)
                    : c <= 42508)
                  : (c <= 42539 || (c < 42612
                    ? (c >= 42560 && c <= 42607)
                    : c <= 42621)))))))
            : (c <= 42737 || (c < 43232
              ? (c < 42965
                ? (c < 42891
                  ? (c < 42786
                    ? (c >= 42775 && c <= 42783)
                    : c <= 42888)
                  : (c <= 42954 || (c < 42963
                    ? (c >= 42960 && c <= 42961)
                    : c <= 42963)))
                : (c <= 42969 || (c < 43072
                  ? (c < 43052
                    ? (c >= 42994 && c <= 43047)
                    : c <= 43052)
                  : (c <= 43123 || (c < 43216
                    ? (c >= 43136 && c <= 43205)
                    : c <= 43225)))))
              : (c <= 43255 || (c < 43471
                ? (c < 43312
                  ? (c < 43261
                    ? c == 43259
                    : c <= 43309)
                  : (c <= 43347 || (c < 43392
                    ? (c >= 43360 && c <= 43388)
                    : c <= 43456)))
                : (c <= 43481 || (c < 43584
                  ? (c < 43520
                    ? (c >= 43488 && c <= 43518)
                    : c <= 43574)
                  : (c <= 43597 || (c >= 43600 && c <= 43609)))))))))))))))
    : (c <= 43638 || (c < 71453
      ? (c < 67639
        ? (c < 65345
          ? (c < 64312
            ? (c < 43888
              ? (c < 43785
                ? (c < 43744
                  ? (c < 43739
                    ? (c >= 43642 && c <= 43714)
                    : c <= 43741)
                  : (c <= 43759 || (c < 43777
                    ? (c >= 43762 && c <= 43766)
                    : c <= 43782)))
                : (c <= 43790 || (c < 43816
                  ? (c < 43808
                    ? (c >= 43793 && c <= 43798)
                    : c <= 43814)
                  : (c <= 43822 || (c < 43868
                    ? (c >= 43824 && c <= 43866)
                    : c <= 43881)))))
              : (c <= 44010 || (c < 63744
                ? (c < 44032
                  ? (c < 44016
                    ? (c >= 44012 && c <= 44013)
                    : c <= 44025)
                  : (c <= 55203 || (c < 55243
                    ? (c >= 55216 && c <= 55238)
                    : c <= 55291)))
                : (c <= 64109 || (c < 64275
                  ? (c < 64256
                    ? (c >= 64112 && c <= 64217)
                    : c <= 64262)
                  : (c <= 64279 || (c < 64298
                    ? (c >= 64285 && c <= 64296)
                    : c <= 64310)))))))
            : (c <= 64316 || (c < 65075
              ? (c < 64612
                ? (c < 64323
                  ? (c < 64320
                    ? c == 64318
                    : c <= 64321)
                  : (c <= 64324 || (c < 64467
                    ? (c >= 64326 && c <= 64433)
                    : c <= 64605)))
                : (c <= 64829 || (c < 65008
                  ? (c < 64914
                    ? (c >= 64848 && c <= 64911)
                    : c <= 64967)
                  : (c <= 65017 || (c < 65056
                    ? (c >= 65024 && c <= 65039)
                    : c <= 65071)))))
              : (c <= 65076 || (c < 65147
                ? (c < 65139
                  ? (c < 65137
                    ? (c >= 65101 && c <= 65103)
                    : c <= 65137)
                  : (c <= 65139 || (c < 65145
                    ? c == 65143
                    : c <= 65145)))
                : (c <= 65147 || (c < 65296
                  ? (c < 65151
                    ? c == 65149
                    : c <= 65276)
                  : (c <= 65305 || (c < 65343
                    ? (c >= 65313 && c <= 65338)
                    : c <= 65343)))))))))
          : (c <= 65370 || (c < 66513
            ? (c < 65664
              ? (c < 65536
                ? (c < 65482
                  ? (c < 65474
                    ? (c >= 65382 && c <= 65470)
                    : c <= 65479)
                  : (c <= 65487 || (c < 65498
                    ? (c >= 65490 && c <= 65495)
                    : c <= 65500)))
                : (c <= 65547 || (c < 65596
                  ? (c < 65576
                    ? (c >= 65549 && c <= 65574)
                    : c <= 65594)
                  : (c <= 65597 || (c < 65616
                    ? (c >= 65599 && c <= 65613)
                    : c <= 65629)))))
              : (c <= 65786 || (c < 66304
                ? (c < 66176
                  ? (c < 66045
                    ? (c >= 65856 && c <= 65908)
                    : c <= 66045)
                  : (c <= 66204 || (c < 66272
                    ? (c >= 66208 && c <= 66256)
                    : c <= 66272)))
                : (c <= 66335 || (c < 66432
                  ? (c < 66384
                    ? (c >= 66349 && c <= 66378)
                    : c <= 66426)
                  : (c <= 66461 || (c < 66504
                    ? (c >= 66464 && c <= 66499)
                    : c <= 66511)))))))
            : (c <= 66517 || (c < 66979
              ? (c < 66864
                ? (c < 66736
                  ? (c < 66720
                    ? (c >= 66560 && c <= 66717)
                    : c <= 66729)
                  : (c <= 66771 || (c < 66816
                    ? (c >= 66776 && c <= 66811)
                    : c <= 66855)))
                : (c <= 66915 || (c < 66956
                  ? (c < 66940
                    ? (c >= 66928 && c <= 66938)
                    : c <= 66954)
                  : (c <= 66962 || (c < 66967
                    ? (c >= 66964 && c <= 66965)
                    : c <= 66977)))))
              : (c <= 66993 || (c < 67456
                ? (c < 67072
                  ? (c < 67003
                    ? (c >= 66995 && c <= 67001)
                    : c <= 67004)
                  : (c <= 67382 || (c < 67424
                    ? (c >= 67392 && c <= 67413)
                    : c <= 67431)))
                : (c <= 67461 || (c < 67584
                  ? (c < 67506
                    ? (c >= 67463 && c <= 67504)
                    : c <= 67514)
                  : (c <= 67589 || (c < 67594
                    ? c == 67592
                    : c <= 67637)))))))))))
        : (c <= 67640 || (c < 69956
          ? (c < 68448
            ? (c < 68101
              ? (c < 67828
                ? (c < 67680
                  ? (c < 67647
                    ? c == 67644
                    : c <= 67669)
                  : (c <= 67702 || (c < 67808
                    ? (c >= 67712 && c <= 67742)
                    : c <= 67826)))
                : (c <= 67829 || (c < 67968
                  ? (c < 67872
                    ? (c >= 67840 && c <= 67861)
                    : c <= 67897)
                  : (c <= 68023 || (c < 68096
                    ? (c >= 68030 && c <= 68031)
                    : c <= 68099)))))
              : (c <= 68102 || (c < 68192
                ? (c < 68121
                  ? (c < 68117
                    ? (c >= 68108 && c <= 68115)
                    : c <= 68119)
                  : (c <= 68149 || (c < 68159
                    ? (c >= 68152 && c <= 68154)
                    : c <= 68159)))
                : (c <= 68220 || (c < 68297
                  ? (c < 68288
                    ? (c >= 68224 && c <= 68252)
                    : c <= 68295)
                  : (c <= 68326 || (c < 68416
                    ? (c >= 68352 && c <= 68405)
                    : c <= 68437)))))))
            : (c <= 68466 || (c < 69424
              ? (c < 68912
                ? (c < 68736
                  ? (c < 68608
                    ? (c >= 68480 && c <= 68497)
                    : c <= 68680)
                  : (c <= 68786 || (c < 68864
                    ? (c >= 68800 && c <= 68850)
                    : c <= 68903)))
                : (c <= 68921 || (c < 69296
                  ? (c < 69291
                    ? (c >= 69248 && c <= 69289)
                    : c <= 69292)
                  : (c <= 69297 || (c < 69415
                    ? (c >= 69376 && c <= 69404)
                    : c <= 69415)))))
              : (c <= 69456 || (c < 69759
                ? (c < 69600
                  ? (c < 69552
                    ? (c >= 69488 && c <= 69509)
                    : c <= 69572)
                  : (c <= 69622 || (c < 69734
                    ? (c >= 69632 && c <= 69702)
                    : c <= 69749)))
                : (c <= 69818 || (c < 69872
                  ? (c < 69840
                    ? c == 69826
                    : c <= 69864)
                  : (c <= 69881 || (c < 69942
                    ? (c >= 69888 && c <= 69940)
                    : c <= 69951)))))))))
          : (c <= 69959 || (c < 70459
            ? (c < 70282
              ? (c < 70108
                ? (c < 70016
                  ? (c < 70006
                    ? (c >= 69968 && c <= 70003)
                    : c <= 70006)
                  : (c <= 70084 || (c < 70094
                    ? (c >= 70089 && c <= 70092)
                    : c <= 70106)))
                : (c <= 70108 || (c < 70206
                  ? (c < 70163
                    ? (c >= 70144 && c <= 70161)
                    : c <= 70199)
                  : (c <= 70206 || (c < 70280
                    ? (c >= 70272 && c <= 70278)
                    : c <= 70280)))))
              : (c <= 70285 || (c < 70405
                ? (c < 70320
                  ? (c < 70303
                    ? (c >= 70287 && c <= 70301)
                    : c <= 70312)
                  : (c <= 70378 || (c < 70400
                    ? (c >= 70384 && c <= 70393)
                    : c <= 70403)))
                : (c <= 70412 || (c < 70442
                  ? (c < 70419
                    ? (c >= 70415 && c <= 70416)
                    : c <= 70440)
                  : (c <= 70448 || (c < 70453
                    ? (c >= 70450 && c <= 70451)
                    : c <= 70457)))))))
            : (c <= 70468 || (c < 70855
              ? (c < 70502
                ? (c < 70480
                  ? (c < 70475
                    ? (c >= 70471 && c <= 70472)
                    : c <= 70477)
                  : (c <= 70480 || (c < 70493
                    ? c == 70487
                    : c <= 70499)))
                : (c <= 70508 || (c < 70736
                  ? (c < 70656
                    ? (c >= 70512 && c <= 70516)
                    : c <= 70730)
                  : (c <= 70745 || (c < 70784
                    ? (c >= 70750 && c <= 70753)
                    : c <= 70853)))))
              : (c <= 70855 || (c < 71236
                ? (c < 71096
                  ? (c < 71040
                    ? (c >= 70864 && c <= 70873)
                    : c <= 71093)
                  : (c <= 71104 || (c < 71168
                    ? (c >= 71128 && c <= 71133)
                    : c <= 71232)))
                : (c <= 71236 || (c < 71360
                  ? (c < 71296
                    ? (c >= 71248 && c <= 71257)
                    : c <= 71352)
                  : (c <= 71369 || (c >= 71424 && c <= 71450)))))))))))))
      : (c <= 71467 || (c < 119973
        ? (c < 77824
          ? (c < 72760
            ? (c < 72016
              ? (c < 71945
                ? (c < 71680
                  ? (c < 71488
                    ? (c >= 71472 && c <= 71481)
                    : c <= 71494)
                  : (c <= 71738 || (c < 71935
                    ? (c >= 71840 && c <= 71913)
                    : c <= 71942)))
                : (c <= 71945 || (c < 71960
                  ? (c < 71957
                    ? (c >= 71948 && c <= 71955)
                    : c <= 71958)
                  : (c <= 71989 || (c < 71995
                    ? (c >= 71991 && c <= 71992)
                    : c <= 72003)))))
              : (c <= 72025 || (c < 72263
                ? (c < 72154
                  ? (c < 72106
                    ? (c >= 72096 && c <= 72103)
                    : c <= 72151)
                  : (c <= 72161 || (c < 72192
                    ? (c >= 72163 && c <= 72164)
                    : c <= 72254)))
                : (c <= 72263 || (c < 72368
                  ? (c < 72349
                    ? (c >= 72272 && c <= 72345)
                    : c <= 72349)
                  : (c <= 72440 || (c < 72714
                    ? (c >= 72704 && c <= 72712)
                    : c <= 72758)))))))
            : (c <= 72768 || (c < 73056
              ? (c < 72968
                ? (c < 72850
                  ? (c < 72818
                    ? (c >= 72784 && c <= 72793)
                    : c <= 72847)
                  : (c <= 72871 || (c < 72960
                    ? (c >= 72873 && c <= 72886)
                    : c <= 72966)))
                : (c <= 72969 || (c < 73020
                  ? (c < 73018
                    ? (c >= 72971 && c <= 73014)
                    : c <= 73018)
                  : (c <= 73021 || (c < 73040
                    ? (c >= 73023 && c <= 73031)
                    : c <= 73049)))))
              : (c <= 73061 || (c < 73440
                ? (c < 73104
                  ? (c < 73066
                    ? (c >= 73063 && c <= 73064)
                    : c <= 73102)
                  : (c <= 73105 || (c < 73120
                    ? (c >= 73107 && c <= 73112)
                    : c <= 73129)))
                : (c <= 73462 || (c < 74752
                  ? (c < 73728
                    ? c == 73648
                    : c <= 74649)
                  : (c <= 74862 || (c < 77712
                    ? (c >= 74880 && c <= 75075)
                    : c <= 77808)))))))))
          : (c <= 78894 || (c < 110576
            ? (c < 93027
              ? (c < 92864
                ? (c < 92736
                  ? (c < 92160
                    ? (c >= 82944 && c <= 83526)
                    : c <= 92728)
                  : (c <= 92766 || (c < 92784
                    ? (c >= 92768 && c <= 92777)
                    : c <= 92862)))
                : (c <= 92873 || (c < 92928
                  ? (c < 92912
                    ? (c >= 92880 && c <= 92909)
                    : c <= 92916)
                  : (c <= 92982 || (c < 93008
                    ? (c >= 92992 && c <= 92995)
                    : c <= 93017)))))
              : (c <= 93047 || (c < 94176
                ? (c < 93952
                  ? (c < 93760
                    ? (c >= 93053 && c <= 93071)
                    : c <= 93823)
                  : (c <= 94026 || (c < 94095
                    ? (c >= 94031 && c <= 94087)
                    : c <= 94111)))
                : (c <= 94177 || (c < 94208
                  ? (c < 94192
                    ? (c >= 94179 && c <= 94180)
                    : c <= 94193)
                  : (c <= 100343 || (c < 101632
                    ? (c >= 100352 && c <= 101589)
                    : c <= 101640)))))))
            : (c <= 110579 || (c < 118528
              ? (c < 110960
                ? (c < 110592
                  ? (c < 110589
                    ? (c >= 110581 && c <= 110587)
                    : c <= 110590)
                  : (c <= 110882 || (c < 110948
                    ? (c >= 110928 && c <= 110930)
                    : c <= 110951)))
                : (c <= 111355 || (c < 113792
                  ? (c < 113776
                    ? (c >= 113664 && c <= 113770)
                    : c <= 113788)
                  : (c <= 113800 || (c < 113821
                    ? (c >= 113808 && c <= 113817)
                    : c <= 113822)))))
              : (c <= 118573 || (c < 119210
                ? (c < 119149
                  ? (c < 119141
                    ? (c >= 118576 && c <= 118598)
                    : c <= 119145)
                  : (c <= 119154 || (c < 119173
                    ? (c >= 119163 && c <= 119170)
                    : c <= 119179)))
                : (c <= 119213 || (c < 119894
                  ? (c < 119808
                    ? (c >= 119362 && c <= 119364)
                    : c <= 119892)
                  : (c <= 119964 || (c < 119970
                    ? (c >= 119966 && c <= 119967)
                    : c <= 119970)))))))))))
        : (c <= 119974 || (c < 124912
          ? (c < 120746
            ? (c < 120134
              ? (c < 120071
                ? (c < 119995
                  ? (c < 119982
                    ? (c >= 119977 && c <= 119980)
                    : c <= 119993)
                  : (c <= 119995 || (c < 120005
                    ? (c >= 119997 && c <= 120003)
                    : c <= 120069)))
                : (c <= 120074 || (c < 120094
                  ? (c < 120086
                    ? (c >= 120077 && c <= 120084)
                    : c <= 120092)
                  : (c <= 120121 || (c < 120128
                    ? (c >= 120123 && c <= 120126)
                    : c <= 120132)))))
              : (c <= 120134 || (c < 120572
                ? (c < 120488
                  ? (c < 120146
                    ? (c >= 120138 && c <= 120144)
                    : c <= 120485)
                  : (c <= 120512 || (c < 120540
                    ? (c >= 120514 && c <= 120538)
                    : c <= 120570)))
                : (c <= 120596 || (c < 120656
                  ? (c < 120630
                    ? (c >= 120598 && c <= 120628)
                    : c <= 120654)
                  : (c <= 120686 || (c < 120714
                    ? (c >= 120688 && c <= 120712)
                    : c <= 120744)))))))
            : (c <= 120770 || (c < 122907
              ? (c < 121476
                ? (c < 121344
                  ? (c < 120782
                    ? (c >= 120772 && c <= 120779)
                    : c <= 120831)
                  : (c <= 121398 || (c < 121461
                    ? (c >= 121403 && c <= 121452)
                    : c <= 121461)))
                : (c <= 121476 || (c < 122624
                  ? (c < 121505
                    ? (c >= 121499 && c <= 121503)
                    : c <= 121519)
                  : (c <= 122654 || (c < 122888
                    ? (c >= 122880 && c <= 122886)
                    : c <= 122904)))))
              : (c <= 122913 || (c < 123214
                ? (c < 123136
                  ? (c < 122918
                    ? (c >= 122915 && c <= 122916)
                    : c <= 122922)
                  : (c <= 123180 || (c < 123200
                    ? (c >= 123184 && c <= 123197)
                    : c <= 123209)))
                : (c <= 123214 || (c < 124896
                  ? (c < 123584
                    ? (c >= 123536 && c <= 123566)
                    : c <= 123641)
                  : (c <= 124902 || (c < 124909
                    ? (c >= 124904 && c <= 124907)
                    : c <= 124910)))))))))
          : (c <= 124926 || (c < 126557
            ? (c < 126521
              ? (c < 126469
                ? (c < 125184
                  ? (c < 125136
                    ? (c >= 124928 && c <= 125124)
                    : c <= 125142)
                  : (c <= 125259 || (c < 126464
                    ? (c >= 125264 && c <= 125273)
                    : c <= 126467)))
                : (c <= 126495 || (c < 126503
                  ? (c < 126500
                    ? (c >= 126497 && c <= 126498)
                    : c <= 126500)
                  : (c <= 126503 || (c < 126516
                    ? (c >= 126505 && c <= 126514)
                    : c <= 126519)))))
              : (c <= 126521 || (c < 126541
                ? (c < 126535
                  ? (c < 126530
                    ? c == 126523
                    : c <= 126530)
                  : (c <= 126535 || (c < 126539
                    ? c == 126537
                    : c <= 126539)))
                : (c <= 126543 || (c < 126551
                  ? (c < 126548
                    ? (c >= 126545 && c <= 126546)
                    : c <= 126548)
                  : (c <= 126551 || (c < 126555
                    ? c == 126553
                    : c <= 126555)))))))
            : (c <= 126557 || (c < 126629
              ? (c < 126580
                ? (c < 126564
                  ? (c < 126561
                    ? c == 126559
                    : c <= 126562)
                  : (c <= 126564 || (c < 126572
                    ? (c >= 126567 && c <= 126570)
                    : c <= 126578)))
                : (c <= 126583 || (c < 126592
                  ? (c < 126590
                    ? (c >= 126585 && c <= 126588)
                    : c <= 126590)
                  : (c <= 126601 || (c < 126625
                    ? (c >= 126603 && c <= 126619)
                    : c <= 126627)))))
              : (c <= 126633 || (c < 178208
                ? (c < 131072
                  ? (c < 130032
                    ? (c >= 126635 && c <= 126651)
                    : c <= 130041)
                  : (c <= 173791 || (c < 177984
                    ? (c >= 173824 && c <= 177976)
                    : c <= 178205)))
                : (c <= 183969 || (c < 196608
                  ? (c < 194560
                    ? (c >= 183984 && c <= 191456)
                    : c <= 195101)
                  : (c <= 201546 || (c >= 917760 && c <= 917999)))))))))))))))));
}

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(139);
      if (lookahead == '!') ADVANCE(215);
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '%') ADVANCE(213);
      if (lookahead == '&') ADVANCE(198);
      if (lookahead == '(') ADVANCE(151);
      if (lookahead == ')') ADVANCE(152);
      if (lookahead == '*') ADVANCE(211);
      if (lookahead == '+') ADVANCE(209);
      if (lookahead == ',') ADVANCE(178);
      if (lookahead == '-') ADVANCE(210);
      if (lookahead == '.') ADVANCE(223);
      if (lookahead == '/') ADVANCE(212);
      if (lookahead == ':') ADVANCE(219);
      if (lookahead == ';') ADVANCE(140);
      if (lookahead == '<') ADVANCE(203);
      if (lookahead == '=') ADVANCE(176);
      if (lookahead == '>') ADVANCE(205);
      if (lookahead == 'B') ADVANCE(84);
      if (lookahead == 'N') ADVANCE(115);
      if (lookahead == 'S') ADVANCE(108);
      if (lookahead == '[') ADVANCE(221);
      if (lookahead == '\\') SKIP(129)
      if (lookahead == ']') ADVANCE(222);
      if (lookahead == '^') ADVANCE(200);
      if (lookahead == 'a') ADVANCE(98);
      if (lookahead == 'c') ADVANCE(23);
      if (lookahead == 'e') ADVANCE(67);
      if (lookahead == 'f') ADVANCE(57);
      if (lookahead == 'i') ADVANCE(50);
      if (lookahead == 'n') ADVANCE(39);
      if (lookahead == 'p') ADVANCE(90);
      if (lookahead == 'r') ADVANCE(44);
      if (lookahead == 't') ADVANCE(91);
      if (lookahead == 'u') ADVANCE(99);
      if (lookahead == 'v') ADVANCE(24);
      if (lookahead == 'w') ADVANCE(56);
      if (lookahead == '{') ADVANCE(185);
      if (lookahead == '|') ADVANCE(199);
      if (lookahead == '}') ADVANCE(186);
      if (lookahead == '~') ADVANCE(216);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(0)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(195);
      END_STATE();
    case 1:
      if (lookahead == '\n') SKIP(14)
      END_STATE();
    case 2:
      if (lookahead == '\n') SKIP(14)
      if (lookahead == '\r') SKIP(1)
      END_STATE();
    case 3:
      if (lookahead == '\n') SKIP(13)
      END_STATE();
    case 4:
      if (lookahead == '\n') SKIP(13)
      if (lookahead == '\r') SKIP(3)
      END_STATE();
    case 5:
      if (lookahead == '\n') SKIP(15)
      END_STATE();
    case 6:
      if (lookahead == '\n') SKIP(15)
      if (lookahead == '\r') SKIP(5)
      END_STATE();
    case 7:
      if (lookahead == '\n') SKIP(16)
      END_STATE();
    case 8:
      if (lookahead == '\n') SKIP(16)
      if (lookahead == '\r') SKIP(7)
      END_STATE();
    case 9:
      if (lookahead == '\n') SKIP(16)
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '/') ADVANCE(190);
      if (lookahead == '\\') ADVANCE(10);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(193);
      if (lookahead != 0) ADVANCE(194);
      END_STATE();
    case 10:
      if (lookahead == '\n') ADVANCE(226);
      if (lookahead == '\r') ADVANCE(226);
      if (lookahead == 'u') ADVANCE(121);
      if (lookahead == 'x') ADVANCE(126);
      if (lookahead != 0) ADVANCE(226);
      END_STATE();
    case 11:
      if (lookahead == '\n') SKIP(20)
      END_STATE();
    case 12:
      if (lookahead == '\n') SKIP(20)
      if (lookahead == '\r') SKIP(11)
      END_STATE();
    case 13:
      if (lookahead == '!') ADVANCE(214);
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '(') ADVANCE(150);
      if (lookahead == ')') ADVANCE(152);
      if (lookahead == '+') ADVANCE(209);
      if (lookahead == '-') ADVANCE(210);
      if (lookahead == '/') ADVANCE(17);
      if (lookahead == ';') ADVANCE(140);
      if (lookahead == '[') ADVANCE(221);
      if (lookahead == '\\') SKIP(4)
      if (lookahead == 'n') ADVANCE(246);
      if (lookahead == '{') ADVANCE(185);
      if (lookahead == '~') ADVANCE(216);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(13)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(195);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(316);
      END_STATE();
    case 14:
      if (lookahead == '!') ADVANCE(21);
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '%') ADVANCE(213);
      if (lookahead == '&') ADVANCE(198);
      if (lookahead == '(') ADVANCE(151);
      if (lookahead == ')') ADVANCE(152);
      if (lookahead == '*') ADVANCE(211);
      if (lookahead == '+') ADVANCE(209);
      if (lookahead == ',') ADVANCE(178);
      if (lookahead == '-') ADVANCE(210);
      if (lookahead == '.') ADVANCE(223);
      if (lookahead == '/') ADVANCE(212);
      if (lookahead == ':') ADVANCE(219);
      if (lookahead == ';') ADVANCE(140);
      if (lookahead == '<') ADVANCE(203);
      if (lookahead == '=') ADVANCE(22);
      if (lookahead == '>') ADVANCE(205);
      if (lookahead == '[') ADVANCE(221);
      if (lookahead == '\\') SKIP(2)
      if (lookahead == ']') ADVANCE(222);
      if (lookahead == '^') ADVANCE(200);
      if (lookahead == 'a') ADVANCE(98);
      if (lookahead == 'i') ADVANCE(82);
      if (lookahead == '{') ADVANCE(185);
      if (lookahead == '|') ADVANCE(199);
      if (lookahead == '}') ADVANCE(186);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(14)
      END_STATE();
    case 15:
      if (lookahead == '!') ADVANCE(21);
      if (lookahead == '%') ADVANCE(213);
      if (lookahead == '&') ADVANCE(198);
      if (lookahead == '*') ADVANCE(211);
      if (lookahead == '+') ADVANCE(209);
      if (lookahead == '-') ADVANCE(210);
      if (lookahead == '.') ADVANCE(223);
      if (lookahead == '/') ADVANCE(212);
      if (lookahead == ';') ADVANCE(140);
      if (lookahead == '<') ADVANCE(203);
      if (lookahead == '=') ADVANCE(175);
      if (lookahead == '>') ADVANCE(205);
      if (lookahead == 'B') ADVANCE(287);
      if (lookahead == 'N') ADVANCE(310);
      if (lookahead == 'S') ADVANCE(309);
      if (lookahead == '[') ADVANCE(221);
      if (lookahead == '\\') SKIP(6)
      if (lookahead == '^') ADVANCE(200);
      if (lookahead == '|') ADVANCE(199);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(15)
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(316);
      END_STATE();
    case 16:
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '/') ADVANCE(17);
      if (lookahead == '\\') SKIP(8)
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(16)
      END_STATE();
    case 17:
      if (lookahead == '*') ADVANCE(19);
      if (lookahead == '/') ADVANCE(318);
      END_STATE();
    case 18:
      if (lookahead == '*') ADVANCE(18);
      if (lookahead == '/') ADVANCE(317);
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 19:
      if (lookahead == '*') ADVANCE(18);
      if (lookahead != 0) ADVANCE(19);
      END_STATE();
    case 20:
      if (lookahead == '/') ADVANCE(17);
      if (lookahead == '\\') SKIP(12)
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(20)
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(316);
      END_STATE();
    case 21:
      if (lookahead == '=') ADVANCE(202);
      END_STATE();
    case 22:
      if (lookahead == '=') ADVANCE(201);
      if (lookahead == '>') ADVANCE(220);
      END_STATE();
    case 23:
      if (lookahead == 'a') ADVANCE(105);
      if (lookahead == 'l') ADVANCE(25);
      END_STATE();
    case 24:
      if (lookahead == 'a') ADVANCE(93);
      END_STATE();
    case 25:
      if (lookahead == 'a') ADVANCE(104);
      END_STATE();
    case 26:
      if (lookahead == 'a') ADVANCE(77);
      END_STATE();
    case 27:
      if (lookahead == 'a') ADVANCE(71);
      END_STATE();
    case 28:
      if (lookahead == 'a') ADVANCE(83);
      END_STATE();
    case 29:
      if (lookahead == 'a') ADVANCE(113);
      END_STATE();
    case 30:
      if (lookahead == 'b') ADVANCE(69);
      END_STATE();
    case 31:
      if (lookahead == 'b') ADVANCE(48);
      END_STATE();
    case 32:
      if (lookahead == 'c') ADVANCE(227);
      END_STATE();
    case 33:
      if (lookahead == 'c') ADVANCE(55);
      END_STATE();
    case 34:
      if (lookahead == 'c') ADVANCE(111);
      END_STATE();
    case 35:
      if (lookahead == 'c') ADVANCE(45);
      END_STATE();
    case 36:
      if (lookahead == 'c') ADVANCE(114);
      END_STATE();
    case 37:
      if (lookahead == 'd') ADVANCE(231);
      END_STATE();
    case 38:
      if (lookahead == 'd') ADVANCE(101);
      END_STATE();
    case 39:
      if (lookahead == 'e') ADVANCE(118);
      if (lookahead == 'u') ADVANCE(68);
      END_STATE();
    case 40:
      if (lookahead == 'e') ADVANCE(154);
      END_STATE();
    case 41:
      if (lookahead == 'e') ADVANCE(158);
      END_STATE();
    case 42:
      if (lookahead == 'e') ADVANCE(229);
      END_STATE();
    case 43:
      if (lookahead == 'e') ADVANCE(37);
      END_STATE();
    case 44:
      if (lookahead == 'e') ADVANCE(107);
      END_STATE();
    case 45:
      if (lookahead == 'e') ADVANCE(86);
      END_STATE();
    case 46:
      if (lookahead == 'e') ADVANCE(75);
      END_STATE();
    case 47:
      if (lookahead == 'e') ADVANCE(26);
      END_STATE();
    case 48:
      if (lookahead == 'e') ADVANCE(94);
      END_STATE();
    case 49:
      if (lookahead == 'e') ADVANCE(36);
      END_STATE();
    case 50:
      if (lookahead == 'f') ADVANCE(148);
      if (lookahead == 'm') ADVANCE(89);
      if (lookahead == 'n') ADVANCE(103);
      END_STATE();
    case 51:
      if (lookahead == 'f') ADVANCE(153);
      END_STATE();
    case 52:
      if (lookahead == 'f') ADVANCE(164);
      END_STATE();
    case 53:
      if (lookahead == 'g') ADVANCE(141);
      END_STATE();
    case 54:
      if (lookahead == 'g') ADVANCE(181);
      END_STATE();
    case 55:
      if (lookahead == 'h') ADVANCE(162);
      END_STATE();
    case 56:
      if (lookahead == 'h') ADVANCE(64);
      END_STATE();
    case 57:
      if (lookahead == 'i') ADVANCE(81);
      if (lookahead == 'o') ADVANCE(92);
      if (lookahead == 'u') ADVANCE(79);
      END_STATE();
    case 58:
      if (lookahead == 'i') ADVANCE(117);
      if (lookahead == 'o') ADVANCE(112);
      END_STATE();
    case 59:
      if (lookahead == 'i') ADVANCE(51);
      END_STATE();
    case 60:
      if (lookahead == 'i') ADVANCE(74);
      END_STATE();
    case 61:
      if (lookahead == 'i') ADVANCE(32);
      END_STATE();
    case 62:
      if (lookahead == 'i') ADVANCE(88);
      END_STATE();
    case 63:
      if (lookahead == 'i') ADVANCE(80);
      END_STATE();
    case 64:
      if (lookahead == 'i') ADVANCE(72);
      END_STATE();
    case 65:
      if (lookahead == 'l') ADVANCE(187);
      END_STATE();
    case 66:
      if (lookahead == 'l') ADVANCE(120);
      END_STATE();
    case 67:
      if (lookahead == 'l') ADVANCE(102);
      if (lookahead == 'x') ADVANCE(109);
      END_STATE();
    case 68:
      if (lookahead == 'l') ADVANCE(65);
      END_STATE();
    case 69:
      if (lookahead == 'l') ADVANCE(61);
      END_STATE();
    case 70:
      if (lookahead == 'l') ADVANCE(47);
      END_STATE();
    case 71:
      if (lookahead == 'l') ADVANCE(66);
      END_STATE();
    case 72:
      if (lookahead == 'l') ADVANCE(41);
      END_STATE();
    case 73:
      if (lookahead == 'm') ADVANCE(31);
      END_STATE();
    case 74:
      if (lookahead == 'n') ADVANCE(53);
      END_STATE();
    case 75:
      if (lookahead == 'n') ADVANCE(38);
      END_STATE();
    case 76:
      if (lookahead == 'n') ADVANCE(145);
      END_STATE();
    case 77:
      if (lookahead == 'n') ADVANCE(179);
      END_STATE();
    case 78:
      if (lookahead == 'n') ADVANCE(170);
      END_STATE();
    case 79:
      if (lookahead == 'n') ADVANCE(34);
      END_STATE();
    case 80:
      if (lookahead == 'n') ADVANCE(54);
      END_STATE();
    case 81:
      if (lookahead == 'n') ADVANCE(27);
      END_STATE();
    case 82:
      if (lookahead == 'n') ADVANCE(103);
      END_STATE();
    case 83:
      if (lookahead == 'n') ADVANCE(35);
      END_STATE();
    case 84:
      if (lookahead == 'o') ADVANCE(85);
      END_STATE();
    case 85:
      if (lookahead == 'o') ADVANCE(70);
      END_STATE();
    case 86:
      if (lookahead == 'o') ADVANCE(52);
      END_STATE();
    case 87:
      if (lookahead == 'o') ADVANCE(96);
      END_STATE();
    case 88:
      if (lookahead == 'o') ADVANCE(78);
      END_STATE();
    case 89:
      if (lookahead == 'p') ADVANCE(87);
      END_STATE();
    case 90:
      if (lookahead == 'r') ADVANCE(58);
      if (lookahead == 'u') ADVANCE(30);
      END_STATE();
    case 91:
      if (lookahead == 'r') ADVANCE(119);
      END_STATE();
    case 92:
      if (lookahead == 'r') ADVANCE(156);
      END_STATE();
    case 93:
      if (lookahead == 'r') ADVANCE(172);
      END_STATE();
    case 94:
      if (lookahead == 'r') ADVANCE(183);
      END_STATE();
    case 95:
      if (lookahead == 'r') ADVANCE(76);
      END_STATE();
    case 96:
      if (lookahead == 'r') ADVANCE(106);
      END_STATE();
    case 97:
      if (lookahead == 'r') ADVANCE(63);
      END_STATE();
    case 98:
      if (lookahead == 's') ADVANCE(147);
      END_STATE();
    case 99:
      if (lookahead == 's') ADVANCE(60);
      END_STATE();
    case 100:
      if (lookahead == 's') ADVANCE(167);
      END_STATE();
    case 101:
      if (lookahead == 's') ADVANCE(169);
      END_STATE();
    case 102:
      if (lookahead == 's') ADVANCE(40);
      END_STATE();
    case 103:
      if (lookahead == 's') ADVANCE(110);
      END_STATE();
    case 104:
      if (lookahead == 's') ADVANCE(100);
      END_STATE();
    case 105:
      if (lookahead == 't') ADVANCE(33);
      END_STATE();
    case 106:
      if (lookahead == 't') ADVANCE(143);
      END_STATE();
    case 107:
      if (lookahead == 't') ADVANCE(116);
      END_STATE();
    case 108:
      if (lookahead == 't') ADVANCE(97);
      END_STATE();
    case 109:
      if (lookahead == 't') ADVANCE(46);
      END_STATE();
    case 110:
      if (lookahead == 't') ADVANCE(28);
      END_STATE();
    case 111:
      if (lookahead == 't') ADVANCE(62);
      END_STATE();
    case 112:
      if (lookahead == 't') ADVANCE(49);
      END_STATE();
    case 113:
      if (lookahead == 't') ADVANCE(42);
      END_STATE();
    case 114:
      if (lookahead == 't') ADVANCE(43);
      END_STATE();
    case 115:
      if (lookahead == 'u') ADVANCE(73);
      END_STATE();
    case 116:
      if (lookahead == 'u') ADVANCE(95);
      END_STATE();
    case 117:
      if (lookahead == 'v') ADVANCE(29);
      END_STATE();
    case 118:
      if (lookahead == 'w') ADVANCE(224);
      END_STATE();
    case 119:
      if (lookahead == 'y') ADVANCE(160);
      END_STATE();
    case 120:
      if (lookahead == 'y') ADVANCE(165);
      END_STATE();
    case 121:
      if (lookahead == '{') ADVANCE(125);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(124);
      END_STATE();
    case 122:
      if (lookahead == '}') ADVANCE(226);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(122);
      END_STATE();
    case 123:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(226);
      END_STATE();
    case 124:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(126);
      END_STATE();
    case 125:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(122);
      END_STATE();
    case 126:
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'F') ||
          ('a' <= lookahead && lookahead <= 'f')) ADVANCE(123);
      END_STATE();
    case 127:
      if (lookahead != 0 &&
          lookahead != '\r') ADVANCE(318);
      if (lookahead == '\r') ADVANCE(319);
      END_STATE();
    case 128:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(0)
      END_STATE();
    case 129:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(0)
      if (lookahead == '\r') SKIP(128)
      END_STATE();
    case 130:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(136)
      END_STATE();
    case 131:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(136)
      if (lookahead == '\r') SKIP(130)
      END_STATE();
    case 132:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(137)
      END_STATE();
    case 133:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(137)
      if (lookahead == '\r') SKIP(132)
      END_STATE();
    case 134:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(138)
      END_STATE();
    case 135:
      if (eof) ADVANCE(139);
      if (lookahead == '\n') SKIP(138)
      if (lookahead == '\r') SKIP(134)
      END_STATE();
    case 136:
      if (eof) ADVANCE(139);
      if (lookahead == '!') ADVANCE(214);
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '(') ADVANCE(150);
      if (lookahead == '+') ADVANCE(209);
      if (lookahead == '-') ADVANCE(210);
      if (lookahead == '/') ADVANCE(17);
      if (lookahead == ';') ADVANCE(140);
      if (lookahead == '=') ADVANCE(174);
      if (lookahead == '[') ADVANCE(221);
      if (lookahead == '\\') SKIP(131)
      if (lookahead == 'c') ADVANCE(268);
      if (lookahead == 'f') ADVANCE(284);
      if (lookahead == 'i') ADVANCE(255);
      if (lookahead == 'n') ADVANCE(246);
      if (lookahead == 'p') ADVANCE(290);
      if (lookahead == 'r') ADVANCE(247);
      if (lookahead == 't') ADVANCE(291);
      if (lookahead == 'u') ADVANCE(298);
      if (lookahead == 'v') ADVANCE(234);
      if (lookahead == 'w') ADVANCE(259);
      if (lookahead == '{') ADVANCE(185);
      if (lookahead == '}') ADVANCE(186);
      if (lookahead == '~') ADVANCE(216);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(136)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(195);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(316);
      END_STATE();
    case 137:
      if (eof) ADVANCE(139);
      if (lookahead == '!') ADVANCE(214);
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '(') ADVANCE(150);
      if (lookahead == '+') ADVANCE(209);
      if (lookahead == '-') ADVANCE(210);
      if (lookahead == '/') ADVANCE(17);
      if (lookahead == '[') ADVANCE(221);
      if (lookahead == '\\') SKIP(133)
      if (lookahead == 'c') ADVANCE(237);
      if (lookahead == 'f') ADVANCE(265);
      if (lookahead == 'i') ADVANCE(255);
      if (lookahead == 'n') ADVANCE(246);
      if (lookahead == 'p') ADVANCE(290);
      if (lookahead == 'r') ADVANCE(247);
      if (lookahead == 't') ADVANCE(291);
      if (lookahead == 'u') ADVANCE(298);
      if (lookahead == 'v') ADVANCE(234);
      if (lookahead == 'w') ADVANCE(259);
      if (lookahead == '{') ADVANCE(185);
      if (lookahead == '}') ADVANCE(186);
      if (lookahead == '~') ADVANCE(216);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(137)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(195);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(316);
      END_STATE();
    case 138:
      if (eof) ADVANCE(139);
      if (lookahead == '!') ADVANCE(214);
      if (lookahead == '"') ADVANCE(189);
      if (lookahead == '(') ADVANCE(150);
      if (lookahead == '+') ADVANCE(209);
      if (lookahead == '-') ADVANCE(210);
      if (lookahead == '/') ADVANCE(17);
      if (lookahead == '[') ADVANCE(221);
      if (lookahead == '\\') SKIP(135)
      if (lookahead == 'c') ADVANCE(268);
      if (lookahead == 'e') ADVANCE(273);
      if (lookahead == 'f') ADVANCE(284);
      if (lookahead == 'i') ADVANCE(255);
      if (lookahead == 'n') ADVANCE(246);
      if (lookahead == 'p') ADVANCE(290);
      if (lookahead == 'r') ADVANCE(247);
      if (lookahead == 't') ADVANCE(291);
      if (lookahead == 'u') ADVANCE(298);
      if (lookahead == 'v') ADVANCE(234);
      if (lookahead == 'w') ADVANCE(259);
      if (lookahead == '{') ADVANCE(185);
      if (lookahead == '}') ADVANCE(186);
      if (lookahead == '~') ADVANCE(216);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(138)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(195);
      if (sym_identifier_character_set_1(lookahead)) ADVANCE(316);
      END_STATE();
    case 139:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 140:
      ACCEPT_TOKEN(anon_sym_SEMI);
      END_STATE();
    case 141:
      ACCEPT_TOKEN(anon_sym_using);
      END_STATE();
    case 142:
      ACCEPT_TOKEN(anon_sym_using);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 143:
      ACCEPT_TOKEN(anon_sym_import);
      END_STATE();
    case 144:
      ACCEPT_TOKEN(anon_sym_import);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 145:
      ACCEPT_TOKEN(anon_sym_return);
      END_STATE();
    case 146:
      ACCEPT_TOKEN(anon_sym_return);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 147:
      ACCEPT_TOKEN(anon_sym_as);
      END_STATE();
    case 148:
      ACCEPT_TOKEN(anon_sym_if);
      END_STATE();
    case 149:
      ACCEPT_TOKEN(anon_sym_if);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 150:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 151:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      if (lookahead == ')') ADVANCE(177);
      END_STATE();
    case 152:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 153:
      ACCEPT_TOKEN(anon_sym_elseif);
      END_STATE();
    case 154:
      ACCEPT_TOKEN(anon_sym_else);
      if (lookahead == ' ') ADVANCE(59);
      END_STATE();
    case 155:
      ACCEPT_TOKEN(anon_sym_else);
      if (lookahead == ' ') ADVANCE(59);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 156:
      ACCEPT_TOKEN(anon_sym_for);
      END_STATE();
    case 157:
      ACCEPT_TOKEN(anon_sym_for);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 158:
      ACCEPT_TOKEN(anon_sym_while);
      END_STATE();
    case 159:
      ACCEPT_TOKEN(anon_sym_while);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 160:
      ACCEPT_TOKEN(anon_sym_try);
      END_STATE();
    case 161:
      ACCEPT_TOKEN(anon_sym_try);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 162:
      ACCEPT_TOKEN(anon_sym_catch);
      END_STATE();
    case 163:
      ACCEPT_TOKEN(anon_sym_catch);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 164:
      ACCEPT_TOKEN(anon_sym_instanceof);
      END_STATE();
    case 165:
      ACCEPT_TOKEN(anon_sym_finally);
      END_STATE();
    case 166:
      ACCEPT_TOKEN(anon_sym_finally);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 167:
      ACCEPT_TOKEN(anon_sym_class);
      END_STATE();
    case 168:
      ACCEPT_TOKEN(anon_sym_class);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 169:
      ACCEPT_TOKEN(anon_sym_extends);
      END_STATE();
    case 170:
      ACCEPT_TOKEN(anon_sym_function);
      END_STATE();
    case 171:
      ACCEPT_TOKEN(anon_sym_function);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 172:
      ACCEPT_TOKEN(anon_sym_var);
      END_STATE();
    case 173:
      ACCEPT_TOKEN(anon_sym_var);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 174:
      ACCEPT_TOKEN(anon_sym_EQ);
      END_STATE();
    case 175:
      ACCEPT_TOKEN(anon_sym_EQ);
      if (lookahead == '=') ADVANCE(201);
      END_STATE();
    case 176:
      ACCEPT_TOKEN(anon_sym_EQ);
      if (lookahead == '=') ADVANCE(201);
      if (lookahead == '>') ADVANCE(220);
      END_STATE();
    case 177:
      ACCEPT_TOKEN(anon_sym_LPAREN_RPAREN);
      END_STATE();
    case 178:
      ACCEPT_TOKEN(anon_sym_COMMA);
      END_STATE();
    case 179:
      ACCEPT_TOKEN(anon_sym_Boolean);
      END_STATE();
    case 180:
      ACCEPT_TOKEN(anon_sym_Boolean);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 181:
      ACCEPT_TOKEN(anon_sym_String);
      END_STATE();
    case 182:
      ACCEPT_TOKEN(anon_sym_String);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 183:
      ACCEPT_TOKEN(anon_sym_Number);
      END_STATE();
    case 184:
      ACCEPT_TOKEN(anon_sym_Number);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 185:
      ACCEPT_TOKEN(anon_sym_LBRACE);
      END_STATE();
    case 186:
      ACCEPT_TOKEN(anon_sym_RBRACE);
      END_STATE();
    case 187:
      ACCEPT_TOKEN(sym_null_expression);
      END_STATE();
    case 188:
      ACCEPT_TOKEN(sym_null_expression);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 189:
      ACCEPT_TOKEN(anon_sym_DQUOTE);
      END_STATE();
    case 190:
      ACCEPT_TOKEN(aux_sym_string_expression_token1);
      if (lookahead == '*') ADVANCE(192);
      if (lookahead == '/') ADVANCE(194);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '"' &&
          lookahead != '\\') ADVANCE(194);
      END_STATE();
    case 191:
      ACCEPT_TOKEN(aux_sym_string_expression_token1);
      if (lookahead == '*') ADVANCE(191);
      if (lookahead == '/') ADVANCE(194);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '"' &&
          lookahead != '\\') ADVANCE(192);
      END_STATE();
    case 192:
      ACCEPT_TOKEN(aux_sym_string_expression_token1);
      if (lookahead == '*') ADVANCE(191);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '"' &&
          lookahead != '\\') ADVANCE(192);
      END_STATE();
    case 193:
      ACCEPT_TOKEN(aux_sym_string_expression_token1);
      if (lookahead == '/') ADVANCE(190);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(193);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '"' &&
          lookahead != '\\') ADVANCE(194);
      END_STATE();
    case 194:
      ACCEPT_TOKEN(aux_sym_string_expression_token1);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '"' &&
          lookahead != '\\') ADVANCE(194);
      END_STATE();
    case 195:
      ACCEPT_TOKEN(sym_number_expression);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(195);
      END_STATE();
    case 196:
      ACCEPT_TOKEN(anon_sym_AMP_AMP);
      END_STATE();
    case 197:
      ACCEPT_TOKEN(anon_sym_PIPE_PIPE);
      END_STATE();
    case 198:
      ACCEPT_TOKEN(anon_sym_AMP);
      if (lookahead == '&') ADVANCE(196);
      END_STATE();
    case 199:
      ACCEPT_TOKEN(anon_sym_PIPE);
      if (lookahead == '|') ADVANCE(197);
      END_STATE();
    case 200:
      ACCEPT_TOKEN(anon_sym_CARET);
      END_STATE();
    case 201:
      ACCEPT_TOKEN(anon_sym_EQ_EQ);
      END_STATE();
    case 202:
      ACCEPT_TOKEN(anon_sym_BANG_EQ);
      END_STATE();
    case 203:
      ACCEPT_TOKEN(anon_sym_LT);
      if (lookahead == '<') ADVANCE(207);
      if (lookahead == '=') ADVANCE(204);
      END_STATE();
    case 204:
      ACCEPT_TOKEN(anon_sym_LT_EQ);
      END_STATE();
    case 205:
      ACCEPT_TOKEN(anon_sym_GT);
      if (lookahead == '=') ADVANCE(206);
      if (lookahead == '>') ADVANCE(208);
      END_STATE();
    case 206:
      ACCEPT_TOKEN(anon_sym_GT_EQ);
      END_STATE();
    case 207:
      ACCEPT_TOKEN(anon_sym_LT_LT);
      END_STATE();
    case 208:
      ACCEPT_TOKEN(anon_sym_GT_GT);
      END_STATE();
    case 209:
      ACCEPT_TOKEN(anon_sym_PLUS);
      if (lookahead == '+') ADVANCE(218);
      END_STATE();
    case 210:
      ACCEPT_TOKEN(anon_sym_DASH);
      if (lookahead == '-') ADVANCE(217);
      END_STATE();
    case 211:
      ACCEPT_TOKEN(anon_sym_STAR);
      END_STATE();
    case 212:
      ACCEPT_TOKEN(anon_sym_SLASH);
      if (lookahead == '*') ADVANCE(19);
      if (lookahead == '/') ADVANCE(318);
      END_STATE();
    case 213:
      ACCEPT_TOKEN(anon_sym_PERCENT);
      END_STATE();
    case 214:
      ACCEPT_TOKEN(anon_sym_BANG);
      END_STATE();
    case 215:
      ACCEPT_TOKEN(anon_sym_BANG);
      if (lookahead == '=') ADVANCE(202);
      END_STATE();
    case 216:
      ACCEPT_TOKEN(anon_sym_TILDE);
      END_STATE();
    case 217:
      ACCEPT_TOKEN(anon_sym_DASH_DASH);
      END_STATE();
    case 218:
      ACCEPT_TOKEN(anon_sym_PLUS_PLUS);
      END_STATE();
    case 219:
      ACCEPT_TOKEN(anon_sym_COLON);
      END_STATE();
    case 220:
      ACCEPT_TOKEN(anon_sym_EQ_GT);
      END_STATE();
    case 221:
      ACCEPT_TOKEN(anon_sym_LBRACK);
      END_STATE();
    case 222:
      ACCEPT_TOKEN(anon_sym_RBRACK);
      END_STATE();
    case 223:
      ACCEPT_TOKEN(anon_sym_DOT);
      END_STATE();
    case 224:
      ACCEPT_TOKEN(anon_sym_new);
      END_STATE();
    case 225:
      ACCEPT_TOKEN(anon_sym_new);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 226:
      ACCEPT_TOKEN(sym_escape_sequence);
      END_STATE();
    case 227:
      ACCEPT_TOKEN(anon_sym_public);
      END_STATE();
    case 228:
      ACCEPT_TOKEN(anon_sym_public);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 229:
      ACCEPT_TOKEN(anon_sym_private);
      END_STATE();
    case 230:
      ACCEPT_TOKEN(anon_sym_private);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 231:
      ACCEPT_TOKEN(anon_sym_protected);
      END_STATE();
    case 232:
      ACCEPT_TOKEN(anon_sym_protected);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 233:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(300);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(316);
      END_STATE();
    case 234:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(293);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(316);
      END_STATE();
    case 235:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(306);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(316);
      END_STATE();
    case 236:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(281);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(316);
      END_STATE();
    case 237:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(305);
      if (lookahead == 'l') ADVANCE(233);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(316);
      END_STATE();
    case 238:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'a') ADVANCE(274);
      if (sym_identifier_character_set_3(lookahead)) ADVANCE(316);
      END_STATE();
    case 239:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'b') ADVANCE(272);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 240:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'b') ADVANCE(253);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 241:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'c') ADVANCE(228);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 242:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'c') ADVANCE(258);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 243:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'c') ADVANCE(307);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 244:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'c') ADVANCE(308);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 245:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'd') ADVANCE(232);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 246:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(313);
      if (lookahead == 'u') ADVANCE(270);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 247:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(302);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 248:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(159);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 249:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(230);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 250:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(245);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 251:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(155);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 252:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(236);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 253:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(294);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 254:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'e') ADVANCE(244);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 255:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'f') ADVANCE(149);
      if (lookahead == 'm') ADVANCE(289);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 256:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'g') ADVANCE(142);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 257:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'g') ADVANCE(182);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 258:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'h') ADVANCE(163);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 259:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'h') ADVANCE(264);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 260:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(312);
      if (lookahead == 'o') ADVANCE(304);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 261:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(278);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 262:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(241);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 263:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(286);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 264:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(271);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 265:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(283);
      if (lookahead == 'o') ADVANCE(292);
      if (lookahead == 'u') ADVANCE(277);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 266:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'i') ADVANCE(282);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 267:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(188);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 268:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(233);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 269:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(315);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 270:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(267);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 271:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(248);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 272:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(262);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 273:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(301);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 274:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(269);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 275:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'l') ADVANCE(252);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 276:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'm') ADVANCE(240);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 277:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(243);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 278:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(256);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 279:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(146);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 280:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(171);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 281:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(180);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 282:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(257);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 283:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'n') ADVANCE(238);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 284:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(292);
      if (lookahead == 'u') ADVANCE(277);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 285:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(296);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 286:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(280);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 287:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(288);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 288:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'o') ADVANCE(275);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 289:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'p') ADVANCE(285);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 290:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(260);
      if (lookahead == 'u') ADVANCE(239);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 291:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(314);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 292:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(157);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 293:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(173);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 294:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(184);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 295:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(279);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 296:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(303);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 297:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'r') ADVANCE(266);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 298:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(261);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 299:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(168);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 300:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(299);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 301:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 's') ADVANCE(251);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 302:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(311);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 303:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(144);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 304:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(254);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 305:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(242);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 306:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(249);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 307:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(263);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 308:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(250);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 309:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 't') ADVANCE(297);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 310:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'u') ADVANCE(276);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 311:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'u') ADVANCE(295);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 312:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'v') ADVANCE(235);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 313:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'w') ADVANCE(225);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 314:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'y') ADVANCE(161);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 315:
      ACCEPT_TOKEN(sym_identifier);
      if (lookahead == 'y') ADVANCE(166);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 316:
      ACCEPT_TOKEN(sym_identifier);
      if (sym_identifier_character_set_2(lookahead)) ADVANCE(316);
      END_STATE();
    case 317:
      ACCEPT_TOKEN(sym_comment);
      END_STATE();
    case 318:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead == '\\') ADVANCE(127);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(318);
      END_STATE();
    case 319:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead != 0 &&
          lookahead != '\\') ADVANCE(318);
      if (lookahead == '\\') ADVANCE(127);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 136},
  [2] = {.lex_state = 136},
  [3] = {.lex_state = 136},
  [4] = {.lex_state = 136},
  [5] = {.lex_state = 136},
  [6] = {.lex_state = 136},
  [7] = {.lex_state = 136},
  [8] = {.lex_state = 136},
  [9] = {.lex_state = 136},
  [10] = {.lex_state = 14},
  [11] = {.lex_state = 137},
  [12] = {.lex_state = 138},
  [13] = {.lex_state = 138},
  [14] = {.lex_state = 137},
  [15] = {.lex_state = 138},
  [16] = {.lex_state = 14},
  [17] = {.lex_state = 14},
  [18] = {.lex_state = 14},
  [19] = {.lex_state = 14},
  [20] = {.lex_state = 14},
  [21] = {.lex_state = 14},
  [22] = {.lex_state = 137},
  [23] = {.lex_state = 137},
  [24] = {.lex_state = 138},
  [25] = {.lex_state = 14},
  [26] = {.lex_state = 138},
  [27] = {.lex_state = 14},
  [28] = {.lex_state = 14},
  [29] = {.lex_state = 137},
  [30] = {.lex_state = 14},
  [31] = {.lex_state = 14},
  [32] = {.lex_state = 14},
  [33] = {.lex_state = 138},
  [34] = {.lex_state = 14},
  [35] = {.lex_state = 14},
  [36] = {.lex_state = 14},
  [37] = {.lex_state = 138},
  [38] = {.lex_state = 14},
  [39] = {.lex_state = 14},
  [40] = {.lex_state = 14},
  [41] = {.lex_state = 14},
  [42] = {.lex_state = 14},
  [43] = {.lex_state = 14},
  [44] = {.lex_state = 14},
  [45] = {.lex_state = 14},
  [46] = {.lex_state = 14},
  [47] = {.lex_state = 14},
  [48] = {.lex_state = 137},
  [49] = {.lex_state = 14},
  [50] = {.lex_state = 136},
  [51] = {.lex_state = 136},
  [52] = {.lex_state = 136},
  [53] = {.lex_state = 136},
  [54] = {.lex_state = 136},
  [55] = {.lex_state = 136},
  [56] = {.lex_state = 136},
  [57] = {.lex_state = 136},
  [58] = {.lex_state = 136},
  [59] = {.lex_state = 136},
  [60] = {.lex_state = 136},
  [61] = {.lex_state = 136},
  [62] = {.lex_state = 136},
  [63] = {.lex_state = 136},
  [64] = {.lex_state = 136},
  [65] = {.lex_state = 136},
  [66] = {.lex_state = 136},
  [67] = {.lex_state = 136},
  [68] = {.lex_state = 136},
  [69] = {.lex_state = 136},
  [70] = {.lex_state = 136},
  [71] = {.lex_state = 136},
  [72] = {.lex_state = 136},
  [73] = {.lex_state = 136},
  [74] = {.lex_state = 136},
  [75] = {.lex_state = 136},
  [76] = {.lex_state = 136},
  [77] = {.lex_state = 136},
  [78] = {.lex_state = 13},
  [79] = {.lex_state = 13},
  [80] = {.lex_state = 13},
  [81] = {.lex_state = 13},
  [82] = {.lex_state = 13},
  [83] = {.lex_state = 13},
  [84] = {.lex_state = 13},
  [85] = {.lex_state = 13},
  [86] = {.lex_state = 13},
  [87] = {.lex_state = 13},
  [88] = {.lex_state = 13},
  [89] = {.lex_state = 13},
  [90] = {.lex_state = 13},
  [91] = {.lex_state = 13},
  [92] = {.lex_state = 13},
  [93] = {.lex_state = 13},
  [94] = {.lex_state = 13},
  [95] = {.lex_state = 13},
  [96] = {.lex_state = 13},
  [97] = {.lex_state = 13},
  [98] = {.lex_state = 13},
  [99] = {.lex_state = 13},
  [100] = {.lex_state = 13},
  [101] = {.lex_state = 13},
  [102] = {.lex_state = 13},
  [103] = {.lex_state = 13},
  [104] = {.lex_state = 13},
  [105] = {.lex_state = 13},
  [106] = {.lex_state = 13},
  [107] = {.lex_state = 13},
  [108] = {.lex_state = 13},
  [109] = {.lex_state = 13},
  [110] = {.lex_state = 14},
  [111] = {.lex_state = 14},
  [112] = {.lex_state = 14},
  [113] = {.lex_state = 14},
  [114] = {.lex_state = 15},
  [115] = {.lex_state = 14},
  [116] = {.lex_state = 14},
  [117] = {.lex_state = 14},
  [118] = {.lex_state = 14},
  [119] = {.lex_state = 14},
  [120] = {.lex_state = 14},
  [121] = {.lex_state = 14},
  [122] = {.lex_state = 14},
  [123] = {.lex_state = 14},
  [124] = {.lex_state = 14},
  [125] = {.lex_state = 14},
  [126] = {.lex_state = 14},
  [127] = {.lex_state = 14},
  [128] = {.lex_state = 14},
  [129] = {.lex_state = 14},
  [130] = {.lex_state = 14},
  [131] = {.lex_state = 14},
  [132] = {.lex_state = 13},
  [133] = {.lex_state = 13},
  [134] = {.lex_state = 13},
  [135] = {.lex_state = 13},
  [136] = {.lex_state = 0},
  [137] = {.lex_state = 0},
  [138] = {.lex_state = 0},
  [139] = {.lex_state = 0},
  [140] = {.lex_state = 15},
  [141] = {.lex_state = 0},
  [142] = {.lex_state = 0},
  [143] = {.lex_state = 0},
  [144] = {.lex_state = 0},
  [145] = {.lex_state = 9},
  [146] = {.lex_state = 0},
  [147] = {.lex_state = 0},
  [148] = {.lex_state = 9},
  [149] = {.lex_state = 9},
  [150] = {.lex_state = 0},
  [151] = {.lex_state = 0},
  [152] = {.lex_state = 0},
  [153] = {.lex_state = 0},
  [154] = {.lex_state = 0},
  [155] = {.lex_state = 0},
  [156] = {.lex_state = 0},
  [157] = {.lex_state = 20},
  [158] = {.lex_state = 0},
  [159] = {.lex_state = 0},
  [160] = {.lex_state = 0},
  [161] = {.lex_state = 0},
  [162] = {.lex_state = 0},
  [163] = {.lex_state = 0},
  [164] = {.lex_state = 20},
  [165] = {.lex_state = 0},
  [166] = {.lex_state = 0},
  [167] = {.lex_state = 0},
  [168] = {.lex_state = 0},
  [169] = {.lex_state = 136},
  [170] = {.lex_state = 0},
  [171] = {.lex_state = 20},
  [172] = {.lex_state = 0},
  [173] = {.lex_state = 0},
  [174] = {.lex_state = 0},
  [175] = {.lex_state = 0},
  [176] = {.lex_state = 136},
  [177] = {.lex_state = 0},
  [178] = {.lex_state = 0},
  [179] = {.lex_state = 0},
  [180] = {.lex_state = 136},
  [181] = {.lex_state = 0},
  [182] = {.lex_state = 0},
  [183] = {.lex_state = 20},
  [184] = {.lex_state = 136},
  [185] = {.lex_state = 0},
  [186] = {.lex_state = 0},
  [187] = {.lex_state = 0},
  [188] = {.lex_state = 0},
  [189] = {.lex_state = 0},
  [190] = {.lex_state = 0},
  [191] = {.lex_state = 0},
  [192] = {.lex_state = 0},
  [193] = {.lex_state = 20},
  [194] = {.lex_state = 20},
  [195] = {.lex_state = 20},
  [196] = {.lex_state = 20},
  [197] = {.lex_state = 14},
  [198] = {.lex_state = 20},
  [199] = {.lex_state = 14},
  [200] = {.lex_state = 0},
  [201] = {.lex_state = 20},
  [202] = {.lex_state = 20},
  [203] = {.lex_state = 20},
  [204] = {.lex_state = 0},
  [205] = {.lex_state = 0},
  [206] = {.lex_state = 0},
  [207] = {.lex_state = 0},
  [208] = {.lex_state = 0},
  [209] = {.lex_state = 0},
  [210] = {.lex_state = 20},
  [211] = {.lex_state = 0},
  [212] = {.lex_state = 20},
  [213] = {.lex_state = 20},
  [214] = {.lex_state = 20},
  [215] = {.lex_state = 20},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_SEMI] = ACTIONS(1),
    [anon_sym_using] = ACTIONS(1),
    [anon_sym_import] = ACTIONS(1),
    [anon_sym_return] = ACTIONS(1),
    [anon_sym_as] = ACTIONS(1),
    [anon_sym_if] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [anon_sym_elseif] = ACTIONS(1),
    [anon_sym_else] = ACTIONS(1),
    [anon_sym_for] = ACTIONS(1),
    [anon_sym_while] = ACTIONS(1),
    [anon_sym_try] = ACTIONS(1),
    [anon_sym_catch] = ACTIONS(1),
    [anon_sym_instanceof] = ACTIONS(1),
    [anon_sym_finally] = ACTIONS(1),
    [anon_sym_class] = ACTIONS(1),
    [anon_sym_extends] = ACTIONS(1),
    [anon_sym_function] = ACTIONS(1),
    [anon_sym_var] = ACTIONS(1),
    [anon_sym_EQ] = ACTIONS(1),
    [anon_sym_LPAREN_RPAREN] = ACTIONS(1),
    [anon_sym_COMMA] = ACTIONS(1),
    [anon_sym_Boolean] = ACTIONS(1),
    [anon_sym_String] = ACTIONS(1),
    [anon_sym_Number] = ACTIONS(1),
    [anon_sym_LBRACE] = ACTIONS(1),
    [anon_sym_RBRACE] = ACTIONS(1),
    [sym_null_expression] = ACTIONS(1),
    [anon_sym_DQUOTE] = ACTIONS(1),
    [sym_number_expression] = ACTIONS(1),
    [anon_sym_AMP_AMP] = ACTIONS(1),
    [anon_sym_PIPE_PIPE] = ACTIONS(1),
    [anon_sym_AMP] = ACTIONS(1),
    [anon_sym_PIPE] = ACTIONS(1),
    [anon_sym_CARET] = ACTIONS(1),
    [anon_sym_EQ_EQ] = ACTIONS(1),
    [anon_sym_BANG_EQ] = ACTIONS(1),
    [anon_sym_LT] = ACTIONS(1),
    [anon_sym_LT_EQ] = ACTIONS(1),
    [anon_sym_GT] = ACTIONS(1),
    [anon_sym_GT_EQ] = ACTIONS(1),
    [anon_sym_LT_LT] = ACTIONS(1),
    [anon_sym_GT_GT] = ACTIONS(1),
    [anon_sym_PLUS] = ACTIONS(1),
    [anon_sym_DASH] = ACTIONS(1),
    [anon_sym_STAR] = ACTIONS(1),
    [anon_sym_SLASH] = ACTIONS(1),
    [anon_sym_PERCENT] = ACTIONS(1),
    [anon_sym_BANG] = ACTIONS(1),
    [anon_sym_TILDE] = ACTIONS(1),
    [anon_sym_DASH_DASH] = ACTIONS(1),
    [anon_sym_PLUS_PLUS] = ACTIONS(1),
    [anon_sym_COLON] = ACTIONS(1),
    [anon_sym_EQ_GT] = ACTIONS(1),
    [anon_sym_LBRACK] = ACTIONS(1),
    [anon_sym_RBRACK] = ACTIONS(1),
    [anon_sym_DOT] = ACTIONS(1),
    [anon_sym_new] = ACTIONS(1),
    [anon_sym_public] = ACTIONS(1),
    [anon_sym_private] = ACTIONS(1),
    [anon_sym_protected] = ACTIONS(1),
    [sym_comment] = ACTIONS(3),
  },
  [1] = {
    [sym_source_file] = STATE(205),
    [sym__statement] = STATE(9),
    [sym__expression_statement] = STATE(9),
    [sym__expression] = STATE(122),
    [sym__declaration_statement] = STATE(9),
    [sym_import_statement] = STATE(9),
    [sym_return_statement] = STATE(9),
    [sym_if_statement] = STATE(9),
    [sym_for_statement] = STATE(9),
    [sym_while_statement] = STATE(9),
    [sym_try_statement] = STATE(9),
    [sym_class_declaration] = STATE(9),
    [sym_function_declaration] = STATE(9),
    [sym_variable_declaration] = STATE(9),
    [sym_variable_assingment] = STATE(9),
    [sym_string_expression] = STATE(122),
    [sym_binary_expression] = STATE(122),
    [sym_unary_expression] = STATE(122),
    [sym_update_expression] = STATE(122),
    [sym_option_dictionary_expression] = STATE(122),
    [sym_dictionary_expression] = STATE(122),
    [sym_array_expression] = STATE(122),
    [sym_attribute_expression] = STATE(122),
    [sym_index_expression] = STATE(122),
    [sym_class_creation_expression] = STATE(122),
    [sym_parenthesized_expression] = STATE(122),
    [sym__modifier] = STATE(181),
    [aux_sym_source_file_repeat1] = STATE(9),
    [ts_builtin_sym_end] = ACTIONS(5),
    [anon_sym_using] = ACTIONS(7),
    [anon_sym_import] = ACTIONS(7),
    [anon_sym_return] = ACTIONS(9),
    [anon_sym_if] = ACTIONS(11),
    [anon_sym_LPAREN] = ACTIONS(13),
    [anon_sym_for] = ACTIONS(15),
    [anon_sym_while] = ACTIONS(17),
    [anon_sym_try] = ACTIONS(19),
    [anon_sym_class] = ACTIONS(21),
    [anon_sym_function] = ACTIONS(23),
    [anon_sym_var] = ACTIONS(25),
    [anon_sym_LBRACE] = ACTIONS(27),
    [sym_null_expression] = ACTIONS(29),
    [anon_sym_DQUOTE] = ACTIONS(31),
    [sym_number_expression] = ACTIONS(33),
    [anon_sym_PLUS] = ACTIONS(35),
    [anon_sym_DASH] = ACTIONS(35),
    [anon_sym_BANG] = ACTIONS(37),
    [anon_sym_TILDE] = ACTIONS(37),
    [anon_sym_DASH_DASH] = ACTIONS(39),
    [anon_sym_PLUS_PLUS] = ACTIONS(39),
    [anon_sym_LBRACK] = ACTIONS(41),
    [anon_sym_new] = ACTIONS(43),
    [anon_sym_public] = ACTIONS(45),
    [anon_sym_private] = ACTIONS(45),
    [anon_sym_protected] = ACTIONS(45),
    [sym_identifier] = ACTIONS(47),
    [sym_comment] = ACTIONS(3),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(54), 1,
      anon_sym_return,
    ACTIONS(57), 1,
      anon_sym_if,
    ACTIONS(60), 1,
      anon_sym_LPAREN,
    ACTIONS(63), 1,
      anon_sym_for,
    ACTIONS(66), 1,
      anon_sym_while,
    ACTIONS(69), 1,
      anon_sym_try,
    ACTIONS(72), 1,
      anon_sym_class,
    ACTIONS(75), 1,
      anon_sym_function,
    ACTIONS(78), 1,
      anon_sym_var,
    ACTIONS(81), 1,
      anon_sym_LBRACE,
    ACTIONS(84), 1,
      sym_null_expression,
    ACTIONS(87), 1,
      anon_sym_DQUOTE,
    ACTIONS(90), 1,
      sym_number_expression,
    ACTIONS(102), 1,
      anon_sym_LBRACK,
    ACTIONS(105), 1,
      anon_sym_new,
    ACTIONS(111), 1,
      sym_identifier,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(49), 2,
      ts_builtin_sym_end,
      anon_sym_RBRACE,
    ACTIONS(51), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(93), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(96), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(99), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(108), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(2), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [110] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_return,
    ACTIONS(11), 1,
      anon_sym_if,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(15), 1,
      anon_sym_for,
    ACTIONS(17), 1,
      anon_sym_while,
    ACTIONS(19), 1,
      anon_sym_try,
    ACTIONS(21), 1,
      anon_sym_class,
    ACTIONS(23), 1,
      anon_sym_function,
    ACTIONS(25), 1,
      anon_sym_var,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      sym_null_expression,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(33), 1,
      sym_number_expression,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(47), 1,
      sym_identifier,
    ACTIONS(114), 1,
      anon_sym_RBRACE,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(7), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(45), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(2), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [219] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_return,
    ACTIONS(11), 1,
      anon_sym_if,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(15), 1,
      anon_sym_for,
    ACTIONS(17), 1,
      anon_sym_while,
    ACTIONS(19), 1,
      anon_sym_try,
    ACTIONS(21), 1,
      anon_sym_class,
    ACTIONS(23), 1,
      anon_sym_function,
    ACTIONS(25), 1,
      anon_sym_var,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      sym_null_expression,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(33), 1,
      sym_number_expression,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(47), 1,
      sym_identifier,
    ACTIONS(116), 1,
      anon_sym_RBRACE,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(7), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(45), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(8), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [328] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_return,
    ACTIONS(11), 1,
      anon_sym_if,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(15), 1,
      anon_sym_for,
    ACTIONS(17), 1,
      anon_sym_while,
    ACTIONS(19), 1,
      anon_sym_try,
    ACTIONS(21), 1,
      anon_sym_class,
    ACTIONS(23), 1,
      anon_sym_function,
    ACTIONS(25), 1,
      anon_sym_var,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      sym_null_expression,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(33), 1,
      sym_number_expression,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(47), 1,
      sym_identifier,
    ACTIONS(118), 1,
      anon_sym_RBRACE,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(7), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(45), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(7), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [437] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_return,
    ACTIONS(11), 1,
      anon_sym_if,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(15), 1,
      anon_sym_for,
    ACTIONS(17), 1,
      anon_sym_while,
    ACTIONS(19), 1,
      anon_sym_try,
    ACTIONS(21), 1,
      anon_sym_class,
    ACTIONS(23), 1,
      anon_sym_function,
    ACTIONS(25), 1,
      anon_sym_var,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      sym_null_expression,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(33), 1,
      sym_number_expression,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(47), 1,
      sym_identifier,
    ACTIONS(120), 1,
      anon_sym_RBRACE,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(7), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(45), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(3), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [546] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_return,
    ACTIONS(11), 1,
      anon_sym_if,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(15), 1,
      anon_sym_for,
    ACTIONS(17), 1,
      anon_sym_while,
    ACTIONS(19), 1,
      anon_sym_try,
    ACTIONS(21), 1,
      anon_sym_class,
    ACTIONS(23), 1,
      anon_sym_function,
    ACTIONS(25), 1,
      anon_sym_var,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      sym_null_expression,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(33), 1,
      sym_number_expression,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(47), 1,
      sym_identifier,
    ACTIONS(122), 1,
      anon_sym_RBRACE,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(7), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(45), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(2), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [655] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_return,
    ACTIONS(11), 1,
      anon_sym_if,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(15), 1,
      anon_sym_for,
    ACTIONS(17), 1,
      anon_sym_while,
    ACTIONS(19), 1,
      anon_sym_try,
    ACTIONS(21), 1,
      anon_sym_class,
    ACTIONS(23), 1,
      anon_sym_function,
    ACTIONS(25), 1,
      anon_sym_var,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      sym_null_expression,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(33), 1,
      sym_number_expression,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(47), 1,
      sym_identifier,
    ACTIONS(124), 1,
      anon_sym_RBRACE,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(7), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(45), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(2), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [764] = 26,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(9), 1,
      anon_sym_return,
    ACTIONS(11), 1,
      anon_sym_if,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(15), 1,
      anon_sym_for,
    ACTIONS(17), 1,
      anon_sym_while,
    ACTIONS(19), 1,
      anon_sym_try,
    ACTIONS(21), 1,
      anon_sym_class,
    ACTIONS(23), 1,
      anon_sym_function,
    ACTIONS(25), 1,
      anon_sym_var,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(29), 1,
      sym_null_expression,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(33), 1,
      sym_number_expression,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(47), 1,
      sym_identifier,
    ACTIONS(126), 1,
      ts_builtin_sym_end,
    STATE(181), 1,
      sym__modifier,
    ACTIONS(7), 2,
      anon_sym_using,
      anon_sym_import,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(45), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
    STATE(122), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
    STATE(2), 14,
      sym__statement,
      sym__expression_statement,
      sym__declaration_statement,
      sym_import_statement,
      sym_return_statement,
      sym_if_statement,
      sym_for_statement,
      sym_while_statement,
      sym_try_statement,
      sym_class_declaration,
      sym_function_declaration,
      sym_variable_declaration,
      sym_variable_assingment,
      aux_sym_source_file_repeat1,
  [873] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(130), 1,
      anon_sym_LPAREN,
    ACTIONS(132), 1,
      anon_sym_LPAREN_RPAREN,
    STATE(41), 2,
      sym_empty_argument_list,
      sym_argument_list,
    ACTIONS(134), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(128), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [922] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(140), 1,
      anon_sym_catch,
    ACTIONS(142), 1,
      anon_sym_finally,
    STATE(67), 1,
      sym_finally_clause,
    STATE(14), 2,
      sym_catch_clause,
      aux_sym_try_statement_repeat1,
    ACTIONS(136), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(138), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [972] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(148), 1,
      anon_sym_elseif,
    ACTIONS(150), 1,
      anon_sym_else,
    STATE(13), 1,
      aux_sym_if_statement_repeat1,
    STATE(33), 1,
      sym_elif_clause,
    STATE(70), 1,
      sym_else_clause,
    ACTIONS(144), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(146), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1024] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(148), 1,
      anon_sym_elseif,
    ACTIONS(150), 1,
      anon_sym_else,
    STATE(15), 1,
      aux_sym_if_statement_repeat1,
    STATE(33), 1,
      sym_elif_clause,
    STATE(62), 1,
      sym_else_clause,
    ACTIONS(152), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(154), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1076] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(160), 1,
      anon_sym_catch,
    STATE(14), 2,
      sym_catch_clause,
      aux_sym_try_statement_repeat1,
    ACTIONS(156), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(158), 19,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_finally,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1121] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(167), 1,
      anon_sym_elseif,
    STATE(15), 1,
      aux_sym_if_statement_repeat1,
    STATE(33), 1,
      sym_elif_clause,
    ACTIONS(163), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(165), 19,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_else,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1168] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(172), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(170), 25,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_EQ_GT,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1208] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(176), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(174), 25,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1248] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(180), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(178), 25,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1288] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(184), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(182), 25,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1328] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(188), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(186), 25,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_EQ_GT,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1368] = 8,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(192), 4,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(190), 19,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1417] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(204), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(206), 20,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_catch,
      anon_sym_finally,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1456] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(208), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(210), 20,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_catch,
      anon_sym_finally,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1495] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(204), 12,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_elseif,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(206), 19,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_else,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1534] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(214), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(212), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1573] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(216), 12,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_elseif,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(218), 19,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_else,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1612] = 10,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(192), 3,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(190), 17,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1665] = 11,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(192), 3,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(190), 16,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1720] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(216), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(218), 20,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_catch,
      anon_sym_finally,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1759] = 9,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(192), 4,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(190), 17,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1810] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(228), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(226), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1849] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(232), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(230), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1888] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(234), 12,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_elseif,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(236), 19,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_else,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [1927] = 14,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
    ACTIONS(190), 11,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_PIPE_PIPE,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [1988] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(248), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(246), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2027] = 13,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
    ACTIONS(190), 12,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2086] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(250), 12,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_elseif,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(252), 19,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_else,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2125] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(192), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(190), 16,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2182] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(256), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(254), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2221] = 7,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(192), 6,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(190), 19,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2268] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(260), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(258), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2307] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(264), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(262), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2346] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(268), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(266), 23,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2387] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(272), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(270), 21,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2430] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(276), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(274), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2469] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(280), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(278), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2508] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(192), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(190), 21,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_COLON,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2551] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(282), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(284), 20,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_catch,
      anon_sym_finally,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2590] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(134), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(128), 24,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_instanceof,
      anon_sym_COMMA,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_COLON,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DOT,
  [2629] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(286), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(288), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2666] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(290), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(292), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2703] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(294), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(296), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2740] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(298), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(300), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2777] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(302), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(304), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2814] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(306), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(308), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2851] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(310), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(312), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2888] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(314), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(316), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2925] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(318), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(320), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2962] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(322), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(324), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [2999] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(326), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(328), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3036] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(330), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(332), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3073] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(334), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(336), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3110] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(338), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(340), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3147] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(342), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(344), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3184] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(346), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(348), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3221] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(350), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(352), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3258] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(354), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(356), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3295] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(358), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(360), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3332] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(362), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(364), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3369] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(366), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(368), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3406] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(136), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(138), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3443] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(370), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(372), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3480] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(216), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(218), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3517] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(374), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(376), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3554] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(204), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(206), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3591] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(378), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(380), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3628] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(382), 11,
      ts_builtin_sym_end,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
    ACTIONS(384), 18,
      anon_sym_using,
      anon_sym_import,
      anon_sym_return,
      anon_sym_if,
      anon_sym_for,
      anon_sym_while,
      anon_sym_try,
      anon_sym_class,
      anon_sym_function,
      anon_sym_var,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
      sym_identifier,
  [3665] = 13,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(386), 1,
      anon_sym_SEMI,
    ACTIONS(390), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(388), 2,
      sym_null_expression,
      sym_identifier,
    STATE(119), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [3720] = 13,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(394), 1,
      sym_number_expression,
    STATE(173), 1,
      sym_argument,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(392), 2,
      sym_null_expression,
      sym_identifier,
    STATE(113), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [3775] = 13,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(396), 1,
      anon_sym_RPAREN,
    ACTIONS(400), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(398), 2,
      sym_null_expression,
      sym_identifier,
    STATE(123), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [3830] = 13,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(402), 1,
      anon_sym_SEMI,
    ACTIONS(406), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(404), 2,
      sym_null_expression,
      sym_identifier,
    STATE(118), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [3885] = 13,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(394), 1,
      sym_number_expression,
    STATE(158), 1,
      sym_argument,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(392), 2,
      sym_null_expression,
      sym_identifier,
    STATE(113), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [3940] = 13,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(408), 1,
      anon_sym_RPAREN,
    ACTIONS(412), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(410), 2,
      sym_null_expression,
      sym_identifier,
    STATE(127), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [3995] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(416), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(414), 2,
      sym_null_expression,
      sym_identifier,
    STATE(38), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4047] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(420), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(418), 2,
      sym_null_expression,
      sym_identifier,
    STATE(40), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4099] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(424), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(422), 2,
      sym_null_expression,
      sym_identifier,
    STATE(124), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4151] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(428), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(426), 2,
      sym_null_expression,
      sym_identifier,
    STATE(130), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4203] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(432), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(430), 2,
      sym_null_expression,
      sym_identifier,
    STATE(120), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4255] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(436), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(434), 2,
      sym_null_expression,
      sym_identifier,
    STATE(128), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4307] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(440), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(438), 2,
      sym_null_expression,
      sym_identifier,
    STATE(125), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4359] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(444), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(442), 2,
      sym_null_expression,
      sym_identifier,
    STATE(111), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4411] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(448), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(446), 2,
      sym_null_expression,
      sym_identifier,
    STATE(36), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4463] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(452), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(450), 2,
      sym_null_expression,
      sym_identifier,
    STATE(129), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4515] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(456), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(454), 2,
      sym_null_expression,
      sym_identifier,
    STATE(34), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4567] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(460), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(458), 2,
      sym_null_expression,
      sym_identifier,
    STATE(30), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4619] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(464), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(462), 2,
      sym_null_expression,
      sym_identifier,
    STATE(28), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4671] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(468), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(466), 2,
      sym_null_expression,
      sym_identifier,
    STATE(115), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4723] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(472), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(470), 2,
      sym_null_expression,
      sym_identifier,
    STATE(117), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4775] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(476), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(474), 2,
      sym_null_expression,
      sym_identifier,
    STATE(43), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4827] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(480), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(478), 2,
      sym_null_expression,
      sym_identifier,
    STATE(112), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4879] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(484), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(482), 2,
      sym_null_expression,
      sym_identifier,
    STATE(27), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4931] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(488), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(486), 2,
      sym_null_expression,
      sym_identifier,
    STATE(44), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [4983] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(492), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(490), 2,
      sym_null_expression,
      sym_identifier,
    STATE(110), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [5035] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(496), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(494), 2,
      sym_null_expression,
      sym_identifier,
    STATE(116), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [5087] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(500), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(498), 2,
      sym_null_expression,
      sym_identifier,
    STATE(21), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [5139] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(504), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(502), 2,
      sym_null_expression,
      sym_identifier,
    STATE(126), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [5191] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(508), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(506), 2,
      sym_null_expression,
      sym_identifier,
    STATE(121), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [5243] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(512), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(510), 2,
      sym_null_expression,
      sym_identifier,
    STATE(131), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [5295] = 12,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(13), 1,
      anon_sym_LPAREN,
    ACTIONS(27), 1,
      anon_sym_LBRACE,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(41), 1,
      anon_sym_LBRACK,
    ACTIONS(43), 1,
      anon_sym_new,
    ACTIONS(516), 1,
      sym_number_expression,
    ACTIONS(35), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(37), 2,
      anon_sym_BANG,
      anon_sym_TILDE,
    ACTIONS(39), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(514), 2,
      sym_null_expression,
      sym_identifier,
    STATE(47), 12,
      sym__expression,
      sym_string_expression,
      sym_binary_expression,
      sym_unary_expression,
      sym_update_expression,
      sym_option_dictionary_expression,
      sym_dictionary_expression,
      sym_array_expression,
      sym_attribute_expression,
      sym_index_expression,
      sym_class_creation_expression,
      sym_parenthesized_expression,
  [5347] = 18,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(518), 1,
      anon_sym_COMMA,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(522), 1,
      anon_sym_RBRACK,
    ACTIONS(524), 1,
      anon_sym_DOT,
    STATE(156), 1,
      aux_sym_array_expression_repeat1,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5410] = 17,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(526), 1,
      anon_sym_COMMA,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(528), 2,
      anon_sym_RBRACE,
      anon_sym_COLON,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5471] = 17,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(530), 1,
      anon_sym_COMMA,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(532), 2,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5532] = 17,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(534), 1,
      anon_sym_as,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(536), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5593] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(540), 1,
      anon_sym_EQ,
    ACTIONS(542), 7,
      anon_sym_AMP,
      anon_sym_PIPE,
      anon_sym_LT,
      anon_sym_GT,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_SLASH,
    ACTIONS(538), 16,
      anon_sym_SEMI,
      anon_sym_AMP_AMP,
      anon_sym_PIPE_PIPE,
      anon_sym_CARET,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
      anon_sym_STAR,
      anon_sym_PERCENT,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
      anon_sym_DOT,
  [5627] = 17,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(544), 1,
      anon_sym_RPAREN,
    ACTIONS(546), 1,
      anon_sym_instanceof,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5687] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(548), 2,
      anon_sym_COMMA,
      anon_sym_RBRACK,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5745] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(550), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5802] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(552), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5859] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(554), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5916] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(556), 1,
      anon_sym_RPAREN,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [5973] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(558), 1,
      anon_sym_RPAREN,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6030] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(560), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6087] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(562), 1,
      anon_sym_RPAREN,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6144] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(564), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6201] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(566), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6258] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(568), 1,
      anon_sym_RPAREN,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6315] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(570), 1,
      anon_sym_RPAREN,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6372] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(572), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6429] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(574), 1,
      anon_sym_RPAREN,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6486] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(576), 1,
      anon_sym_SEMI,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6543] = 16,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(198), 1,
      anon_sym_SLASH,
    ACTIONS(202), 1,
      anon_sym_LBRACK,
    ACTIONS(220), 1,
      anon_sym_AMP,
    ACTIONS(224), 1,
      anon_sym_CARET,
    ACTIONS(238), 1,
      anon_sym_AMP_AMP,
    ACTIONS(240), 1,
      anon_sym_PIPE,
    ACTIONS(520), 1,
      anon_sym_PIPE_PIPE,
    ACTIONS(524), 1,
      anon_sym_DOT,
    ACTIONS(578), 1,
      anon_sym_RBRACK,
    ACTIONS(194), 2,
      anon_sym_PLUS,
      anon_sym_DASH,
    ACTIONS(196), 2,
      anon_sym_STAR,
      anon_sym_PERCENT,
    ACTIONS(200), 2,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
    ACTIONS(222), 2,
      anon_sym_LT_LT,
      anon_sym_GT_GT,
    ACTIONS(244), 2,
      anon_sym_LT,
      anon_sym_GT,
    ACTIONS(242), 4,
      anon_sym_EQ_EQ,
      anon_sym_BANG_EQ,
      anon_sym_LT_EQ,
      anon_sym_GT_EQ,
  [6600] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(384), 5,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      sym_identifier,
    ACTIONS(382), 10,
      anon_sym_SEMI,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
  [6623] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(292), 5,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      sym_identifier,
    ACTIONS(290), 10,
      anon_sym_SEMI,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
  [6646] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(332), 5,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      sym_identifier,
    ACTIONS(330), 10,
      anon_sym_SEMI,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
  [6669] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(328), 5,
      sym_null_expression,
      anon_sym_PLUS,
      anon_sym_DASH,
      anon_sym_new,
      sym_identifier,
    ACTIONS(326), 10,
      anon_sym_SEMI,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_DQUOTE,
      sym_number_expression,
      anon_sym_BANG,
      anon_sym_TILDE,
      anon_sym_DASH_DASH,
      anon_sym_PLUS_PLUS,
      anon_sym_LBRACK,
  [6692] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(580), 1,
      anon_sym_var,
    STATE(78), 1,
      sym_variable_declaration,
    STATE(209), 1,
      sym__modifier,
    ACTIONS(582), 3,
      anon_sym_public,
      anon_sym_private,
      anon_sym_protected,
  [6710] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(586), 1,
      anon_sym_DOT,
    STATE(137), 1,
      aux_sym_dotted_name_repeat1,
    ACTIONS(584), 4,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
  [6726] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(591), 1,
      anon_sym_DOT,
    STATE(139), 1,
      aux_sym_dotted_name_repeat1,
    ACTIONS(589), 4,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
  [6742] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(591), 1,
      anon_sym_DOT,
    STATE(137), 1,
      aux_sym_dotted_name_repeat1,
    ACTIONS(593), 4,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
  [6758] = 3,
    ACTIONS(3), 1,
      sym_comment,
    STATE(172), 1,
      sym__type,
    ACTIONS(595), 4,
      anon_sym_Boolean,
      anon_sym_String,
      anon_sym_Number,
      sym_identifier,
  [6771] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(584), 5,
      anon_sym_SEMI,
      anon_sym_as,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_DOT,
  [6782] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(597), 1,
      anon_sym_catch,
    ACTIONS(599), 1,
      anon_sym_finally,
    STATE(71), 1,
      sym_finally_clause,
    STATE(11), 2,
      sym_catch_clause,
      aux_sym_try_statement_repeat1,
  [6799] = 6,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(601), 1,
      anon_sym_COLON,
    STATE(146), 1,
      aux_sym_dictionary_expression_repeat1,
    STATE(160), 1,
      aux_sym_option_dictionary_expression_repeat1,
    STATE(199), 1,
      sym_string_expression,
  [6818] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(130), 1,
      anon_sym_LPAREN,
    ACTIONS(132), 1,
      anon_sym_LPAREN_RPAREN,
    STATE(151), 2,
      sym_empty_argument_list,
      sym_argument_list,
  [6832] = 4,
    ACTIONS(603), 1,
      anon_sym_DQUOTE,
    ACTIONS(607), 1,
      sym_comment,
    STATE(149), 1,
      aux_sym_string_expression_repeat1,
    ACTIONS(605), 2,
      aux_sym_string_expression_token1,
      sym_escape_sequence,
  [6846] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(31), 1,
      anon_sym_DQUOTE,
    ACTIONS(609), 1,
      anon_sym_RBRACE,
    STATE(150), 1,
      aux_sym_dictionary_expression_repeat1,
    STATE(199), 1,
      sym_string_expression,
  [6862] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(130), 1,
      anon_sym_LPAREN,
    ACTIONS(132), 1,
      anon_sym_LPAREN_RPAREN,
    STATE(154), 2,
      sym_empty_argument_list,
      sym_argument_list,
  [6876] = 4,
    ACTIONS(607), 1,
      sym_comment,
    ACTIONS(611), 1,
      anon_sym_DQUOTE,
    STATE(145), 1,
      aux_sym_string_expression_repeat1,
    ACTIONS(613), 2,
      aux_sym_string_expression_token1,
      sym_escape_sequence,
  [6890] = 4,
    ACTIONS(607), 1,
      sym_comment,
    ACTIONS(615), 1,
      anon_sym_DQUOTE,
    STATE(149), 1,
      aux_sym_string_expression_repeat1,
    ACTIONS(617), 2,
      aux_sym_string_expression_token1,
      sym_escape_sequence,
  [6904] = 5,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(620), 1,
      anon_sym_RBRACE,
    ACTIONS(622), 1,
      anon_sym_DQUOTE,
    STATE(150), 1,
      aux_sym_dictionary_expression_repeat1,
    STATE(199), 1,
      sym_string_expression,
  [6920] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(625), 1,
      anon_sym_as,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(58), 1,
      sym_block,
  [6933] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    ACTIONS(629), 1,
      anon_sym_extends,
    STATE(56), 1,
      sym_block,
  [6946] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(631), 1,
      anon_sym_RPAREN,
    ACTIONS(633), 1,
      anon_sym_COMMA,
    STATE(162), 1,
      aux_sym_argument_list_repeat1,
  [6959] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    ACTIONS(635), 1,
      anon_sym_as,
    STATE(69), 1,
      sym_block,
  [6972] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(637), 1,
      anon_sym_RBRACE,
    ACTIONS(639), 1,
      anon_sym_COLON,
    STATE(155), 1,
      aux_sym_option_dictionary_expression_repeat1,
  [6985] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(518), 1,
      anon_sym_COMMA,
    ACTIONS(642), 1,
      anon_sym_RBRACK,
    STATE(159), 1,
      aux_sym_array_expression_repeat1,
  [6998] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(644), 1,
      sym_identifier,
    STATE(174), 1,
      sym_dotted_name,
    STATE(200), 1,
      sym_aliased_import,
  [7011] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(633), 1,
      anon_sym_COMMA,
    ACTIONS(646), 1,
      anon_sym_RPAREN,
    STATE(153), 1,
      aux_sym_argument_list_repeat1,
  [7024] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(548), 1,
      anon_sym_RBRACK,
    ACTIONS(648), 1,
      anon_sym_COMMA,
    STATE(159), 1,
      aux_sym_array_expression_repeat1,
  [7037] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(601), 1,
      anon_sym_COLON,
    ACTIONS(651), 1,
      anon_sym_RBRACE,
    STATE(155), 1,
      aux_sym_option_dictionary_expression_repeat1,
  [7050] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(130), 1,
      anon_sym_LPAREN,
    ACTIONS(653), 1,
      anon_sym_LPAREN_RPAREN,
    STATE(39), 1,
      sym_argument_list,
  [7063] = 4,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(655), 1,
      anon_sym_RPAREN,
    ACTIONS(657), 1,
      anon_sym_COMMA,
    STATE(162), 1,
      aux_sym_argument_list_repeat1,
  [7076] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(52), 1,
      sym_block,
  [7086] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(660), 1,
      sym_identifier,
    STATE(49), 1,
      sym_call,
  [7096] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(63), 1,
      sym_block,
  [7106] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(662), 1,
      anon_sym_LBRACE,
    STATE(12), 1,
      sym_block,
  [7116] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(53), 1,
      sym_block,
  [7126] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(55), 1,
      sym_block,
  [7136] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(664), 1,
      anon_sym_SEMI,
    ACTIONS(666), 1,
      anon_sym_EQ,
  [7146] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(668), 1,
      anon_sym_LBRACE,
    STATE(23), 1,
      sym_block,
  [7156] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(644), 1,
      sym_identifier,
    STATE(192), 1,
      sym_dotted_name,
  [7166] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(670), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [7174] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(655), 2,
      anon_sym_RPAREN,
      anon_sym_COMMA,
  [7182] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(672), 1,
      anon_sym_SEMI,
    ACTIONS(674), 1,
      anon_sym_as,
  [7192] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(74), 1,
      sym_block,
  [7202] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(676), 1,
      anon_sym_SEMI,
    ACTIONS(678), 1,
      anon_sym_EQ,
  [7212] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(680), 2,
      anon_sym_RBRACE,
      anon_sym_COLON,
  [7220] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(65), 1,
      sym_block,
  [7230] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(668), 1,
      anon_sym_LBRACE,
    STATE(142), 1,
      sym_block,
  [7240] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(682), 1,
      anon_sym_SEMI,
    ACTIONS(684), 1,
      anon_sym_EQ,
  [7250] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(686), 1,
      anon_sym_function,
    ACTIONS(688), 1,
      anon_sym_var,
  [7260] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(50), 1,
      sym_block,
  [7270] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(644), 1,
      sym_identifier,
    STATE(189), 1,
      sym_dotted_name,
  [7280] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(690), 1,
      anon_sym_SEMI,
    ACTIONS(692), 1,
      anon_sym_EQ,
  [7290] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(662), 1,
      anon_sym_LBRACE,
    STATE(37), 1,
      sym_block,
  [7300] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(54), 1,
      sym_block,
  [7310] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(694), 2,
      anon_sym_RBRACE,
      anon_sym_DQUOTE,
  [7318] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(668), 1,
      anon_sym_LBRACE,
    STATE(48), 1,
      sym_block,
  [7328] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(64), 1,
      sym_block,
  [7338] = 3,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(627), 1,
      anon_sym_LBRACE,
    STATE(76), 1,
      sym_block,
  [7348] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(696), 1,
      anon_sym_LPAREN,
  [7355] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(698), 1,
      anon_sym_RPAREN,
  [7362] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(700), 1,
      sym_identifier,
  [7369] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(702), 1,
      sym_identifier,
  [7376] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(704), 1,
      sym_identifier,
  [7383] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(706), 1,
      sym_identifier,
  [7390] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(708), 1,
      anon_sym_EQ_GT,
  [7397] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(710), 1,
      sym_identifier,
  [7404] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(712), 1,
      anon_sym_EQ_GT,
  [7411] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(672), 1,
      anon_sym_SEMI,
  [7418] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(714), 1,
      sym_identifier,
  [7425] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(716), 1,
      sym_identifier,
  [7432] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(718), 1,
      sym_identifier,
  [7439] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(720), 1,
      anon_sym_LPAREN,
  [7446] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(722), 1,
      ts_builtin_sym_end,
  [7453] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(724), 1,
      anon_sym_LPAREN,
  [7460] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(726), 1,
      anon_sym_SEMI,
  [7467] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(728), 1,
      anon_sym_LPAREN,
  [7474] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(730), 1,
      anon_sym_var,
  [7481] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(732), 1,
      sym_identifier,
  [7488] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(734), 1,
      anon_sym_LPAREN,
  [7495] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(736), 1,
      sym_identifier,
  [7502] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(738), 1,
      sym_identifier,
  [7509] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(740), 1,
      sym_identifier,
  [7516] = 2,
    ACTIONS(3), 1,
      sym_comment,
    ACTIONS(742), 1,
      sym_identifier,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 110,
  [SMALL_STATE(4)] = 219,
  [SMALL_STATE(5)] = 328,
  [SMALL_STATE(6)] = 437,
  [SMALL_STATE(7)] = 546,
  [SMALL_STATE(8)] = 655,
  [SMALL_STATE(9)] = 764,
  [SMALL_STATE(10)] = 873,
  [SMALL_STATE(11)] = 922,
  [SMALL_STATE(12)] = 972,
  [SMALL_STATE(13)] = 1024,
  [SMALL_STATE(14)] = 1076,
  [SMALL_STATE(15)] = 1121,
  [SMALL_STATE(16)] = 1168,
  [SMALL_STATE(17)] = 1208,
  [SMALL_STATE(18)] = 1248,
  [SMALL_STATE(19)] = 1288,
  [SMALL_STATE(20)] = 1328,
  [SMALL_STATE(21)] = 1368,
  [SMALL_STATE(22)] = 1417,
  [SMALL_STATE(23)] = 1456,
  [SMALL_STATE(24)] = 1495,
  [SMALL_STATE(25)] = 1534,
  [SMALL_STATE(26)] = 1573,
  [SMALL_STATE(27)] = 1612,
  [SMALL_STATE(28)] = 1665,
  [SMALL_STATE(29)] = 1720,
  [SMALL_STATE(30)] = 1759,
  [SMALL_STATE(31)] = 1810,
  [SMALL_STATE(32)] = 1849,
  [SMALL_STATE(33)] = 1888,
  [SMALL_STATE(34)] = 1927,
  [SMALL_STATE(35)] = 1988,
  [SMALL_STATE(36)] = 2027,
  [SMALL_STATE(37)] = 2086,
  [SMALL_STATE(38)] = 2125,
  [SMALL_STATE(39)] = 2182,
  [SMALL_STATE(40)] = 2221,
  [SMALL_STATE(41)] = 2268,
  [SMALL_STATE(42)] = 2307,
  [SMALL_STATE(43)] = 2346,
  [SMALL_STATE(44)] = 2387,
  [SMALL_STATE(45)] = 2430,
  [SMALL_STATE(46)] = 2469,
  [SMALL_STATE(47)] = 2508,
  [SMALL_STATE(48)] = 2551,
  [SMALL_STATE(49)] = 2590,
  [SMALL_STATE(50)] = 2629,
  [SMALL_STATE(51)] = 2666,
  [SMALL_STATE(52)] = 2703,
  [SMALL_STATE(53)] = 2740,
  [SMALL_STATE(54)] = 2777,
  [SMALL_STATE(55)] = 2814,
  [SMALL_STATE(56)] = 2851,
  [SMALL_STATE(57)] = 2888,
  [SMALL_STATE(58)] = 2925,
  [SMALL_STATE(59)] = 2962,
  [SMALL_STATE(60)] = 2999,
  [SMALL_STATE(61)] = 3036,
  [SMALL_STATE(62)] = 3073,
  [SMALL_STATE(63)] = 3110,
  [SMALL_STATE(64)] = 3147,
  [SMALL_STATE(65)] = 3184,
  [SMALL_STATE(66)] = 3221,
  [SMALL_STATE(67)] = 3258,
  [SMALL_STATE(68)] = 3295,
  [SMALL_STATE(69)] = 3332,
  [SMALL_STATE(70)] = 3369,
  [SMALL_STATE(71)] = 3406,
  [SMALL_STATE(72)] = 3443,
  [SMALL_STATE(73)] = 3480,
  [SMALL_STATE(74)] = 3517,
  [SMALL_STATE(75)] = 3554,
  [SMALL_STATE(76)] = 3591,
  [SMALL_STATE(77)] = 3628,
  [SMALL_STATE(78)] = 3665,
  [SMALL_STATE(79)] = 3720,
  [SMALL_STATE(80)] = 3775,
  [SMALL_STATE(81)] = 3830,
  [SMALL_STATE(82)] = 3885,
  [SMALL_STATE(83)] = 3940,
  [SMALL_STATE(84)] = 3995,
  [SMALL_STATE(85)] = 4047,
  [SMALL_STATE(86)] = 4099,
  [SMALL_STATE(87)] = 4151,
  [SMALL_STATE(88)] = 4203,
  [SMALL_STATE(89)] = 4255,
  [SMALL_STATE(90)] = 4307,
  [SMALL_STATE(91)] = 4359,
  [SMALL_STATE(92)] = 4411,
  [SMALL_STATE(93)] = 4463,
  [SMALL_STATE(94)] = 4515,
  [SMALL_STATE(95)] = 4567,
  [SMALL_STATE(96)] = 4619,
  [SMALL_STATE(97)] = 4671,
  [SMALL_STATE(98)] = 4723,
  [SMALL_STATE(99)] = 4775,
  [SMALL_STATE(100)] = 4827,
  [SMALL_STATE(101)] = 4879,
  [SMALL_STATE(102)] = 4931,
  [SMALL_STATE(103)] = 4983,
  [SMALL_STATE(104)] = 5035,
  [SMALL_STATE(105)] = 5087,
  [SMALL_STATE(106)] = 5139,
  [SMALL_STATE(107)] = 5191,
  [SMALL_STATE(108)] = 5243,
  [SMALL_STATE(109)] = 5295,
  [SMALL_STATE(110)] = 5347,
  [SMALL_STATE(111)] = 5410,
  [SMALL_STATE(112)] = 5471,
  [SMALL_STATE(113)] = 5532,
  [SMALL_STATE(114)] = 5593,
  [SMALL_STATE(115)] = 5627,
  [SMALL_STATE(116)] = 5687,
  [SMALL_STATE(117)] = 5745,
  [SMALL_STATE(118)] = 5802,
  [SMALL_STATE(119)] = 5859,
  [SMALL_STATE(120)] = 5916,
  [SMALL_STATE(121)] = 5973,
  [SMALL_STATE(122)] = 6030,
  [SMALL_STATE(123)] = 6087,
  [SMALL_STATE(124)] = 6144,
  [SMALL_STATE(125)] = 6201,
  [SMALL_STATE(126)] = 6258,
  [SMALL_STATE(127)] = 6315,
  [SMALL_STATE(128)] = 6372,
  [SMALL_STATE(129)] = 6429,
  [SMALL_STATE(130)] = 6486,
  [SMALL_STATE(131)] = 6543,
  [SMALL_STATE(132)] = 6600,
  [SMALL_STATE(133)] = 6623,
  [SMALL_STATE(134)] = 6646,
  [SMALL_STATE(135)] = 6669,
  [SMALL_STATE(136)] = 6692,
  [SMALL_STATE(137)] = 6710,
  [SMALL_STATE(138)] = 6726,
  [SMALL_STATE(139)] = 6742,
  [SMALL_STATE(140)] = 6758,
  [SMALL_STATE(141)] = 6771,
  [SMALL_STATE(142)] = 6782,
  [SMALL_STATE(143)] = 6799,
  [SMALL_STATE(144)] = 6818,
  [SMALL_STATE(145)] = 6832,
  [SMALL_STATE(146)] = 6846,
  [SMALL_STATE(147)] = 6862,
  [SMALL_STATE(148)] = 6876,
  [SMALL_STATE(149)] = 6890,
  [SMALL_STATE(150)] = 6904,
  [SMALL_STATE(151)] = 6920,
  [SMALL_STATE(152)] = 6933,
  [SMALL_STATE(153)] = 6946,
  [SMALL_STATE(154)] = 6959,
  [SMALL_STATE(155)] = 6972,
  [SMALL_STATE(156)] = 6985,
  [SMALL_STATE(157)] = 6998,
  [SMALL_STATE(158)] = 7011,
  [SMALL_STATE(159)] = 7024,
  [SMALL_STATE(160)] = 7037,
  [SMALL_STATE(161)] = 7050,
  [SMALL_STATE(162)] = 7063,
  [SMALL_STATE(163)] = 7076,
  [SMALL_STATE(164)] = 7086,
  [SMALL_STATE(165)] = 7096,
  [SMALL_STATE(166)] = 7106,
  [SMALL_STATE(167)] = 7116,
  [SMALL_STATE(168)] = 7126,
  [SMALL_STATE(169)] = 7136,
  [SMALL_STATE(170)] = 7146,
  [SMALL_STATE(171)] = 7156,
  [SMALL_STATE(172)] = 7166,
  [SMALL_STATE(173)] = 7174,
  [SMALL_STATE(174)] = 7182,
  [SMALL_STATE(175)] = 7192,
  [SMALL_STATE(176)] = 7202,
  [SMALL_STATE(177)] = 7212,
  [SMALL_STATE(178)] = 7220,
  [SMALL_STATE(179)] = 7230,
  [SMALL_STATE(180)] = 7240,
  [SMALL_STATE(181)] = 7250,
  [SMALL_STATE(182)] = 7260,
  [SMALL_STATE(183)] = 7270,
  [SMALL_STATE(184)] = 7280,
  [SMALL_STATE(185)] = 7290,
  [SMALL_STATE(186)] = 7300,
  [SMALL_STATE(187)] = 7310,
  [SMALL_STATE(188)] = 7318,
  [SMALL_STATE(189)] = 7328,
  [SMALL_STATE(190)] = 7338,
  [SMALL_STATE(191)] = 7348,
  [SMALL_STATE(192)] = 7355,
  [SMALL_STATE(193)] = 7362,
  [SMALL_STATE(194)] = 7369,
  [SMALL_STATE(195)] = 7376,
  [SMALL_STATE(196)] = 7383,
  [SMALL_STATE(197)] = 7390,
  [SMALL_STATE(198)] = 7397,
  [SMALL_STATE(199)] = 7404,
  [SMALL_STATE(200)] = 7411,
  [SMALL_STATE(201)] = 7418,
  [SMALL_STATE(202)] = 7425,
  [SMALL_STATE(203)] = 7432,
  [SMALL_STATE(204)] = 7439,
  [SMALL_STATE(205)] = 7446,
  [SMALL_STATE(206)] = 7453,
  [SMALL_STATE(207)] = 7460,
  [SMALL_STATE(208)] = 7467,
  [SMALL_STATE(209)] = 7474,
  [SMALL_STATE(210)] = 7481,
  [SMALL_STATE(211)] = 7488,
  [SMALL_STATE(212)] = 7495,
  [SMALL_STATE(213)] = 7502,
  [SMALL_STATE(214)] = 7509,
  [SMALL_STATE(215)] = 7516,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, SHIFT_EXTRA(),
  [5] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 0),
  [7] = {.entry = {.count = 1, .reusable = false}}, SHIFT(157),
  [9] = {.entry = {.count = 1, .reusable = false}}, SHIFT(81),
  [11] = {.entry = {.count = 1, .reusable = false}}, SHIFT(211),
  [13] = {.entry = {.count = 1, .reusable = true}}, SHIFT(88),
  [15] = {.entry = {.count = 1, .reusable = false}}, SHIFT(206),
  [17] = {.entry = {.count = 1, .reusable = false}}, SHIFT(204),
  [19] = {.entry = {.count = 1, .reusable = false}}, SHIFT(179),
  [21] = {.entry = {.count = 1, .reusable = false}}, SHIFT(202),
  [23] = {.entry = {.count = 1, .reusable = false}}, SHIFT(198),
  [25] = {.entry = {.count = 1, .reusable = false}}, SHIFT(196),
  [27] = {.entry = {.count = 1, .reusable = true}}, SHIFT(143),
  [29] = {.entry = {.count = 1, .reusable = false}}, SHIFT(122),
  [31] = {.entry = {.count = 1, .reusable = true}}, SHIFT(148),
  [33] = {.entry = {.count = 1, .reusable = true}}, SHIFT(122),
  [35] = {.entry = {.count = 1, .reusable = false}}, SHIFT(99),
  [37] = {.entry = {.count = 1, .reusable = true}}, SHIFT(99),
  [39] = {.entry = {.count = 1, .reusable = true}}, SHIFT(102),
  [41] = {.entry = {.count = 1, .reusable = true}}, SHIFT(103),
  [43] = {.entry = {.count = 1, .reusable = false}}, SHIFT(193),
  [45] = {.entry = {.count = 1, .reusable = false}}, SHIFT(181),
  [47] = {.entry = {.count = 1, .reusable = false}}, SHIFT(114),
  [49] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [51] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(157),
  [54] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(81),
  [57] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(211),
  [60] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(88),
  [63] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(206),
  [66] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(204),
  [69] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(179),
  [72] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(202),
  [75] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(198),
  [78] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(196),
  [81] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(143),
  [84] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(122),
  [87] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(148),
  [90] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(122),
  [93] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(99),
  [96] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(99),
  [99] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(102),
  [102] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(103),
  [105] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(193),
  [108] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(181),
  [111] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(114),
  [114] = {.entry = {.count = 1, .reusable = true}}, SHIFT(75),
  [116] = {.entry = {.count = 1, .reusable = true}}, SHIFT(26),
  [118] = {.entry = {.count = 1, .reusable = true}}, SHIFT(29),
  [120] = {.entry = {.count = 1, .reusable = true}}, SHIFT(73),
  [122] = {.entry = {.count = 1, .reusable = true}}, SHIFT(22),
  [124] = {.entry = {.count = 1, .reusable = true}}, SHIFT(24),
  [126] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1),
  [128] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_attribute_expression, 3, .production_id = 5),
  [130] = {.entry = {.count = 1, .reusable = false}}, SHIFT(82),
  [132] = {.entry = {.count = 1, .reusable = true}}, SHIFT(17),
  [134] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_attribute_expression, 3, .production_id = 5),
  [136] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_try_statement, 3, .production_id = 3),
  [138] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_try_statement, 3, .production_id = 3),
  [140] = {.entry = {.count = 1, .reusable = false}}, SHIFT(208),
  [142] = {.entry = {.count = 1, .reusable = false}}, SHIFT(182),
  [144] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_if_statement, 5, .production_id = 8),
  [146] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_if_statement, 5, .production_id = 8),
  [148] = {.entry = {.count = 1, .reusable = true}}, SHIFT(191),
  [150] = {.entry = {.count = 1, .reusable = false}}, SHIFT(178),
  [152] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_if_statement, 6, .production_id = 13),
  [154] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_if_statement, 6, .production_id = 13),
  [156] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_try_statement_repeat1, 2),
  [158] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_try_statement_repeat1, 2),
  [160] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_try_statement_repeat1, 2), SHIFT_REPEAT(208),
  [163] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_if_statement_repeat1, 2, .production_id = 19),
  [165] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_if_statement_repeat1, 2, .production_id = 19),
  [167] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_if_statement_repeat1, 2, .production_id = 19), SHIFT_REPEAT(191),
  [170] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_string_expression, 3),
  [172] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_string_expression, 3),
  [174] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_empty_argument_list, 1),
  [176] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_empty_argument_list, 1),
  [178] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_argument_list, 4),
  [180] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_argument_list, 4),
  [182] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_argument_list, 3),
  [184] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_argument_list, 3),
  [186] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_string_expression, 2),
  [188] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_string_expression, 2),
  [190] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_binary_expression, 3, .production_id = 4),
  [192] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_binary_expression, 3, .production_id = 4),
  [194] = {.entry = {.count = 1, .reusable = false}}, SHIFT(85),
  [196] = {.entry = {.count = 1, .reusable = true}}, SHIFT(109),
  [198] = {.entry = {.count = 1, .reusable = false}}, SHIFT(109),
  [200] = {.entry = {.count = 1, .reusable = true}}, SHIFT(45),
  [202] = {.entry = {.count = 1, .reusable = true}}, SHIFT(108),
  [204] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 3),
  [206] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 3),
  [208] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_catch_clause, 5, .production_id = 22),
  [210] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_catch_clause, 5, .production_id = 22),
  [212] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_array_expression, 3),
  [214] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_array_expression, 3),
  [216] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_block, 2),
  [218] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_block, 2),
  [220] = {.entry = {.count = 1, .reusable = false}}, SHIFT(95),
  [222] = {.entry = {.count = 1, .reusable = true}}, SHIFT(105),
  [224] = {.entry = {.count = 1, .reusable = true}}, SHIFT(101),
  [226] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_parenthesized_expression, 3),
  [228] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_parenthesized_expression, 3),
  [230] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_dictionary_expression, 3),
  [232] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_dictionary_expression, 3),
  [234] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_if_statement_repeat1, 1, .production_id = 11),
  [236] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_if_statement_repeat1, 1, .production_id = 11),
  [238] = {.entry = {.count = 1, .reusable = true}}, SHIFT(92),
  [240] = {.entry = {.count = 1, .reusable = false}}, SHIFT(96),
  [242] = {.entry = {.count = 1, .reusable = true}}, SHIFT(84),
  [244] = {.entry = {.count = 1, .reusable = false}}, SHIFT(84),
  [246] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_option_dictionary_expression, 3),
  [248] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_option_dictionary_expression, 3),
  [250] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_elif_clause, 5, .production_id = 8),
  [252] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_elif_clause, 5, .production_id = 8),
  [254] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_class_creation_expression, 3),
  [256] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_class_creation_expression, 3),
  [258] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_call, 2, .production_id = 7),
  [260] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_call, 2, .production_id = 7),
  [262] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_index_expression, 4),
  [264] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_index_expression, 4),
  [266] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_unary_expression, 2, .production_id = 1),
  [268] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_unary_expression, 2, .production_id = 1),
  [270] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_update_expression, 2, .production_id = 1),
  [272] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_update_expression, 2, .production_id = 1),
  [274] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_update_expression, 2, .production_id = 2),
  [276] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_update_expression, 2, .production_id = 2),
  [278] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_array_expression, 4),
  [280] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_array_expression, 4),
  [282] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_catch_clause, 7, .production_id = 25),
  [284] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_catch_clause, 7, .production_id = 25),
  [286] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_finally_clause, 2, .production_id = 3),
  [288] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_finally_clause, 2, .production_id = 3),
  [290] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_variable_declaration, 5, .production_id = 9),
  [292] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_variable_declaration, 5, .production_id = 9),
  [294] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_function_declaration, 7, .production_id = 23),
  [296] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_function_declaration, 7, .production_id = 23),
  [298] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_for_statement, 7, .production_id = 20),
  [300] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_for_statement, 7, .production_id = 20),
  [302] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_for_statement, 8, .production_id = 24),
  [304] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_for_statement, 8, .production_id = 24),
  [306] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_for_statement, 7, .production_id = 21),
  [308] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_for_statement, 7, .production_id = 21),
  [310] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_class_declaration, 3),
  [312] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_class_declaration, 3),
  [314] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__expression_statement, 2),
  [316] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__expression_statement, 2),
  [318] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_function_declaration, 4),
  [320] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_function_declaration, 4),
  [322] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_variable_assingment, 4),
  [324] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_variable_assingment, 4),
  [326] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_variable_declaration, 3),
  [328] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_variable_declaration, 3),
  [330] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_variable_declaration, 4),
  [332] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_variable_declaration, 4),
  [334] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_if_statement, 7, .production_id = 18),
  [336] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_if_statement, 7, .production_id = 18),
  [338] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_while_statement, 5, .production_id = 8),
  [340] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_while_statement, 5, .production_id = 8),
  [342] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_class_declaration, 5),
  [344] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_class_declaration, 5),
  [346] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_else_clause, 2, .production_id = 3),
  [348] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_else_clause, 2, .production_id = 3),
  [350] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_import_statement, 3),
  [352] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_import_statement, 3),
  [354] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_try_statement, 4, .production_id = 3),
  [356] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_try_statement, 4, .production_id = 3),
  [358] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_return_statement, 3),
  [360] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_return_statement, 3),
  [362] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_function_declaration, 5, .production_id = 10),
  [364] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_function_declaration, 5, .production_id = 10),
  [366] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_if_statement, 6, .production_id = 12),
  [368] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_if_statement, 6, .production_id = 12),
  [370] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_return_statement, 2),
  [372] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_return_statement, 2),
  [374] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_for_statement, 6, .production_id = 14),
  [376] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_for_statement, 6, .production_id = 14),
  [378] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_function_declaration, 6, .production_id = 16),
  [380] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_function_declaration, 6, .production_id = 16),
  [382] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_variable_declaration, 6, .production_id = 17),
  [384] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_variable_declaration, 6, .production_id = 17),
  [386] = {.entry = {.count = 1, .reusable = true}}, SHIFT(83),
  [388] = {.entry = {.count = 1, .reusable = false}}, SHIFT(119),
  [390] = {.entry = {.count = 1, .reusable = true}}, SHIFT(119),
  [392] = {.entry = {.count = 1, .reusable = false}}, SHIFT(113),
  [394] = {.entry = {.count = 1, .reusable = true}}, SHIFT(113),
  [396] = {.entry = {.count = 1, .reusable = true}}, SHIFT(168),
  [398] = {.entry = {.count = 1, .reusable = false}}, SHIFT(123),
  [400] = {.entry = {.count = 1, .reusable = true}}, SHIFT(123),
  [402] = {.entry = {.count = 1, .reusable = true}}, SHIFT(72),
  [404] = {.entry = {.count = 1, .reusable = false}}, SHIFT(118),
  [406] = {.entry = {.count = 1, .reusable = true}}, SHIFT(118),
  [408] = {.entry = {.count = 1, .reusable = true}}, SHIFT(175),
  [410] = {.entry = {.count = 1, .reusable = false}}, SHIFT(127),
  [412] = {.entry = {.count = 1, .reusable = true}}, SHIFT(127),
  [414] = {.entry = {.count = 1, .reusable = false}}, SHIFT(38),
  [416] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [418] = {.entry = {.count = 1, .reusable = false}}, SHIFT(40),
  [420] = {.entry = {.count = 1, .reusable = true}}, SHIFT(40),
  [422] = {.entry = {.count = 1, .reusable = false}}, SHIFT(124),
  [424] = {.entry = {.count = 1, .reusable = true}}, SHIFT(124),
  [426] = {.entry = {.count = 1, .reusable = false}}, SHIFT(130),
  [428] = {.entry = {.count = 1, .reusable = true}}, SHIFT(130),
  [430] = {.entry = {.count = 1, .reusable = false}}, SHIFT(120),
  [432] = {.entry = {.count = 1, .reusable = true}}, SHIFT(120),
  [434] = {.entry = {.count = 1, .reusable = false}}, SHIFT(128),
  [436] = {.entry = {.count = 1, .reusable = true}}, SHIFT(128),
  [438] = {.entry = {.count = 1, .reusable = false}}, SHIFT(125),
  [440] = {.entry = {.count = 1, .reusable = true}}, SHIFT(125),
  [442] = {.entry = {.count = 1, .reusable = false}}, SHIFT(111),
  [444] = {.entry = {.count = 1, .reusable = true}}, SHIFT(111),
  [446] = {.entry = {.count = 1, .reusable = false}}, SHIFT(36),
  [448] = {.entry = {.count = 1, .reusable = true}}, SHIFT(36),
  [450] = {.entry = {.count = 1, .reusable = false}}, SHIFT(129),
  [452] = {.entry = {.count = 1, .reusable = true}}, SHIFT(129),
  [454] = {.entry = {.count = 1, .reusable = false}}, SHIFT(34),
  [456] = {.entry = {.count = 1, .reusable = true}}, SHIFT(34),
  [458] = {.entry = {.count = 1, .reusable = false}}, SHIFT(30),
  [460] = {.entry = {.count = 1, .reusable = true}}, SHIFT(30),
  [462] = {.entry = {.count = 1, .reusable = false}}, SHIFT(28),
  [464] = {.entry = {.count = 1, .reusable = true}}, SHIFT(28),
  [466] = {.entry = {.count = 1, .reusable = false}}, SHIFT(115),
  [468] = {.entry = {.count = 1, .reusable = true}}, SHIFT(115),
  [470] = {.entry = {.count = 1, .reusable = false}}, SHIFT(117),
  [472] = {.entry = {.count = 1, .reusable = true}}, SHIFT(117),
  [474] = {.entry = {.count = 1, .reusable = false}}, SHIFT(43),
  [476] = {.entry = {.count = 1, .reusable = true}}, SHIFT(43),
  [478] = {.entry = {.count = 1, .reusable = false}}, SHIFT(112),
  [480] = {.entry = {.count = 1, .reusable = true}}, SHIFT(112),
  [482] = {.entry = {.count = 1, .reusable = false}}, SHIFT(27),
  [484] = {.entry = {.count = 1, .reusable = true}}, SHIFT(27),
  [486] = {.entry = {.count = 1, .reusable = false}}, SHIFT(44),
  [488] = {.entry = {.count = 1, .reusable = true}}, SHIFT(44),
  [490] = {.entry = {.count = 1, .reusable = false}}, SHIFT(110),
  [492] = {.entry = {.count = 1, .reusable = true}}, SHIFT(110),
  [494] = {.entry = {.count = 1, .reusable = false}}, SHIFT(116),
  [496] = {.entry = {.count = 1, .reusable = true}}, SHIFT(116),
  [498] = {.entry = {.count = 1, .reusable = false}}, SHIFT(21),
  [500] = {.entry = {.count = 1, .reusable = true}}, SHIFT(21),
  [502] = {.entry = {.count = 1, .reusable = false}}, SHIFT(126),
  [504] = {.entry = {.count = 1, .reusable = true}}, SHIFT(126),
  [506] = {.entry = {.count = 1, .reusable = false}}, SHIFT(121),
  [508] = {.entry = {.count = 1, .reusable = true}}, SHIFT(121),
  [510] = {.entry = {.count = 1, .reusable = false}}, SHIFT(131),
  [512] = {.entry = {.count = 1, .reusable = true}}, SHIFT(131),
  [514] = {.entry = {.count = 1, .reusable = false}}, SHIFT(47),
  [516] = {.entry = {.count = 1, .reusable = true}}, SHIFT(47),
  [518] = {.entry = {.count = 1, .reusable = true}}, SHIFT(104),
  [520] = {.entry = {.count = 1, .reusable = true}}, SHIFT(94),
  [522] = {.entry = {.count = 1, .reusable = true}}, SHIFT(25),
  [524] = {.entry = {.count = 1, .reusable = true}}, SHIFT(164),
  [526] = {.entry = {.count = 1, .reusable = true}}, SHIFT(177),
  [528] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_option_dictionary_expression_repeat1, 4),
  [530] = {.entry = {.count = 1, .reusable = true}}, SHIFT(187),
  [532] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_dictionary_expression_repeat1, 3),
  [534] = {.entry = {.count = 1, .reusable = true}}, SHIFT(140),
  [536] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_argument, 1),
  [538] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym__expression, 1),
  [540] = {.entry = {.count = 1, .reusable = false}}, SHIFT(90),
  [542] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym__expression, 1),
  [544] = {.entry = {.count = 1, .reusable = true}}, SHIFT(170),
  [546] = {.entry = {.count = 1, .reusable = true}}, SHIFT(171),
  [548] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_array_expression_repeat1, 2),
  [550] = {.entry = {.count = 1, .reusable = true}}, SHIFT(51),
  [552] = {.entry = {.count = 1, .reusable = true}}, SHIFT(68),
  [554] = {.entry = {.count = 1, .reusable = true}}, SHIFT(80),
  [556] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [558] = {.entry = {.count = 1, .reusable = true}}, SHIFT(185),
  [560] = {.entry = {.count = 1, .reusable = true}}, SHIFT(57),
  [562] = {.entry = {.count = 1, .reusable = true}}, SHIFT(186),
  [564] = {.entry = {.count = 1, .reusable = true}}, SHIFT(132),
  [566] = {.entry = {.count = 1, .reusable = true}}, SHIFT(59),
  [568] = {.entry = {.count = 1, .reusable = true}}, SHIFT(166),
  [570] = {.entry = {.count = 1, .reusable = true}}, SHIFT(167),
  [572] = {.entry = {.count = 1, .reusable = true}}, SHIFT(77),
  [574] = {.entry = {.count = 1, .reusable = true}}, SHIFT(165),
  [576] = {.entry = {.count = 1, .reusable = true}}, SHIFT(133),
  [578] = {.entry = {.count = 1, .reusable = true}}, SHIFT(42),
  [580] = {.entry = {.count = 1, .reusable = true}}, SHIFT(212),
  [582] = {.entry = {.count = 1, .reusable = true}}, SHIFT(209),
  [584] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_dotted_name_repeat1, 2),
  [586] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_dotted_name_repeat1, 2), SHIFT_REPEAT(214),
  [589] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_dotted_name, 1),
  [591] = {.entry = {.count = 1, .reusable = true}}, SHIFT(214),
  [593] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_dotted_name, 2),
  [595] = {.entry = {.count = 1, .reusable = false}}, SHIFT(172),
  [597] = {.entry = {.count = 1, .reusable = true}}, SHIFT(208),
  [599] = {.entry = {.count = 1, .reusable = true}}, SHIFT(182),
  [601] = {.entry = {.count = 1, .reusable = true}}, SHIFT(203),
  [603] = {.entry = {.count = 1, .reusable = false}}, SHIFT(16),
  [605] = {.entry = {.count = 1, .reusable = true}}, SHIFT(149),
  [607] = {.entry = {.count = 1, .reusable = false}}, SHIFT_EXTRA(),
  [609] = {.entry = {.count = 1, .reusable = true}}, SHIFT(32),
  [611] = {.entry = {.count = 1, .reusable = false}}, SHIFT(20),
  [613] = {.entry = {.count = 1, .reusable = true}}, SHIFT(145),
  [615] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_string_expression_repeat1, 2),
  [617] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_string_expression_repeat1, 2), SHIFT_REPEAT(149),
  [620] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_dictionary_expression_repeat1, 2),
  [622] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_dictionary_expression_repeat1, 2), SHIFT_REPEAT(148),
  [625] = {.entry = {.count = 1, .reusable = true}}, SHIFT(201),
  [627] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [629] = {.entry = {.count = 1, .reusable = true}}, SHIFT(183),
  [631] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [633] = {.entry = {.count = 1, .reusable = true}}, SHIFT(79),
  [635] = {.entry = {.count = 1, .reusable = true}}, SHIFT(194),
  [637] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_option_dictionary_expression_repeat1, 2),
  [639] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_option_dictionary_expression_repeat1, 2), SHIFT_REPEAT(203),
  [642] = {.entry = {.count = 1, .reusable = true}}, SHIFT(46),
  [644] = {.entry = {.count = 1, .reusable = true}}, SHIFT(138),
  [646] = {.entry = {.count = 1, .reusable = true}}, SHIFT(19),
  [648] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_array_expression_repeat1, 2), SHIFT_REPEAT(104),
  [651] = {.entry = {.count = 1, .reusable = true}}, SHIFT(35),
  [653] = {.entry = {.count = 1, .reusable = true}}, SHIFT(39),
  [655] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_argument_list_repeat1, 2),
  [657] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_argument_list_repeat1, 2), SHIFT_REPEAT(79),
  [660] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [662] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [664] = {.entry = {.count = 1, .reusable = true}}, SHIFT(134),
  [666] = {.entry = {.count = 1, .reusable = true}}, SHIFT(86),
  [668] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [670] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_argument, 3, .production_id = 15),
  [672] = {.entry = {.count = 1, .reusable = true}}, SHIFT(66),
  [674] = {.entry = {.count = 1, .reusable = true}}, SHIFT(210),
  [676] = {.entry = {.count = 1, .reusable = true}}, SHIFT(135),
  [678] = {.entry = {.count = 1, .reusable = true}}, SHIFT(87),
  [680] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_option_dictionary_expression_repeat1, 5),
  [682] = {.entry = {.count = 1, .reusable = true}}, SHIFT(61),
  [684] = {.entry = {.count = 1, .reusable = true}}, SHIFT(89),
  [686] = {.entry = {.count = 1, .reusable = true}}, SHIFT(195),
  [688] = {.entry = {.count = 1, .reusable = true}}, SHIFT(215),
  [690] = {.entry = {.count = 1, .reusable = true}}, SHIFT(60),
  [692] = {.entry = {.count = 1, .reusable = true}}, SHIFT(98),
  [694] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_dictionary_expression_repeat1, 4),
  [696] = {.entry = {.count = 1, .reusable = true}}, SHIFT(107),
  [698] = {.entry = {.count = 1, .reusable = true}}, SHIFT(188),
  [700] = {.entry = {.count = 1, .reusable = true}}, SHIFT(161),
  [702] = {.entry = {.count = 1, .reusable = true}}, SHIFT(163),
  [704] = {.entry = {.count = 1, .reusable = true}}, SHIFT(147),
  [706] = {.entry = {.count = 1, .reusable = true}}, SHIFT(184),
  [708] = {.entry = {.count = 1, .reusable = true}}, SHIFT(91),
  [710] = {.entry = {.count = 1, .reusable = true}}, SHIFT(144),
  [712] = {.entry = {.count = 1, .reusable = true}}, SHIFT(100),
  [714] = {.entry = {.count = 1, .reusable = true}}, SHIFT(190),
  [716] = {.entry = {.count = 1, .reusable = true}}, SHIFT(152),
  [718] = {.entry = {.count = 1, .reusable = true}}, SHIFT(197),
  [720] = {.entry = {.count = 1, .reusable = true}}, SHIFT(93),
  [722] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [724] = {.entry = {.count = 1, .reusable = true}}, SHIFT(136),
  [726] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_aliased_import, 3, .production_id = 6),
  [728] = {.entry = {.count = 1, .reusable = true}}, SHIFT(97),
  [730] = {.entry = {.count = 1, .reusable = true}}, SHIFT(213),
  [732] = {.entry = {.count = 1, .reusable = true}}, SHIFT(207),
  [734] = {.entry = {.count = 1, .reusable = true}}, SHIFT(106),
  [736] = {.entry = {.count = 1, .reusable = true}}, SHIFT(176),
  [738] = {.entry = {.count = 1, .reusable = true}}, SHIFT(169),
  [740] = {.entry = {.count = 1, .reusable = true}}, SHIFT(141),
  [742] = {.entry = {.count = 1, .reusable = true}}, SHIFT(180),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_monkeyc(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
